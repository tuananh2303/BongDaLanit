import {
  RANK_NEWBIE,
  RANK_PUPIL,
  RANK_SPECIALIST,
  RANK_EXPERT,
  RANK_MASTER,
  RANK_MASTER_1,
  RANK_MASTER_2,
  RANK_MASTER_3,
  RANK_MASTER_4,
  RANK_MASTER_5,
  RANK_MASTER_6,
  RANK_MASTER_7,
  RANK_MASTER_8,
  RANK_MASTER_9,
  RANK_MASTER_10,
  RANK_MASTER_11,
  RANK_LEGEND,
  ADMIN,
} from '../constants/RankList';
import { Images } from '../themes';

const getIconRank = data => {
  console.log(data);
  if (data == RANK_NEWBIE) {
    return Images.rankIcon01;
  }
  if (data == RANK_PUPIL) {
    return Images.rankIcon02;
  }
  if (data == RANK_SPECIALIST) {
    return Images.rankIcon03;
  }
  if (data == RANK_EXPERT) {
    return Images.rankIcon04;
  }
  if (data == RANK_MASTER) {
    return Images.rankIcon05;
  }
  if (data == RANK_MASTER_1) {
    return Images.rankIcon06;
  }
  if (data == RANK_MASTER_2) {
    return Images.rankIcon07;
  }
  if (data == RANK_MASTER_3) {
    return Images.rankIcon08;
  }
  if (data == RANK_MASTER_4) {
    return Images.rankIcon09;
  }
  if (data == RANK_MASTER_5) {
    return Images.rankIcon10;
  }
  if (data == RANK_MASTER_6) {
    return Images.rankIcon11;
  }
  if (data == RANK_MASTER_7) {
    return Images.rankIcon12;
  }
  if (data == RANK_MASTER_8) {
    return Images.rankIcon13;
  }
  if (data == RANK_MASTER_9) {
    return Images.rankIcon14;
  }
  if (data == RANK_MASTER_10) {
    return Images.rankIcon15;
  }
  if (data == RANK_MASTER_11) {
    return Images.rankIcon16;
  }
  if (data == RANK_LEGEND) {
    return Images.rankIcon17;
  }

  if (data == ADMIN) {
    return Images.rankIcon17;
  }

  return Images.rankIcon01;
};

export { getIconRank };
