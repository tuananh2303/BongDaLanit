import Moment from 'moment/min/moment-with-locales';
import LanguageVI from '../constants/Language';

const getTimeFromNow = data => {
  const moment = Moment(data);
  const now = Moment();
  // console.log(now.diff(moment, 'minutes'));
  return now.diff(moment, 'minutes');
};

const converTime = data => {
  const moment = Moment(data);
  moment.locale('vi');
  return moment.format('DD MMMM YYYY');
};

const converTimeShort = data => {
  const moment = Moment(data);
  moment.locale('vi');
  return moment.format('DD MMM');
};

const converTimeShortGamePlay = data => {
  const moment = Moment(data);
  moment.locale('vi');
  return moment.format('H:mm - DD/MM');
};

const getDayOfWeekAndTimeShot = day => {
  const moment = Moment(day);
  moment.locale('vi');

  return {
    // time: time,
    day: moment.format('ddd'),
    time: moment.format('H:mm'),
  };
};

const dayOfWeek = numberToAdd => {
  // it will return t2 t3 t7 CN depend on date
  const moment = Moment();
  moment.locale('vi');
  const condition = moment.add(numberToAdd, 'day').day();
  // console.log(condition);

  if (condition === 0) {
    return LanguageVI.SUN;
  }
  if (condition === 1) {
    return LanguageVI.MON;
  }
  if (condition === 2) {
    return LanguageVI.TUE;
  }
  if (condition === 3) {
    return LanguageVI.WED;
  }
  if (condition === 4) {
    return LanguageVI.THU;
  }
  if (condition === 5) {
    return LanguageVI.FRI;
  }
  if (condition === 6) {
    return LanguageVI.SAT;
  }

  return '';
};

const dayOfMonth = numberToAdd => {
  const moment = Moment();
  moment.locale('vi');
  return moment.add(numberToAdd, 'day').date();
};

export {
  getTimeFromNow,
  converTime,
  converTimeShort,
  dayOfWeek,
  dayOfMonth,
  getDayOfWeekAndTimeShot,
  converTimeShortGamePlay,
};
