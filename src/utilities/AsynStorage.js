import { AsyncStorage } from 'react-native';

const storeData = async (dataName, data) => {
  try {
    await AsyncStorage.setItem(dataName, JSON.stringify(data));
  } catch (error) {
    console.log(error);
  }
};

export default storeData;
