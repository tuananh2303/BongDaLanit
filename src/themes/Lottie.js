const Lottie = {
  reward: require('../../assets/lottie/reward.json'),
  error: require('../../assets/lottie/error.json'),
};

export default Lottie;
