const Images = {
  startUpImage: require('../../assets/foot_m.jpg'),
  teamImage: require('../../assets/fc_barcelona_football_tickets.jpg'),
  real: require('../../assets/real.png'),
  valencia: require('../../assets/valencia.png'),
  hedera: require('../../assets/hedera.jpg'),
  iconGoogle: require('../../assets/icongoogle.png'),

  SoccerField: require('../../assets/SoccerField.png'),

  SoccerFieldTop: require('../../assets/SoccerFieldTop.png'),
  SoccerFieldBottom: require('../../assets/SoccerFieldBottom.png'),
  ball: require('../../assets/quabong.jpg'),
  // chanthuong: require('../../assets/chanthuong.png'),

  england: require('../../assets/england.png'),
  chelsea: require('../../assets/chelsea.png'),

  defaultImage: require('../../assets/default-image.png'),
  logoTeamDefault: require('../../assets/logoTeamDefault.png'),
  avatarPlayerDefault: require('../../assets/avatarPlayerDefault.png'),

  theVang: require('../../assets/iconEvents/thevang.png'),
  theDo: require('../../assets/iconEvents/thedo.png'),
  buGio: require('../../assets/iconEvents/bugio.png'),
  chanThuong: require('../../assets/iconEvents/chanthuong.png'),
  cotDoc: require('../../assets/iconEvents/cotdoc.png'),
  phamLoi: require('../../assets/iconEvents/phamloi.png'),
  phatGoc: require('../../assets/iconEvents/phatgoc.png'),
  sutTrungCauMon: require('../../assets/iconEvents/suttrungcaumon.png'),
  thayNguoi: require('../../assets/iconEvents/thaynguoi.png'),
  theVangThu2: require('../../assets/iconEvents/thevangthu2.png'),
  vietVi: require('../../assets/iconEvents/vietvi.png'),
  xaNgang: require('../../assets/iconEvents/xangang.png'),

  stadium: require('../../assets/stadium.jpg'),
  stadium2: require('../../assets/stadium2.jpg'),
  stadium3: require('../../assets/stadium3.jpg'),
  stadium4: require('../../assets/stadium4.jpg'),
  stadium5: require('../../assets/stadium5.jpg'),

  coin: require('../../assets/coin.jpg'),

  Facebook_Login: require('../../assets/Facebook_Login.png'),

  // rankIcon

  rankIcon01: require('../../assets/rankicon/01.png'),
  rankIcon02: require('../../assets/rankicon/02.png'),
  rankIcon03: require('../../assets/rankicon/03.png'),
  rankIcon04: require('../../assets/rankicon/11.png'),
  rankIcon05: require('../../assets/rankicon/12.png'),
  rankIcon06: require('../../assets/rankicon/13.png'),
  rankIcon07: require('../../assets/rankicon/14.png'),
  rankIcon08: require('../../assets/rankicon/21.png'),
  rankIcon09: require('../../assets/rankicon/22.png'),
  rankIcon10: require('../../assets/rankicon/23.png'),
  rankIcon11: require('../../assets/rankicon/31.png'),
  rankIcon12: require('../../assets/rankicon/32.png'),
  rankIcon13: require('../../assets/rankicon/33.png'),
  rankIcon14: require('../../assets/rankicon/41.png'),
  rankIcon15: require('../../assets/rankicon/42.png'),
  rankIcon16: require('../../assets/rankicon/43.png'),
  rankIcon17: require('../../assets/rankicon/51.png'),
};

export default Images;
