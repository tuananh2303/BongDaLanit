const ColorThemeDay = {
  white: 'rgb(250,250,250)',
  white1: 'rgb(245,245,245)',
  white2: 'rgb(224,224,224)',

  blue: 'rgb(3,169,244)',
  blueGrey500a: 'rgba(96,125,139,0.5)',
  blueGrey200a: 'rgba(176,190,197,0.6)',
  blue700: 'rgb(25,118,210)',
  blue800: 'rgb(21,101,192)',
  blue900: 'rgb(13,71,161)',

  LightBlue500: 'rgb(3,169,244)',
  lightBlue300a: 'rgba(79,195,247,0.6)',

  black: 'rgb(33,33,33)',

  gray: 'rgb(97,97,97)',
  gray500: 'rgb(158,158,158)',
  gray500a: 'rgba(158,158,158,0.6)',
  gray300: 'rgba(224,224,224,0.5)',
  gray200: 'rgb(238,238,238)',
  gray700: 'rgb(97,97,97)',
  gray400: 'rgb(189,189,189)',
  grayModalTransparent: 'rgba(52, 52, 52, 0.8)',

  green: 'rgb(0,77,64)',
  green400: 'rgb(102,187,106)',
  green500: 'rgb(76,175,80)',
  green900: 'rgb(27,94,32)',
  lightGreen500: 'rgb(139,195,74)',
  lightGreen400a: 'rgba(156,204,101,0.6)',

  Teal900: 'rgb(0,77,64)',
  Teal500: 'rgb(0,150,136)',

  red500: 'rgb(244,67,54)',
  red900: 'rgb(183,28,28)',

  indigo700: 'rgb(48,63,159)',

  lime500: 'rgb(205,220,57)',
  lime400: 'rgb(212,225,87)',

  purple700: 'rgb(123,31,162)',
  deepPurple400a: 'rgba(149,117,205,0.6)',

  orange400: 'rgb(255,167,38)',
  orange200: 'rgb(255,204,128)',
  orange700: 'rgb(245,124,0)',
  deepOrange400a: 'rgba(255,112,67,0.6)',
  deepOrange500: 'rgb(255,87,34)',
  deepOrange600: 'rgb(244,81,30)',

  yellow600: 'rgb(253,216,53)',

  amber500: 'rgb(255,193,7)',
};

export default ColorThemeDay;
