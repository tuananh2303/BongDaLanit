import Axios from 'axios';
import firebase from 'react-native-firebase';

import {
  GET_USER_TEAM_LEAGUE_LIKE_ING,
  GET_USER_TEAM_LEAGUE_LIKE_SUCCESS,
  GET_USER_TEAM_LEAGUE_LIKE_FAIL,
} from '../constants/actionTypes';

import { FIREBASE_REF_USER } from '../../constants/URL';

const getUserTeamLeague = () => {
  return {
    type: GET_USER_TEAM_LEAGUE_LIKE_ING,
  };
};

const getUserTeamLeagueSuccess = data => {
  return {
    type: GET_USER_TEAM_LEAGUE_LIKE_SUCCESS,
    payload: data,
  };
};

const getUserTeamLeagueFail = () => {
  return {
    type: GET_USER_TEAM_LEAGUE_LIKE_FAIL,
  };
};

const fetchDatagetUserTeamLeague = userID => {
  return dispatch => {
    dispatch(getUserTeamLeague());
    // console.log(FIREBASE_REF_USER + userID);

    const database = firebase
      .database()
      .ref(FIREBASE_REF_USER + userID)
      .on('value', snapShot => {
        if (snapShot.val() == null) {
          dispatch(getUserTeamLeagueFail());
        }
        console.log(snapShot.val());
        dispatch(getUserTeamLeagueSuccess(snapShot.val()));
      });
  };
};

export { fetchDatagetUserTeamLeague as default };
