import {
  SETTING_TIME_TO_REFRESH,
  SETTING_INIT,
  SETTING_SHOW_EVENT,
  SETTING_NOTI,
  SETTING_THEME,
} from '../constants/actionTypes';

const settingTimeToRefresh = data => {
  return {
    type: SETTING_TIME_TO_REFRESH,
    payload: data,
  };
};

const settingInit = data => {
  return {
    type: SETTING_INIT,
    payload: data,
  };
};

const settingEvent = data => {
  return {
    type: SETTING_SHOW_EVENT,
    payload: data,
  };
};

const settingNoti = data => {
  return {
    type: SETTING_NOTI,
    payload: data,
  };
};

const settingTheme = data => {
  return {
    type: SETTING_THEME,
    payload: data,
  };
};

export {
  settingTimeToRefresh,
  settingInit,
  settingTheme,
  settingNoti,
  settingEvent,
};
