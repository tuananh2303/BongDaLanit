import { CHANGE_CALENDAR } from '../constants/actionTypes';

const changeCalenDar = day => {
  return {
    type: CHANGE_CALENDAR,
    day,
  };
};

export { changeCalenDar as default };
