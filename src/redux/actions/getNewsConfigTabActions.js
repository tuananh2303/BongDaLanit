// import Axios from 'axios';
import firebase from 'react-native-firebase';

import {
  GET_NEWS_CONFIG_TAB_ING,
  GET_NEWS_CONFIG_TAB_SUCCESS,
  GET_NEWS_CONFIG_TAB_FAIL,
  CHANGE_NEWS_TAB,
} from '../constants/actionTypes';

import { FIREBASE_REF_NEWS_CONFIG_TAB } from '../../constants/URL';

const getNewsConfigTab = () => {
  return {
    type: GET_NEWS_CONFIG_TAB_ING,
  };
};

const getNewsConfigTabSuccess = data => {
  return {
    type: GET_NEWS_CONFIG_TAB_SUCCESS,
    payload: data,
  };
};

const getNewsConfigTabFail = () => {
  return {
    type: GET_NEWS_CONFIG_TAB_FAIL,
  };
};

const fetchDatagetNewsConfigTab = () => {
  return dispatch => {
    dispatch(getNewsConfigTab());
    console.log(FIREBASE_REF_NEWS_CONFIG_TAB);

    const database = firebase
      .database()
      .ref(FIREBASE_REF_NEWS_CONFIG_TAB)
      .on('value', snapShot => {
        if (snapShot.val() == null) {
          dispatch(getNewsConfigTabFail());
        }
        console.log(snapShot.val());
        dispatch(getNewsConfigTabSuccess(snapShot.val()));
      });
  };
};

const changeNewsTab = index => {
  return {
    type: CHANGE_NEWS_TAB,
    index,
  };
};

export { fetchDatagetNewsConfigTab, changeNewsTab };
