import Axios from 'axios';

import {
  ADD_USER_INFO,
  USER_INFO,
  USER_INFO_ING,
  USER_INFO_FAIL,
} from '../constants/actionTypes';
import { URL_LOGIN } from '../../constants/URL';

const addUserInfo = data => {
  return {
    type: ADD_USER_INFO,
    payload: data,
  };
};

const userInfo = data => {
  return {
    type: USER_INFO,
    payload: data,
  };
};

const userInfoIng = () => {
  return {
    type: USER_INFO_ING,
  };
};

const userInfoFail = data => {
  return {
    type: USER_INFO_FAIL,
    payload: data,
  };
};

const refreshData = data => {
  return dispatch => {
    console.log('refreshData', data);

    Axios.get(URL_LOGIN + data.fbid + '?token=' + data.token)
      .then(response => {
        console.log(response);
        if (response.data.status == 'success') {
          dispatch(userInfo(response.data));
        }
        if (response.data.status == 'error') {
          dispatch(userInfoFail(response.data));
        }
      })
      .catch(error => {
        dispatch(userInfoFail(error));
      });
  };
};

export { addUserInfo, userInfo, refreshData };
