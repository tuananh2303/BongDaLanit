import {
  GET_USER_TEAM_LEAGUE_LIKE_ING,
  GET_USER_TEAM_LEAGUE_LIKE_SUCCESS,
  GET_USER_TEAM_LEAGUE_LIKE_FAIL,
} from '../constants/actionTypes';

const INIT_STATE = {
  isFetching: false,
  error: false,
  data: [],
  dataSuccess: false,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_USER_TEAM_LEAGUE_LIKE_ING:
      return {
        ...state,
        isFetching: true,
      };
    case GET_USER_TEAM_LEAGUE_LIKE_SUCCESS:
      return {
        ...state,
        isFetching: false,
        dataSuccess: true,
        data: action.payload,
      };
    case GET_USER_TEAM_LEAGUE_LIKE_FAIL:
      return {
        ...state,
        isFetching: false,
        dataSuccess: false,
        error: true,
      };
    default:
      return state;
  }
};
