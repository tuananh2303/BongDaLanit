import {
  ADD_USER_INFO,
  USER_INFO,
  USER_INFO_FAIL,
  USER_INFO_ING,
} from '../constants/actionTypes';

const INIT_STATE = {
  data: null,
  dataFull: null,
  isFetching: false,
  error: false,
  dataSuccess: false,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case ADD_USER_INFO:
      console.log('action.payload ', action.payload);
      return {
        ...state,
        data: action.payload,
      };

    case USER_INFO:
      console.log('action.payload ', action.payload);
      return {
        ...state,
        dataFull: action.payload,
        dataSuccess: true,
        isFetching: false,
      };

    case USER_INFO_ING:
      return {
        ...state,
        isFetching: true,
      };

    case USER_INFO_FAIL:
      return {
        ...state,
        isFetching: false,
        dataSuccess: false,
        error: true,
      };

    default:
      return state;
  }
};
