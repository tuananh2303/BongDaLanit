import { CHANGE_CALENDAR } from '../constants/actionTypes';

const INIT_STATE = {
  day: 3,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case CHANGE_CALENDAR:
      return {
        ...state,
        day: action.day,
      };

    default:
      return state;
  }
};
