import {
  SETTING_TIME_TO_REFRESH,
  SETTING_INIT,
  SETTING_SHOW_EVENT,
  SETTING_NOTI,
  SETTING_THEME,
} from '../constants/actionTypes';

const INIT_STATE = {
  data: null,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case SETTING_INIT:
      console.log('init', action.payload);

      return {
        ...state,
        data: action.payload,
      };

    case SETTING_TIME_TO_REFRESH:
      console.log('time', action.payload);
      const dataTemp = state.data;
      dataTemp.timeToRefresh = action.payload;
      console.log(dataTemp);

      return {
        ...state,
        data: dataTemp,
      };

    case SETTING_SHOW_EVENT:
      console.log('event', action.payload);
      const dataTemp1 = state.data;
      dataTemp1.hideShowEvent = action.payload;
      console.log(dataTemp1);

      return {
        ...state,
        data: dataTemp1,
      };

    case SETTING_NOTI:
      console.log('noti', action.payload);
      const dataTemp2 = state.data;
      dataTemp2.notification = action.payload;
      console.log(dataTemp2);

      return {
        ...state,
        data: dataTemp2,
      };

    case SETTING_THEME:
      console.log('theme', action.payload);
      const dataTemp3 = state.data;
      dataTemp3.theme = action.payload;
      console.log(dataTemp3);

      return {
        ...state,
        data: dataTemp3,
      };

    default:
      return state;
  }
};
