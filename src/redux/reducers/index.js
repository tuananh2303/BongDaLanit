import { combineReducers } from 'redux';
import getUserTeamLeagueReducers from './getUserTeamLeagueReducers';
import getNewsConfigTabReducers from './getNewsConfigTabReducers';
import calendarReducer from './calendarReducer';
import userReducer from './userReducer';
import settingReducer from './settingReducer';

const rootRecuder = combineReducers({
  getUserTeamLeagueReducers,
  getNewsConfigTabReducers,
  calendarReducer,
  userReducer,
  settingReducer,
});

export default rootRecuder;
