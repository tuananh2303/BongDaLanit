import {
  GET_NEWS_CONFIG_TAB_ING,
  GET_NEWS_CONFIG_TAB_SUCCESS,
  GET_NEWS_CONFIG_TAB_FAIL,
  CHANGE_NEWS_TAB,
} from '../constants/actionTypes';

const INIT_STATE = {
  isFetching: false,
  error: false,
  index: 0,
  data: [
    {
      key: 'tab1',
      label: 'PremierLeague',
      category: 65,
      index: 0,
    },
    {
      key: 'tab2',
      label: 'VLeague',
      category: 65,
      index: 1,
    },
    {
      key: 'tab3',
      label: 'Laliga',
      category: 65,
      index: 2,
    },
    {
      key: 'tab4',
      label: 'SerieA',
      category: 65,
      index: 3,
    },
    {
      key: 'tab5',
      label: 'Bundesliga',
      category: 65,
      index: 4,
    },
    {
      key: 'tab6',
      label: 'Ligue1',
      category: 65,
      index: 5,
    },
  ],
  dataSuccess: false,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_NEWS_CONFIG_TAB_ING:
      return {
        ...state,
        isFetching: true,
      };
    case GET_NEWS_CONFIG_TAB_SUCCESS:
      console.log(action.payload);

      return {
        ...state,
        isFetching: false,
        dataSuccess: true,
        data: action.payload,
      };
    case GET_NEWS_CONFIG_TAB_FAIL:
      return {
        ...state,
        isFetching: false,
        dataSuccess: false,
        error: true,
      };

    case CHANGE_NEWS_TAB:
      return {
        ...state,
        index: action.index,
      };
    default:
      return state;
  }
};
