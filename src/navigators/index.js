import React from 'react';
import { createSwitchNavigator } from 'react-navigation';

import AppStack from './AppStack';
import LoginStack from './LoginStack';
import StartUp from '../container/startUp';

const RootNavigator = createSwitchNavigator(
  {
    StartUp: { screen: StartUp },
    LoginStack: { screen: LoginStack },
    AppStack: { screen: AppStack },
  },
  {
    initialRouteKey: 'AppStack',
    headerMode: 'none',
  },
);

export default RootNavigator;
