import React from 'react';
import {
  createBottomTabNavigator,
  createStackNavigator,
} from 'react-navigation';

// import Icon from 'react-native-vector-icons/Ionicons';

// import HomeStack from './HomeStack';
// import CalendarStack from './CalendarStack';
// import Game from '../container/game';
// // import News from '../container/news';
// import NewsStack from './NewsStack';
// import Chat from '../container/chat';
// import SettingStack from './SettingStack';

// import Language from '../constants/Language';
// import { ColorTheme } from '../themes';
import Home from '../container/home';
import Match from '../container/match';
import Team from '../container/team';
import Player from '../container/player';
import League from '../container/league';
import TopScore from '../container/topScore';
import Fixture from '../container/fixture';
import News from '../container/news';
import TransferRumours from '../container/transferRumours';
import TableScoreAll from '../container/tableScoreAll';
import Search from '../container/search';
import searchContent from '../container/searchContent';
import NewsContents from '../container/NewsContents';
import SettingCalendar from '../container/SettingCalendar';
import Setting from '../container/setting';
import SettingMatch from '../container/SettingMatch';
import SettingPushNotification from '../container/SettingPushNotification';
import SettingMatchHideShow from '../container/SettingMatchHideShow';
import SettingData from '../container/SettingData';
import SettingPushNotificationSetting from '../container/SettingPushNotificationSetting';
import SettingPushNotificationTroubleshoot from '../container/SettingPushNotificationTroubleshoot';
import SettingPushNotificationTeam from '../container/SettingPushNotificationTeam';
import SettingPushNotificationEditDeffault from '../container/SettingPushNotificationEditDeffault';
import SettingAppLanguage from '../container/SettingAppLanguage';
import Profile from '../container/profile';
import newsImage from '../container/newsImage';
import NewsContentsWeb from '../container/NewsContentsWeb';
import SettingTheme from '../container/SettingTheme';
import Chat from '../container/chat';

const AppStack = createStackNavigator(
  {
    Home: {
      screen: Home,
    },
    Match: {
      screen: Match,
    },
    Team: {
      screen: Team,
    },
    Player: {
      screen: Player,
    },
    League: {
      screen: League,
    },
    TopScore: {
      screen: TopScore,
    },
    Fixture: {
      screen: Fixture,
    },
    News: {
      screen: News,
    },
    TransferRumours: {
      screen: TransferRumours,
    },
    TableScoreAll: {
      screen: TableScoreAll,
    },
    Search: {
      screen: Search,
    },
    searchContent: {
      screen: searchContent,
    },
    NewsContents: {
      screen: NewsContents,
    },
    SettingCalendar: {
      screen: SettingCalendar,
    },

    Setting: {
      screen: Setting,
    },
    SettingMatch: {
      screen: SettingMatch,
    },
    SettingMatchHideShow: {
      screen: SettingMatchHideShow,
    },
    SettingPushNotification: {
      screen: SettingPushNotification,
    },
    SettingData: {
      screen: SettingData,
    },
    SettingPushNotificationSetting: {
      screen: SettingPushNotificationSetting,
    },
    SettingPushNotificationTroubleshoot: {
      screen: SettingPushNotificationTroubleshoot,
    },
    SettingPushNotificationTeam: {
      screen: SettingPushNotificationTeam,
    },
    SettingPushNotificationEditDeffault: {
      screen: SettingPushNotificationEditDeffault,
    },
    SettingAppLanguage: {
      screen: SettingAppLanguage,
    },
    Profile: {
      screen: Profile,
    },
    newsImage: {
      screen: newsImage,
    },
    NewsContentsWeb: {
      screen: NewsContentsWeb,
    },
    SettingTheme: {
      screen: SettingTheme,
    },
    Chat: {
      screen: Chat,
    },
  },
  {
    initialRouteKey: 'Home',
    headerMode: 'none',
  },
);

export default AppStack;
