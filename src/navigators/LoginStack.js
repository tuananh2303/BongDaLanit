import React from 'react';
import { createStackNavigator } from 'react-navigation';

import Login from '../container/login';
import FirstSetting from '../container/firstSetting';
import Privacy from '../container/Privacy';
import FirstSettingTab from '../container/firstSettingTab';

const LoginStack = createStackNavigator(
  {
    Login: { screen: Login },
    FirstSetting: { screen: FirstSetting },
    Privacy: { screen: Privacy },
    FirstSettingTab: { screen: FirstSettingTab },
  },
  {
    initialRouteKey: 'Login',
    headerMode: 'none',
  },
);

export default LoginStack;
