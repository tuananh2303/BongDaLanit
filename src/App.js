import React, { Component } from 'react';
import { View, Text } from 'react-native';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import rootRecuder from './redux/reducers';
import RootNavigator from './navigators';

const store = createStore(rootRecuder, applyMiddleware(thunk));

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Provider store={store}>
        <RootNavigator />
      </Provider>
    );
  }
}
export default App;
