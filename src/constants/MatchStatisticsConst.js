// export const STATISTICS_0 = 'Kick-off';
// export const STATISTICS_1 = 'First Corner Kick';
// export const STATISTICS_2 = 'First Yellow Card';
// export const STATISTICS_3 = 'Shots';
// export const STATISTICS_4 = 'Shots on Goal';
// export const STATISTICS_5 = 'Fouls';
// export const STATISTICS_6 = 'Corner Kicks';
// export const STATISTICS_7 = 'Corner Kicks(Extra time)';
// export const STATISTICS_8 = 'Free Kicks';
// export const STATISTICS_9 = 'Offsides';
// export const STATISTICS_10 = 'Own Goals';
// export const STATISTICS_11 = 'Yellow Cards';
// export const STATISTICS_12 = 'Yellow Cards(Extra time)';
// export const STATISTICS_13 = 'Red Cards';
// export const STATISTICS_14 = 'Ball Possession';
// export const STATISTICS_15 = 'Headers';
// export const STATISTICS_16 = 'Saves';
// export const STATISTICS_17 = 'Goalkeeper off his line';
// export const STATISTICS_18 = 'Balls lost';
// export const STATISTICS_19 = 'Successful tackles';
// export const STATISTICS_20 = 'Interceptions';
// export const STATISTICS_21 = 'Long balls';
// export const STATISTICS_22 = 'Short passes';
// export const STATISTICS_23 = 'Assists';
// export const STATISTICS_24 = 'Successful crosses';
// export const STATISTICS_25 = 'First Substitution';
// export const STATISTICS_26 = 'Last Substitution';
// export const STATISTICS_27 = 'First Offside';
// export const STATISTICS_28 = 'Last Offside';
// export const STATISTICS_29 = 'Substitutions';
// export const STATISTICS_30 = 'Last Corner Kick';
// export const STATISTICS_31 = 'Last Yellow Card';
// export const STATISTICS_32 = 'Substitutions(Extra time)';
// export const STATISTICS_33 = 'Offsides(Extra time';
// export const STATISTICS_34 = 'Off Target';
// export const STATISTICS_35 = 'Hit the post';
// export const STATISTICS_36 = 'Head Success';
// export const STATISTICS_37 = 'Blocked';
// export const STATISTICS_38 = 'Tackles';
// export const STATISTICS_39 = 'Dribbles';
// export const STATISTICS_40 = 'Throw ins';
// export const STATISTICS_41 = 'Pass';
import LanguageVI from './Language';

export const STATISTICS_42 = 'Pass Success';

export const statistics = [
  {
    id: 0,
    label: LanguageVI.Kick_off,
  },
  {
    id: 1,
    label: LanguageVI.First_Corner_Kick,
  },
  {
    id: 2,
    label: LanguageVI.First_Yellow_Card,
  },
  {
    id: 3,
    label: LanguageVI.Shots,
  },
  {
    id: 4,
    label: LanguageVI.ShotsOnGoal,
  },
  {
    id: 5,
    label: LanguageVI.Fouls,
  },
  {
    id: 6,
    label: LanguageVI.Corners,
  },
  {
    id: 7,
    label: LanguageVI.CornersExtraTime,
  },
  {
    id: 8,
    label: LanguageVI.FreeKick,
  },
  {
    id: 9,
    label: LanguageVI.OffSides,
  },
  {
    id: 10,
    label: LanguageVI.Own_Goals,
  },
  {
    id: 11,
    label: LanguageVI.Yellow_Cards,
  },
  {
    id: 12,
    label: LanguageVI.Yellow_Cards_Extra_time,
  },
  {
    id: 13,
    label: LanguageVI.Red_Cards,
  },
  {
    id: 14,
    label: LanguageVI.Possesion,
  },
  {
    id: 15,
    label: LanguageVI.Headers,
  },
  {
    id: 16,
    label: LanguageVI.Saves,
  },
  {
    id: 17,
    label: LanguageVI.Goalkeeper_off_his_line,
  },
  {
    id: 18,
    label: LanguageVI.Balls_lost,
  },
  {
    id: 19,
    label: LanguageVI.Successful_tackles,
  },
  {
    id: 20,
    label: LanguageVI.Interceptions,
  },
  {
    id: 21,
    label: LanguageVI.Long_balls,
  },
  {
    id: 22,
    label: LanguageVI.Short_passes,
  },
  {
    id: 23,
    label: LanguageVI.Assists,
  },
  {
    id: 24,
    label: LanguageVI.Successful_crosses,
  },
  {
    id: 25,
    label: LanguageVI.First_Substitution,
  },
  {
    id: 26,
    label: LanguageVI.Last_Substitution,
  },
  {
    id: 27,
    label: LanguageVI.First_Offside,
  },
  {
    id: 28,
    label: LanguageVI.Last_Offside,
  },
  {
    id: 29,
    label: LanguageVI.Substitutions,
  },
  {
    id: 30,
    label: LanguageVI.Last_Corner_Kick,
  },
  {
    id: 31,
    label: LanguageVI.Last_Yellow_Card,
  },
  {
    id: 32,
    label: LanguageVI.Substitutions_Extra_time,
  },
  {
    id: 33,
    label: LanguageVI.Offsides_Extra_time,
  },
  {
    id: 34,
    label: LanguageVI.Off_Target,
  },
  {
    id: 35,
    label: LanguageVI.Hit_the_post,
  },
  {
    id: 36,
    label: LanguageVI.Head_Success,
  },
  {
    id: 37,
    label: LanguageVI.Blocked,
  },
  {
    id: 38,
    label: LanguageVI.Tackles,
  },
  {
    id: 39,
    label: LanguageVI.Dribbles,
  },
  {
    id: 40,
    label: LanguageVI.ThrowIns,
  },
  {
    id: 41,
    label: LanguageVI.Pass,
  },
  {
    id: 42,
    label: LanguageVI.Pass_Success,
  },
];
