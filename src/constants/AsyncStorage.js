export const LOGIN_REF = 'LOGIN_REF';
export const SETTING_APP = 'SETTING_APP';

export const VI = 'VI';
export const EN = 'EN';

export const SETTING_APP_DEFAULT = {
  hideShowEvent: true,
  notification: true,
  theme: 1,
  language: VI,
  timeToRefresh: 30,
};
