export const URL_WEATHER = 'http://api.congtintuc.org/forecast/vi/';

export const FIREBASE_REF_USER = 'user/';
export const FIREBASE_REF_NEWS_CONFIG_TAB = 'config/news/';

export const URL_NEWS = 'https://muldata.tintuc.app/posts/category/';
// add page number after link
// category = 0 and eg(page 12): https://muldata.tintuc.app/posts/category/0/12

export const URL_LIVESCORE = 'https://apibd.tintuc.app/livescore';
export const URL_IMAGE = 'https://apibd.tintuc.app/'; // + "images/database/projects/103_1.png",
// https://apibd.tintuc.app/images/database/projects/175_2.png

export const URL_CALENDAR = 'https://apibd.tintuc.app/calendar/';
// https://apibd.tintuc.app/calendar/{range} range = tu 1 den 7
// https://apibd.tintuc.app/calendar/7

export const URL_MATCH = 'https://apibd.tintuc.app/match/detail?matchID=';
// https://apibd.tintuc.app/match/detail?matchID=1330
// 1330 la id cua match do

export const URL_IMAGE_FOOTYROOM = 'https://images.footyroom.com/badges/';

export const URL_RANK_FOOTYROOM =
  'https://footyroom.com/predictor/leaderboard.json';

export const URL_RANK_TOP_EVENT = 'https://apibd.tintuc.app/predict/topevent/';
// https://apibd.tintuc.app/predict/topevent/118?token=EAAEAz3cQHgoB

export const URL_RANK_TOP_PREDICT =
  'https://apibd.tintuc.app/predict/toppredictor/';
// https://apibd.tintuc.app/predict/toppredictor/0  : 0 la theo tuan : 1 la theo thang

export const URL_RANK_THANHDU = 'https://apibd.tintuc.app/predict/thanhdu';

export const URL_LOGIN = 'https://apibd.tintuc.app/user/';
// https://apibd.tintuc.app/user/1704893079621477?token=EAAEAz3cQH0K4ZD

export const URL_SIGNIN = 'https://apibd.tintuc.app/auth/fb';

export const URL_GET_FB_INFO = 'https://graph.facebook.com/me?access_token=';

export const URL_PREDICT_LIST = 'https://apibd.tintuc.app/predict/list/';
// https://apibd.tintuc.app/predict/list/118?token=EAAEAz3cQHgoBAAZ
// /predict/list/{user_id}?token={token}

export const URL_PREDICT_HISTORY = 'https://apibd.tintuc.app/predict/history/';
// https://apibd.tintuc.app/predict/history/118

export const URL_IMAGE_PREDICT = 'https://apibd.tintuc.app';
// https://apibd.tintuc.app/images/database/predicts/75.png

export const URL_POST_PREDICT = 'https://apibd.tintuc.app/predict/add?token=';
// /predict/add?token={token}

export const URL_CHAT_SEND = 'https://apibd.tintuc.app/shoutbox/push?token=';
export const URL_CHAT_GET_MESS = 'https://apibd.tintuc.app/shoutbox/get';
export const URL_CHAT_DEL_MESS = 'https://apibd.tintuc.app/shoutbox/del/';
// /shoutbox/del/{index}/{user_id}?token={token}

export const URL_USER_BAN = 'https://apibd.tintuc.app/user/ban/';
// https://apibd.tintuc.app/user/ban/118/119?token=EAAEAz3cQHgoBALZAoLZ
export const URL_USER_UNBAN = 'https://apibd.tintuc.app/user/unban/';
// https://apibd.tintuc.app/user/unban/118/119?token=EAAEAz3cQHgoBALZAoLZ
