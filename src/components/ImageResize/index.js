import React, { Component } from 'react';
import FastImage from 'react-native-fast-image';
import * as d from '../../utilities/Tranform';

export default class ImageResize extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: d.windowSize.width,
      height: 300 * d.ratioH,
      linkImage: this.props.linkImage,
      Error: false,
    };
  }

  render() {
    return (
      <FastImage
        style={[
          this.props.style,
          {
            width: this.state.width,
            height: this.state.height,
          },
        ]}
        source={
          this.state.Error
            ? this.props.linkImageDefault
            : { uri: this.state.linkImage }
        }
        resizeMode={this.props.resizeMode}
        onLoad={e =>
          this.setState({
            height:
              (d.windowSize.width * e.nativeEvent.height) / e.nativeEvent.width,
          })
        }
        onError={Error => this.setState({ Error: true })}
      />
    );
  }
}
