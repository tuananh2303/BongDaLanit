import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import Pie from 'react-native-pie';
class ChartPieRow extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.txt_title}>HLV</Text>
        <View style={styles.row}>
          <Pie
            radius={30} // size
            series={[30, 70]} // phan tram
            colors={['rgb(0,137,123)', 'rgb(3,169,244)']} // mau
          />
        </View>
        <Text>65%</Text>
      </View>
    );
  }
}

export default ChartPieRow;
