import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    flexDirection: 'column',
    marginTop: 10 * d.ratioH,
    alignItems: 'center',
    marginLeft: 20 * d.ratioW,
  },
  row: {
    margin: 10,
  },
  txt_title: {
    fontSize: 16,
    fontWeight: '500',
    color: ColorTheme.black,
  },
});
