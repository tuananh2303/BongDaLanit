import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  Row: {
    flexDirection: 'row',
    paddingTop: 7 * d.ratioH,
    paddingBottom: 7 * d.ratioH,
  },
  icon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textIcon: {
    flex: 4,
    alignItems: 'flex-start',
    paddingLeft: 10 * d.ratioW,
    flexDirection: 'row',
  },
  viewText: {
    flex: 4,
  },
  txt_nd: {
    color: ColorTheme.black,
    fontSize: FontSize.medium - 1,
  },
  button: {
    flex: 1,
  },
});
