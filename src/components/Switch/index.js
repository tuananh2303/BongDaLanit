import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import SwitchRow1 from '../../components/SwitchRow1';

class Switch extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.Row}>
        <View style={styles.icon}>
          <Icon name={this.props.icon} size={23} />
        </View>
        <View style={styles.textIcon}>
          <View style={styles.viewText}>
            <Text style={styles.txt_nd}>{this.props.title}</Text>
          </View>
          <View style={styles.button}>
            <SwitchRow1
              value={this.props.value}
              toggleSwitch1={() => this.props.toggleSwitch1()}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default Switch;
