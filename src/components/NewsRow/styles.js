import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginLeft: 3 * d.ratioW,
    marginRight: 3 * d.ratioW,
    paddingTop: 5 * d.ratioH,
    paddingBottom: 5 * d.ratioH,
  },
  viewTitle: {
    justifyContent: 'center',
    paddingLeft: 10 * d.ratioW,
    paddingRight: 5 * d.ratioW,
  },
  title: {
    color: ColorTheme.black,
    fontSize: FontSize.medium,
  },
  subTitle: {
    color: ColorTheme.gray500,
    fontSize: FontSize.medium - 3,
  },

  viewID: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 30 * d.ratioH,
    width: 30 * d.ratioW,
    borderRadius: 15,
    backgroundColor: ColorTheme.red500,
    marginLeft: 10 * d.ratioW,
  },
  id: {
    color: ColorTheme.white,
    fontSize: FontSize.medium - 3,
  },
});
