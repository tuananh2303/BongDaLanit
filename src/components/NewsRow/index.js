import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import styles from './styles';

class NewsRow extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.viewID}>
          <Text style={styles.id}>{this.props.data.id}</Text>
        </View>
        <View style={styles.viewTitle}>
          <Text style={styles.title}>{this.props.data.title}</Text>
          <Text style={styles.subTitle}>{this.props.data.sub_title}</Text>
        </View>
      </View>
    );
  }
}

export default NewsRow;
