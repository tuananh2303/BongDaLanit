import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import Images from '../../themes/Images';
import {
  EVENT_ASSIT,
  EVENT_CLEARENCE_OFF_THE_LINE,
  EVENT_ERROR_LEAD_TO_GOAL,
  EVENT_FOUL_LEAD_TO_PENALTY,
  EVENT_GOAL,
  EVENT_LAST_MAN_TACKLE,
  EVENT_MAN_OF_THE_MATCH,
  EVENT_MARK,
  EVENT_OWN_GOAL,
  EVENT_PENALTY_MISSED,
  EVENT_PENALTY_SAVED,
  EVENT_PENALTY_SCORED,
  EVENT_RED_CARD,
  EVENT_SECOND_YELLOW_CARD,
  EVENT_SHOT_ON_POST,
  EVENT_SUB,
  EVENT_SUB_IN,
  EVENT_SUB_OUT,
  EVENT_YELLOW_CARD,
} from '../../constants/MatchEventTypes';

const TEAM_HOME = 0;
const TEAM_AWAY = 1;

class Match1Row extends PureComponent {
  state = {};

  componentDidMount() {
    // console.log('match1 row', this.props.data);
  }

  eventGoal = (left, middle, right) => {
    return (
      <View style={styles.ViewItem}>
        <View style={[styles.ViewItemName, { justifyContent: 'flex-end' }]}>
          {left}
        </View>
        <View>{middle}</View>
        <View style={[styles.ViewItemName, { justifyContent: 'flex-start' }]}>
          {right}
        </View>
      </View>
    );
  };

  getImage = type => {
    if (type == EVENT_RED_CARD) {
      return Images.theDo;
    }
    if (type == EVENT_YELLOW_CARD) {
      return Images.theVang;
    }

    if (
      type == EVENT_PENALTY_SCORED ||
      type == EVENT_OWN_GOAL ||
      type == EVENT_ERROR_LEAD_TO_GOAL ||
      type == EVENT_GOAL
    ) {
      return Images.ball;
    }

    if (type == EVENT_SUB || type == EVENT_SUB_IN || type == EVENT_SUB_OUT) {
      return Images.thayNguoi;
    }

    if (type == EVENT_SECOND_YELLOW_CARD) {
      return Images.theVangThu2;
    }

    if (type == EVENT_SHOT_ON_POST) {
      return Images.cotDoc;
    }
  };

  renderItem = (type, team) => {
    if (
      type == EVENT_GOAL ||
      type == EVENT_RED_CARD ||
      type == EVENT_YELLOW_CARD ||
      type == EVENT_PENALTY_SCORED ||
      type == EVENT_OWN_GOAL ||
      type == EVENT_ERROR_LEAD_TO_GOAL ||
      type == EVENT_SECOND_YELLOW_CARD ||
      type == EVENT_SHOT_ON_POST ||
      type == EVENT_SUB_IN ||
      type == EVENT_SUB_OUT
    ) {
      if (team == TEAM_HOME) {
        return this.eventGoal(
          <View style={styles.ViewItemDetail}>
            <Text style={styles.text}>{this.props.data[3]}</Text>
            <Image
              source={this.getImage(type)}
              style={styles.ImageMiddle}
              resizeMode="contain"
            />
          </View>,
          <Text>
            {this.props.data[0]}
            {"'"}
          </Text>,
          null,
        );
      }
      if (team == TEAM_AWAY) {
        return this.eventGoal(
          null,
          <Text>
            {this.props.data[0]}
            {"'"}
          </Text>,
          <View style={styles.ViewItemDetail}>
            <Image
              source={this.getImage(type)}
              style={styles.ImageMiddle}
              resizeMode="contain"
            />
            <Text style={styles.text}>{this.props.data[3]}</Text>
          </View>,
        );
      }
    }
    if (type == EVENT_SUB) {
      if (team == TEAM_HOME) {
        return this.eventGoal(
          <View style={styles.ViewItemDetail}>
            <View style={{ alignItems: 'flex-end' }}>
              <Text style={styles.textGreen}>{this.props.data[3]}</Text>
              <Text style={styles.textRed}>{this.props.data[5]}</Text>
            </View>

            <Image
              source={this.getImage(type)}
              style={styles.ImageMiddle}
              resizeMode="contain"
            />
          </View>,
          <Text>
            {this.props.data[0]}
            {"'"}
          </Text>,
          null,
        );
      }
      if (team == TEAM_AWAY) {
        return this.eventGoal(
          null,
          <Text>
            {this.props.data[0]}
            {"'"}
          </Text>,
          <View style={styles.ViewItemDetail}>
            <Image
              source={this.getImage(type)}
              style={styles.ImageMiddle}
              resizeMode="contain"
            />
            <View>
              <Text style={styles.textGreen}>{this.props.data[3]}</Text>
              <Text style={styles.textRed}>{this.props.data[5]}</Text>
            </View>
          </View>,
        );
      }
    }
  };

  render() {
    return (
      <View>{this.renderItem(this.props.data[6], this.props.data[1])}</View>
    );
  }
}

export default Match1Row;
