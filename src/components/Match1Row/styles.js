import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  ViewItem: {
    height: 50 * d.ratioH,
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'center',
    flex: 1,
  },
  ImageMiddle: {
    height: 30 * d.ratioW,
    width: 30 * d.ratioW,
  },
  ViewItemName: {
    flex: 4,
    flexDirection: 'row',
    marginHorizontal: 10 * d.ratioW,
  },

  ViewItemDetail: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  textGreen: {
    color: ColorTheme.green400,
    fontSize: FontSize.medium - 3,
  },

  textRed: {
    color: ColorTheme.red500,
    fontSize: FontSize.medium - 3,
  },

  text: {
    fontSize: FontSize.medium - 3,
  },
});
