import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  InfoKickRow: {
    flexDirection: 'row',
    paddingTop: 7 * d.ratioH,
    paddingBottom: 7 * d.ratioH,
    paddingLeft: 10 * d.ratioW,
  },
  icon: {
    marginTop: 10 * d.ratioH,
    height: 20 * d.ratioH,
    width: 30 * d.ratioH,
    // borderColor: 'red',
    // borderWidth: 2,
    marginLeft: 10 * d.ratioW,
  },

  content: {
    marginLeft: 10 * d.ratioW,
    // width: 310 * d.ratioW,
  },
  txt_top: {
    fontSize: 14,
    color: ColorTheme.black,
    fontWeight: '500',
  },
  txt_bottom: {
    fontSize: FontSize.small,
    paddingBottom: 5 * d.ratioH,
  },
  txt_right: {
    color: ColorTheme.black,
  },
  rightContent: {
    flex: 1,
    // backgroundColor: 'red',
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: 20 * d.ratioW,
  },
});
