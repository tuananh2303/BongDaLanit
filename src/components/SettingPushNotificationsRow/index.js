import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { ColorTheme } from '../../themes';
import styles from './styles';
import { Images } from '../../themes';

class SettingPushNotificationsRow extends PureComponent {
  state = {};

  render() {
    return (
      <View
        style={styles.InfoKickRow}
        // onPress={() => this.props.onPressTeamNotification()}
      >
        <Image source={Images.england} style={styles.icon} />

        <View style={styles.content}>
          <Text style={styles.txt_top}>{this.props.title}</Text>
          <Text>{this.props.sub_title}</Text>
        </View>
        <View style={styles.rightContent}>
          <Icon name="md-more" size={22} color={ColorTheme.Teal900} />
        </View>
      </View>
    );
  }
}

export default SettingPushNotificationsRow;
