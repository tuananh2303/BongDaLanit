import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import { ColorTheme } from '../../themes';

class IconBackCustom extends PureComponent {
  state = {};

  getColor = () => {
    if (this.props.settingReducer.data.theme == 1) {
      return ColorTheme.Teal900;
    }
    return ColorTheme.white;
  };

  render() {
    return <Icon name="md-arrow-back" size={22} color={this.getColor()} />;
  }
}

const mapStateToProps = state => ({
  settingReducer: state.settingReducer,
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(IconBackCustom);
