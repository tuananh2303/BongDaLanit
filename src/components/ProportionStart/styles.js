import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  ProportionWinChart: {
    flexDirection: 'row',
    paddingTop: 10 * d.ratioH,
    marginLeft: 70 * d.ratioW,
    // backgroundColor: 'white',
    height: 80 * d.ratioH,
  },
  ProportionWinChartColumn: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  team1: {
    width: 85 * d.ratioW,
    marginRight: 5 * d.ratioW,
    height: 50 * d.ratioH,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: ColorTheme.gray500,
    borderRadius: 10,
    borderWidth: 1,
  },
});
