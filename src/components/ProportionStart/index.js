import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import ProportionWinChart from '../ProportionWinChart';
class ProportionStart extends PureComponent {
  state = { count: 0 };

  onPress = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  row = team => {
    return (
      <TouchableOpacity
        style={styles.ProportionWinChartColumn}
        onPress={() => this.onPress()}
      >
        <View style={styles.team1}>
          <Text>{team}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  rederRow = count => {
    if (count === 0) {
      return (
        <View style={styles.ProportionWinChart}>
          {this.row(1)}
          {this.row('X')}
          {this.row(2)}
        </View>
      );
    }
    if (count === 1) {
      return <ProportionWinChart chartTeam1="23" chartX="15" chartTeam2="62" />;
    }
  };

  render() {
    // console.log(this.state.count);

    return <View>{this.rederRow(this.state.count)}</View>;
  }
}

export default ProportionStart;
