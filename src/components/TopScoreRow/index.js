import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Images } from '../../themes';

class InfoKickRow extends PureComponent {
  state = {};

  render() {
    return (
      <TouchableOpacity
        style={styles.InfoKickRow}
        // onPress={() => this.props.onPressPlayer()}
      >
        <Image source={Images.hedera} style={styles.icon} />

        <View style={styles.content}>
          <Text style={styles.txt_top}>{this.props.title}</Text>
          <Text>{this.props.sub_title}</Text>
        </View>
        <View style={styles.rightContent}>
          <Text style={styles.txt_right}>{this.props.goals}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default InfoKickRow;
