import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import LanguageVI from '../../constants/Language';

class NameTeamTableScroce extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.nameteam}>
        <View style={styles.viewSTT}>
          <Text style={styles.stt}>{this.props.leaghName}</Text>
        </View>
        <View style={styles.viewMatch}>
          <Text style={styles.textMatch}>{LanguageVI.PLD}</Text>
        </View>
        <View style={styles.viewMatch}>
          <Text style={styles.textMatch}>+/-</Text>
        </View>
        <View style={styles.viewMatch}>
          <Text style={styles.textMatch}>{LanguageVI.PTS}</Text>
        </View>
      </View>
    );
  }
}

export default NameTeamTableScroce;
