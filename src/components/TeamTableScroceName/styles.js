import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  nameteam: {
    height: 50 * d.ratioH,
    flexDirection: 'row',
    paddingTop: 10 * d.ratioH,
    // justifyContent: 'center',
    // alignItems: 'center',
    // backgroundColor: 'yellow',
  },
  stt: {
    fontSize: 16,
    color: ColorTheme.black,
  },

  viewSTT: {
    width: 200 * d.ratioW,
    // backgroundColor: 'red',
    flexDirection: 'row',
    paddingLeft: 20 * d.ratioW,
    // justifyContent: 'center',
    alignItems: 'center',
  },

  textMatch: {
    fontSize: 10,
  },

  viewMatch: {
    width: 40 * d.ratioW,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: 'blue',
  },
});
