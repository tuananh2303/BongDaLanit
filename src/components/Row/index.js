import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Switch, Image } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

import SwitchRow1 from '../../components/SwitchRow1';

const row = (icon, text) => (
  <View style={styles.Row}>
    <View style={styles.icon}>
      <Icon name={icon} size={23} />
    </View>
    <View style={styles.text}>
      <Text style={styles.txt_nd}>{text}</Text>
    </View>
  </View>
);

const rowIcon = (icon, text, value) => (
  <View style={styles.Row}>
    <View style={styles.icon}>
      <Icon name={icon} size={23} />
    </View>
    <View style={styles.textIcon}>
      <View style={styles.viewText}>
        <Text style={styles.txt_nd}>{text}</Text>
      </View>
      <View style={styles.button}>
        <SwitchRow1
          value={value}
          // toggleSwitch1={() => this.props.toggleSwitch1()}
        />
      </View>
    </View>
  </View>
);

const rowsubIcon = (icon, text, textsub) => (
  <View style={styles.Row}>
    <View style={styles.icon}>
      <Icon name={icon} size={23} />
    </View>
    <View style={styles.textIcon}>
      <View style={styles.viewText}>
        <View>
          <Text style={styles.txt_nd}>{text}</Text>
        </View>
        <View>
          <Text style={styles.txt_bottom}>{textsub}</Text>
        </View>
      </View>
      <View style={styles.button}>
        <SwitchRow1 />
      </View>
    </View>
  </View>
);

const rowsub = (icon, text, textsub) => (
  <View style={styles.Row}>
    <View style={styles.icon}>
      <Icon name={icon} size={23} />
    </View>
    <View style={styles.text}>
      <View>
        <Text style={styles.txt_nd}>{text}</Text>
      </View>
      <View>
        <Text style={styles.txt_bottom}>{textsub}</Text>
      </View>
    </View>
  </View>
);

const rowsubImg = (image, text, textsub) => (
  <View style={styles.Row}>
    <View style={styles.icon}>
      <Image source={image} style={styles.image} />
    </View>
    <View style={styles.text}>
      <View>
        <Text style={styles.txt_nd}>{text}</Text>
      </View>
      <View>
        <Text style={styles.txt_bottom}>{textsub}</Text>
      </View>
    </View>
  </View>
);

const title = data => {
  return (
    <View style={styles.title}>
      <Text style={styles.txt_title}>{data}</Text>
    </View>
  );
};

export { row, rowsub, title, rowsubImg, rowIcon, rowsubIcon };
