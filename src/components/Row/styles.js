import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    //    flex:1
    borderBottomColor: ColorTheme.white2,
    borderBottomWidth: 1 * d.ratioH,
  },
  Row: {
    flexDirection: 'row',
    paddingTop: 7 * d.ratioH,
    paddingBottom: 7 * d.ratioH,
  },
  icon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  text: {
    flex: 4,
    alignItems: 'flex-start',
    paddingLeft: 10 * d.ratioW,
  },
  textIcon: {
    flex: 4,
    alignItems: 'flex-start',
    paddingLeft: 10 * d.ratioW,
    flexDirection: 'row',
  },
  viewText: {
    flex: 4,
  },
  button: {
    flex: 1,
  },
  image: {
    height: 30 * d.ratioH,
    width: 30 * d.ratioW,
  },
  txt_nd: {
    color: ColorTheme.black,
    fontSize: 14,
  },
  txt_bottom: {
    fontSize: 10,
  },
  help: {
    paddingTop: 20 * d.ratioH,
    borderBottomColor: ColorTheme.white2,
    borderBottomWidth: 1 * d.ratioH,
    paddingBottom: 10 * d.ratioH,
  },
  title: {
    paddingLeft: 15 * d.ratioW,
  },
  txt_title: {
    fontWeight: '500',
    fontSize: 12,
  },
  Add: {
    paddingTop: 20 * d.ratioH,
    paddingBottom: 10 * d.ratioH,
  },
});
