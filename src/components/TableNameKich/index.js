import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import TableNameKichTitle from '../TableNameKichTitle';
import TableNameKichRow from '../TableNameKichRow';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

class TableNameKich extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.container}>
        <TableNameKichTitle />
        <TableNameKichRow />
        <TableNameKichRow />
      </View>
    );
  }
}

export default TableNameKich;
