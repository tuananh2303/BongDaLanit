import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

class DetailsFinesRowColor extends PureComponent {
  state = {};

  componentDidMount() {
    // console.log(this.props.team1 / (this.props.team1 + this.props.team2));
  }

  render() {
    return (
      <View>
        <View style={styles.holdBallColor}>
          <View
            style={{
              width:
                (d.windowSize.width * this.props.team1) /
                (this.props.team1 + this.props.team2),
            }}
          />
          <View
            style={{
              width:
                (d.windowSize.width * this.props.team2) /
                (this.props.team1 + this.props.team2),
              backgroundColor: this.props.color,
            }}
          />
        </View>
        <View style={styles.row}>
          <View style={styles.teamLeft}>
            <Text style={styles.txt_teamLeft}>{this.props.team1}</Text>
          </View>
          <View style={styles.teamRight}>
            <Text style={styles.txt_teamRight}>{this.props.team2}</Text>
          </View>
        </View>
        <View style={styles.tittle}>
          <Text style={styles.txt_tittle}>{this.props.content}</Text>
        </View>
      </View>
    );
  }
}

export default DetailsFinesRowColor;
