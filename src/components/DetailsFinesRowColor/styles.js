import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  holdBallColor: {
    flexDirection: 'row',
    height: 50 * d.ratioH,
    borderBottomColor: ColorTheme.white2,
    borderBottomWidth: 1,
    flex: 1,
  },
  teamLeftColor: {
    flex: 11 / 21,
  },
  teamRightColor: {
    flex: 10 / 21,
    backgroundColor: ColorTheme.blue,
  },
  row: {
    flexDirection: 'row',
    height: 50 * d.ratioH,
    alignItems: 'center',
    paddingLeft: 10 * d.ratioW,
    paddingRight: 10 * d.ratioW,
    borderBottomColor: ColorTheme.white2,
    borderBottomWidth: 1 * d.ratioH,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
  },

  txt_teamLeft: {
    color: ColorTheme.black,
    fontSize: 16,
  },
  teamLeft: {
    flex: 1,
  },
  teamRight: {
    flex: 1,
    alignItems: 'flex-end',
  },
  txt_teamRight: {
    color: ColorTheme.black,
    fontSize: 16,
  },
  tittle: {
    height: 50 * d.ratioH,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
  },
  txt_tittle: {
    fontSize: 13,
    color: ColorTheme.black,
  },
});
