import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

class InfoTeamRow extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.FootballMatchInfoRow}>
       
        <View style={styles.content}>
          <Text style={styles.txt_top}>Hôm qua 20:15</Text>
          <Text style={styles.txt_bottom}>Bóng lăn</Text>
        </View>
      </View>
    );
  }
}

export default InfoTeamRow;
