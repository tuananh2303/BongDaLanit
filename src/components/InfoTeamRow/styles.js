import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  FootballMatchInfoRow: {
    flexDirection: 'row',
    paddingTop: 10 * d.ratioH,
  },
  content: {
    marginLeft: 70 * d.ratioW,
    borderBottomColor: ColorTheme.white2,
    borderBottomWidth: 1,
    width: 310 * d.ratioW,
  },
  txt_top: {
    fontSize: 14,
    color: ColorTheme.black,
    fontWeight: '500',
  },
  txt_bottom: {
    fontSize: 10,
    paddingBottom: 5 * d.ratioH,
  },
});
