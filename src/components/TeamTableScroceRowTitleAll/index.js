import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { ColorTheme } from '../../themes';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import HeaderBubbleWinLose from '../HeaderBubbleWinLose';
const WinLose = ['win', 'lose', 'lose', ''];
class TeamTableScroceRowAll1 extends PureComponent {
  state = {};

  subTitle = title => {
    return (
      <View style={styles.view_subTitle}>
        <Text style={{ fontSize: 10 }}>{title}</Text>
      </View>
    );
  };

  getStylesForBubble = condition => {
    if (condition === 1) {
      return styles.ViewStatus1;
    }
    if (condition === 3) {
      return styles.ViewStatus3;
    }

    return styles.ViewStatus2;
  };

  getStylesForBubbleText = condition => {
    if (condition === 2) {
      return styles.sttNumSimple;
    }

    return styles.sttNum;
  };

  upDown = type => {
    if (type === 'up') {
      return (
        <Icon name="md-arrow-dropup" size={20} color={ColorTheme.green500} />
      );
    }
    if (type === 'down') {
      return (
        <Icon name="md-arrow-dropdown" size={20} color={ColorTheme.red500} />
      );
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.view_title}>
          <View style={styles.title}>
            <Text style={styles.txt_title}>La Liga</Text>
          </View>
        </View>
        {this.subTitle('Trận')}
        {this.subTitle('W')}
        {this.subTitle('D')}
        {this.subTitle('L')}
        {this.subTitle('GB')}
        {this.subTitle('LL')}
        {this.subTitle('+/-')}
        {this.subTitle('Điểm')}
        <View style={styles.view_titleEnd}>
          <Text>Phong độ</Text>
        </View>
      </View>
    );
  }
}

export default TeamTableScroceRowAll1;
