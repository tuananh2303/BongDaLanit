import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

const defaultHeight = 640 * d.ratioH;
const defaultWidth = 640 * d.ratioW;
export default StyleSheet.create({
  container: {
    height: defaultHeight,
    width: defaultWidth,
    flexDirection: 'row',
    transform: [{ rotate: '90deg' }],
  },
  view_title: {
    flex: 1.75,
    // backgroundColor: 'green',
    // transform: [{ rotate: '90deg' }],
    // justifyContent: 'center',
    // alignItems: 'center',
    // justifyContent: 'center',
    // alignItems: 'center',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
  },
  title: {
    paddingLeft: 15 * d.ratioW,
  },
  view_subTitle: {
    flex: 0.25,
    // backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
    // transform: [{ rotate: '90deg' }],
  },
  txt_title: {
    fontSize: FontSize.medium + 1,
    fontWeight: '500',
    color: ColorTheme.black,
  },
  view_titleEnd: {
    flex: 1,
    // backgroundColor: 'rgb(63,81,181)',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    // transform: [{ rotate: '90deg' }],
    paddingLeft: 5 * d.ratioW,
    paddingRight: 5 * d.ratioW,
  },
  stt: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  name: {
    flex: 3,
    // justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
});
