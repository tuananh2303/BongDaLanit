import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  InfoKickRow: {
    // flexDirection: 'row',
    paddingTop: 8 * d.ratioH,
  },

  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 5 * d.ratioH,
  },
  txt_top: {
    fontSize: 12,
    color: ColorTheme.gray500,
    marginLeft: 10 * d.ratioW,
  },
  txt_bottom: {
    fontSize: 15,
    color: ColorTheme.black,
    marginRight: 10 * d.ratioW,
  },
});
