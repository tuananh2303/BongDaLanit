import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import CardMatch from '../CardMatch';
import { Images } from '../../themes';
import { converTime } from '../../utilities/MomentTime';

class FixtureRow extends PureComponent {
  state = {
    data: [],
  };

  componentDidMount() {
    const itemToArr = {
      match_id: this.props.data[0], // array[0] la id cua tran dau
      match_date: this.props.data[2], // array[6] la thoi gian bat dau tran dau
      status: 10, // status doi bong ow vi tri thu [9]
      team1_name: this.props.teams[1], // trong array[4] trong matches la id cua team , array[1] trong team la team_name
      team2_name: this.props.teams[4],
      score1: this.props.data[3], // ty so doi 1 la array[10]
      score2: this.props.data[4], // ty so doi 2 la array[11]
      team1_id: '',
      team2_id: '',
      team1_image: '', // trong array[4] trong matches la id cua team , array[2] trong team la anh
      team2_image: '',
    };
    this.setState({
      data: itemToArr,
    });
  }

  render() {
    return (
      <View style={styles.InfoKickRow}>
        <View style={styles.content}>
          <Text style={styles.txt_top}>{converTime(this.props.data[2])}</Text>
          <Text style={styles.txt_bottom}>{this.props.data[1]}</Text>
        </View>

        <CardMatch
          onPressMatch={data => this.props.onPressMatch(data)}
          data={this.state.data}
        />
      </View>
    );
  }
}

export default FixtureRow;
