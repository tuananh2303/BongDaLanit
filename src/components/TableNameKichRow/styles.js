import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  TableNameKichRow: {
    flexDirection: 'row',
    paddingTop: 5 * d.ratioH,
  },
  icon: {
    width: 60 * d.ratioW,
    justifyContent: 'center',
    alignItems: 'center',
  },
  nameKich: {
    marginLeft: 10 * d.ratioW,
    width: 310 * d.ratioW,
  },
  txt_top: {
    fontSize: 16,
    color: ColorTheme.black,
    fontWeight: '500',
  },
  txt_bottom: {
    fontSize: 16,
    paddingBottom: 5 * d.ratioH,
  },
});
