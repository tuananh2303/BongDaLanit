import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

class TableNameKichRow extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.TableNameKichRow}>
        <View style={styles.icon}>
          <Icon name="ios-timer" size={30} />
        </View>
        <View style={styles.nameKich}>
          <Text style={styles.txt_top}>1. Toni Lato</Text>
          <Text style={styles.txt_bottom}>Hậu vệ</Text>
        </View>
      </View>
    );
  }
}

export default TableNameKichRow;
