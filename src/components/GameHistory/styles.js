import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    backgroundColor: ColorTheme.white2,
    borderRadius: 10,
  },
  row: {
    margin: 5,
    backgroundColor: ColorTheme.white,
    borderRadius: 10,
    elevation: 3,
    overflow: 'hidden',
  },
});
