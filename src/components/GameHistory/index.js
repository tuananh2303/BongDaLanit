import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import GameHistoryRow from '../GameHistoryRow';
import data from './GameHistoryDataFake';
import LanguageVI from '../../constants/Language';

class GameHistory extends PureComponent {
  state = {};

  componentDidMount() {
    // console.log(this.props.data.data);
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => {
            if (item != null) {
              return (
                <View style={styles.row}>
                  <GameHistoryRow data={item} />
                </View>
              );
            }
          }}
        />
      </View>
    );
  }
}

export default GameHistory;
