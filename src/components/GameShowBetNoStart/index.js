import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  FlatList,
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Images, ColorTheme } from '../../themes';
import LanguageVI from '../../constants/Language';
import GameShowBetNoStartRow from '../GameShowBetNoStartRow';

class GameShowBetNoStart extends PureComponent {
  state = {
    homeWin: [],
    draw: [],
    awayWin: [],
  };

  componentDidMount() {
    const homeWin = [];
    const draw = [];
    const awayWin = [];
    this.props.data.map(item => {
      if (item.homeScore > item.awayScore) {
        homeWin.push(item);
      }
      if (item.homeScore == item.awayScore) {
        draw.push(item);
      }
      if (item.homeScore < item.awayScore) {
        awayWin.push(item);
      }
    });

    homeWin.sort((a, b) => {
      return parseInt(b.odds, 10) - parseInt(a.odds, 10);
    });
    draw.sort((a, b) => {
      return parseInt(b.odds, 10) - parseInt(a.odds, 10);
    });
    awayWin.sort((a, b) => {
      return parseInt(b.odds, 10) - parseInt(a.odds, 10);
    });

    this.setState({
      homeWin,
      draw,
      awayWin,
    });
  }

  renderTitleTable = title => {
    return (
      <View style={styles.petHomeWin}>
        <View style={styles.petHomeLeft}>
          <Text style={styles.txt_title}>{title}</Text>
        </View>

        <View style={styles.petHomeRight}>
          <Text>{LanguageVI.Bet}</Text>
        </View>

        <View style={styles.petHomeRight1}>
          <Text>{LanguageVI.Reward}</Text>
        </View>
      </View>
    );
  };

  rederTable = () => {
    return (
      <View>
        <View style={styles.tableHomeWin}>
          <View>{this.renderTitleTable(LanguageVI.HomeWin)}</View>
          {/* <View>{this.renderTitleTable(LanguageVI.OutcomeOnly)}</View> */}
          {this.state.homeWin.map((value, key) => (
            <View key={key.toString()}>
              <GameShowBetNoStartRow
                data={value}
                bet={this.props.bet}
                callback={data => this.props.callback(data)}
              />
            </View>
          ))}
        </View>

        <View style={styles.tableHomeWin}>
          <View>{this.renderTitleTable(LanguageVI.Draw)}</View>
          {this.state.draw.map((value, key) => (
            <View key={key.toString()}>
              <GameShowBetNoStartRow
                data={value}
                bet={this.props.bet}
                callback={data => this.props.callback(data)}
              />
            </View>
          ))}
          {/* <View>{this.renderTitleTable(LanguageVI.OutcomeOnly)}</View> */}
        </View>

        <View style={styles.tableHomeWin}>
          <View>{this.renderTitleTable(LanguageVI.AwayWin)}</View>
          <View>
            {this.state.awayWin.map((value, key) => (
              <View key={key.toString()}>
                <GameShowBetNoStartRow
                  data={value}
                  bet={this.props.bet}
                  callback={data => this.props.callback(data)}
                />
              </View>
            ))}
            {/* {this.renderTitleTable(LanguageVI.OutcomeOnly)} */}
          </View>
        </View>
      </View>
    );
  };

  render() {
    return <View>{this.rederTable(this.props.data, this.props.bet)}</View>;
  }
}

export default GameShowBetNoStart;
