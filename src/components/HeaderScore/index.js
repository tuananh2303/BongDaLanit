import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { converTimeShort } from '../../utilities/MomentTime';

class Home extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.viewMain}>
        <Text style={styles.score}>
          {this.props.scoreLeft} - {this.props.scoreRight}
        </Text>
        <Text style={styles.date}>{converTimeShort(this.props.date)}</Text>
      </View>
    );
  }
}

export default Home;
