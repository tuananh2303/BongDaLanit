import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  viewMain: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  score: {
    color: ColorTheme.white,
    fontSize: FontSize.extraLarge + 5,
  },
  date: {
    color: ColorTheme.white,
    fontSize: FontSize.medium,
  },
});
