import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Images, ColorTheme } from '../../themes';
import LanguageVI from '../../constants/Language';

class GameShowBet extends PureComponent {
  state = {};

  componentDidMount() {
    // console.log(this.props.data);
  }

  renderTitleTable = (title, value) => {
    return (
      <View style={styles.petHomeWin}>
        <View style={styles.petHomeLeft}>
          <Text style={styles.txt_title}>{title}</Text>
        </View>

        <View style={styles.petHomeRight}>
          <Text>{value}</Text>
        </View>
      </View>
    );
  };

  renderRow = data => {
    return (
      <View style={styles.petHomeWin}>
        <View style={styles.petHomeLeft}>
          <View style={styles.table}>
            <View>
              <Text>{data.homeScore}</Text>
            </View>
            <View style={styles.tableMid}>
              <Text>:</Text>
            </View>
            <View>
              <Text>{data.awayScore}</Text>
            </View>
          </View>
        </View>

        <View style={styles.petHomeRight}>
          <Text>6.4k</Text>
        </View>
      </View>
    );
  };

  rederTable = data => {
    return (
      <View>
        <View style={styles.title}>
          <View>
            <Text>This table shows you potentail returns for each</Text>
          </View>
          <View>
            <Text>correct score and out come with your current stake</Text>
          </View>
          <View>
            <Text>Use it to make a better prediction</Text>
          </View>
        </View>

        <View style={styles.tableHomeWin}>
          <View>{this.renderTitleTable(LanguageVI.HomeWin, '6.4k')}</View>
          <View>
            {data.homewin.map((value, key) => (
              <View key={key.toString()}>{this.renderRow(value)}</View>
            ))}
            {this.renderTitleTable(LanguageVI.OutcomeOnly, '-')}
          </View>
        </View>

        <View style={styles.tableHomeWin}>
          <View>{this.renderTitleTable(LanguageVI.AwayWin, '1.9k')}</View>
          <View>
            {data.awaywin.map((value, key) => {
              return <View key={key.toString()}>{this.renderRow(value)}</View>;
            })}
            {this.renderTitleTable(LanguageVI.OutcomeOnly, '-')}
          </View>
        </View>

        <View style={styles.tableHomeWin}>
          <View>{this.renderTitleTable(LanguageVI.Draw, '1.9k')}</View>
          <View>
            {data.draw.map((value, key) => {
              return <View key={key.toString()}>{this.renderRow(value)}</View>;
            })}
            {this.renderTitleTable(LanguageVI.OutcomeOnly, '-')}
          </View>
        </View>
      </View>
    );
  };

  render() {
    return <View>{this.rederTable(this.props.data)}</View>;
  }
}

export default GameShowBet;
