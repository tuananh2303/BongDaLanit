import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  title: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10 * d.ratioH,
  },
  petHomeWin: {
    flexDirection: 'row',
    marginTop: 5 * d.ratioH,
  },
  petHomeLeft: {
    flex: 1,
    alignItems: 'center',
  },
  petHomeRight: {
    flex: 1,
    alignItems: 'center',
  },
  table: {
    height: 20 * d.ratioH,
    width: 70 * d.ratioW,
    flexDirection: 'row',
    backgroundColor: ColorTheme.gray300,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  tableMid: {
    marginLeft: 5 * d.ratioW,
    marginRight: 5 * d.ratioW,
  },
  txt_title: {
    fontSize: FontSize.medium,
    color: ColorTheme.black,
  },
  tableHomeWin: {
    backgroundColor: ColorTheme.white,
    paddingBottom: 15 * d.ratioH,
    borderBottomColor: ColorTheme.gray300,
    borderBottomWidth: 1,
  },
});
