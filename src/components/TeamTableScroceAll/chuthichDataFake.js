const data1 = {
  data: [
    {
      id: 1,
      color: 'xanh',
      chuthich: 'Champions League',
    },
    {
      id: 2,
      color: 'green900',
      chuthich: 'UEFA Europa League',
    },
    {
      id: 3,
      color: 'LightBlue500',
      chuthich: 'Vòng loại UEFA Europa League',
    },
    {
      id: 4,
      color: 'red500',
      chuthich: 'Playoff trụ hạng',
    },
    {
      id: 5,
      color: 'red900',
      chuthich: 'Xuống hạng',
    },
  ],
};

export default data1;
