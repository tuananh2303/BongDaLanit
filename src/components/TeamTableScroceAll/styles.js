import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';
const defaultHeight = 640 * d.ratioH;
const defaultWidth = 640 * d.ratioW;
export default StyleSheet.create({
  //   container: {
  //     flex: 1,
  //     //     transform: [{ rotate: '90deg' }],
  //   },
  //   title: {
  //     height: 640,
  //     width: 640,
  //     transform: [{ rotate: '90deg' }],
  //     flexDirection: 'row',
  //     alignItems: 'center',
  //   },
  //   subTitle1: {
  //     flex: 1.75,
  //     // backgroundColor: 'tomato',
  //     alignItems: 'center',
  //   },
  //   subTitle: {
  //     flex: 0.5,
  //     // backgroundColor: 'green',
  //     alignItems: 'center',
  //   },
  //   subTitle2: {
  //     flex: 0.75,
  //     // backgroundColor: 'red',
  //     alignItems: 'center',
  //   },
  //   txt_subFootball: {
  //     fontSize: FontSize.medium,
  //     fontWeight: '500',
  //     color: ColorTheme.black,
  //   },
  //   txt_subTitle: {
  //     fontSize: FontSize.small,
  //     fontWeight: '500',
  //   },
  //   txt_end: {
  //     fontSize: FontSize.small,
  //     fontWeight: '500',
  //   },
  body: {
    // flexDirectbodyon: 'row',
    alignItems: 'flex-end',
  },
  header: {
    width: 45 * d.ratioW,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    // width: 'auto',
    width: 45 * d.ratioW,
    // height: defaultHeight,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column-reverse',
    // backgroundColor: 'tomato',
  },
  content: {
    width: 45 * d.ratioW,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
