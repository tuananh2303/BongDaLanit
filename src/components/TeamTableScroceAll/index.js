import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  ScrollView,
} from 'react-native';
import TeamTableScroceRowAll from '../TeamTableScroceRowAll';
import TeamTableScroceRowAll1 from '../TeamTableScroceRowAll1';
import TeamTableScroceRowTitleAll from '../TeamTableScroceRowTitleAll';
import TeamTableScroceAllFooter from '../TeamTableScroceAllFooter';
import Icon from 'react-native-vector-icons/Ionicons';
import data1 from './chuthichDataFake';
import styles from './styles';
class TeamTableScroceAll extends PureComponent {
  state = {};

  render() {
    console.log(data1.data);

    return (
      <ScrollView style={styles.container}>
        <View style={styles.body}>
          <FlatList
            ListFooterComponent={() => (
              <View style={styles.header}>
                <TeamTableScroceRowTitleAll />
              </View>
            )}
            ListHeaderComponent={() => (
              <View style={{ flexDirection: 'row' }}>
                {data1.data.map((value, key) => (
                  <View style={styles.content}>
                    <TeamTableScroceAllFooter
                      color={value.color}
                      text={value.chuthich}
                    />
                  </View>
                ))}
                {/* <View style={styles.content}>
                  <TeamTableScroceAllFooter
                    color="xanh"
                    text="Champions League"
                  />
                </View>
                <View style={styles.content}>
                  <TeamTableScroceAllFooter
                    color="green900"
                    text="UEFA Europa League"
                  />
                </View>
                <View style={styles.content}>
                  <TeamTableScroceAllFooter
                    color="LightBlue500"
                    text="Vòng loại UEFA Europa League"
                  />
                </View>
                <View style={styles.content}>
                  <TeamTableScroceAllFooter
                    color="red500"
                    text="Playoff trụ hạng"
                  />
                </View>
                <View style={styles.content}>
                  <TeamTableScroceAllFooter
                    color="red900"
                    text="Xuống hạng"
                  />
                </View> */}
              </View>
            )}
            showsVerticalScrollIndicator={false}
            horizontal={true}
            data={this.props.data.data.reverse()}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => (
              <View style={styles.content}>
                <TeamTableScroceRowAll1 data={item} />
              </View>
            )}
          />
        </View>
      </ScrollView>
    );
  }
}

export default TeamTableScroceAll;
