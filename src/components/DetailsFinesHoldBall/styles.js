import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  holdBallColor: {
    flexDirection: 'row',
    height: 180 * d.ratioH,
    borderBottomColor: ColorTheme.white2,
    borderBottomWidth: 1 * d.ratioH,
  },

  holdBall: {
    flexDirection: 'row',
    height: 180 * d.ratioH,
    borderBottomColor: ColorTheme.white2,
    borderBottomWidth: 1 * d.ratioH,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
  },
  txt_teamLeft: {
    color: ColorTheme.black,
    fontSize: 36,
    marginRight: 20 * d.ratioW,
  },
  txt_teamRight: {
    color: ColorTheme.black,
    fontSize: 36,
    marginLeft: 20 * d.ratioW,
  },
  txt_holdBall: {
    fontSize: 13,
    color: ColorTheme.black,
  },
});
