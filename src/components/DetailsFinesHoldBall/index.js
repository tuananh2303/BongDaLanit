import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { ColorTheme } from '../../themes';
import LanguageVI from '../../constants/Language';

class DetailsFinesHoldBall extends PureComponent {
  state = {};

  render() {
    return (
      <View>
        <View style={styles.holdBallColor}>
          <View style={{ width: (this.props.team1 * 360) / 100 }} />
          <View
            style={{
              width: (this.props.team2 * 360) / 100,
              backgroundColor: this.props.color,
            }}
          />
        </View>
        <View style={styles.holdBall}>
          <Text style={styles.txt_teamLeft}>
            {this.props.team1}
            {LanguageVI.Percent}
          </Text>
          <Text style={styles.txt_holdBall}>{this.props.content}</Text>
          <Text style={styles.txt_teamRight}>
            {this.props.team2}
            {LanguageVI.Percent}
          </Text>
        </View>
      </View>
    );
  }
}

export default DetailsFinesHoldBall;
