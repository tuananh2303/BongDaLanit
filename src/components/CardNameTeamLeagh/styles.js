import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  nameteam: {
    height: 50 * d.ratioH,
    flexDirection: 'row',
  },
  iconteam: {
    width: 70 * d.ratioW,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    height: 30 * d.ratioH,
    width: 30 * d.ratioH,
  },
  team: {
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  txt_team: {
    fontSize: 16,
    color: ColorTheme.black,
    fontWeight: '400',
  },
});
