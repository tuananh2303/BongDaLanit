import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Images } from '../../themes';
import { URL_IMAGE } from '../../constants/URL';

class CardNameTeamLeagh extends PureComponent {
  state = {};

  componentDidMount() {
    // console.log(this.props.image);
  }

  render() {
    return (
      <TouchableOpacity
        style={styles.nameteam}
        onPress={() => this.props.onPressTeam()}
      >
        <View style={styles.iconteam}>
          <Image
            source={
              this.props.image != null
                ? { uri: URL_IMAGE + this.props.image }
                : Images.logoTeamDefault
            }
            style={styles.icon}
            resizeMode="contain"
          />
        </View>
        <View style={styles.team}>
          <Text style={styles.txt_team}>{this.props.teamName}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default CardNameTeamLeagh;
