import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import FootballMatchInfoTitle from '../FootballMatchInfoTitle';
import FootballMatchInfoRow from '../FootballMatchInfoRow';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

class FootballMatchInfo extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.container}>
        <FootballMatchInfoTitle />
        <FootballMatchInfoRow />
        
      </View>
    );
  }
}

export default FootballMatchInfo;
