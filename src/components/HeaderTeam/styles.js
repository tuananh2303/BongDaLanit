import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  viewMain: {
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: 'blue',
    flex: 1,
  },
  logoTeam: {
    height: 50 * d.ratioH,
    width: 50 * d.ratioW,
  },
  date: {
    color: ColorTheme.white,
    fontSize: FontSize.medium,
  },
});
