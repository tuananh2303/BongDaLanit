import React, { PureComponent } from 'react';
import { View, Text, Image } from 'react-native';
// import FastImage from 'react-native-fast-image';

import styles from './styles';
import { Images } from '../../themes';
import { URL_IMAGE } from '../../constants/URL';
import FastImage from '../FastImage';

class Home extends PureComponent {
  state = {};

  componentDidMount() {
    // console.log(this.props.logoTeam.split('/').pop());
  }

  render() {
    return (
      <View style={styles.viewMain}>
        <FastImage
          linkImageDefault={Images.logoTeamDefault}
          linkImage={URL_IMAGE + this.props.logoTeam}
          style={styles.logoTeam}
          resizeMode="contain"
        />
        <Text style={styles.date}>{this.props.teamName}</Text>
      </View>
    );
  }
}

export default Home;
