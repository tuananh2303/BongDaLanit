import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

const defaultHeight = 640 * d.ratioH;
const defaultWidth = 640 * d.ratioW;
export default StyleSheet.create({
  container: {
    height: defaultHeight,
    width: defaultWidth,
    flexDirection: 'column',
  },
  viewSTT: {
    flex: 1.75,
    // backgroundColor: 'rgb(129,199,132)',
    // justifyContent: 'center',
    // alignItems: 'center',
    flexDirection: 'column',
    // marginTop: 15,
  },
  view_stt: {
    flex: 1,
    // backgroundColor: 'blue',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 5 * d.ratioH,
  },
  view_txt: {
    flex: 4,
    // backgroundColor: 'red',
    // justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'left',
    paddingTop: 70 * d.ratioH,
    // paddingTop: 700,
    // marginTop: 30,
  },
  txt_subTitle: {
    flex: 0.25,
    // backgroundColor: 'rgb(38,198,218)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txt_titleEnd: {
    flex: 1,
    // backgroundColor: 'rgb(63,81,181)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txt: {
    transform: [{ rotate: '90deg' }],
  },
  stt: {
    fontSize: 16,
    color: ColorTheme.black,
  },

  sttNum: {
    fontSize: 13,
    color: ColorTheme.white,
  },

  sttNumSimple: {
    fontSize: 16,
    color: ColorTheme.black,
  },

  //   viewSTT: {
  //     width: 200,
  //     // backgroundColor: 'red',
  //     flexDirection: 'row',
  //     paddingLeft: 20,
  //     alignItems: 'center',
  //   },

  textMatch: {
    fontSize: 10,
  },

  viewMatch: {
    width: 40 * d.ratioW,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: 'blue',
  },

  ViewStatus1: {
    backgroundColor: ColorTheme.lightGreen500,
    height: 20 * d.ratioH,
    width: 20 * d.ratioW,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    // marginRight: 10,
  },

  ViewStatus3: {
    backgroundColor: ColorTheme.red500,
    height: 20 * d.ratioH,
    width: 20 * d.ratioW,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10 * d.ratioW,
  },

  ViewStatus2: {
    // backgroundColor: ColorTheme.lightGreen500,
    height: 20 * d.ratioH,
    width: 20 * d.ratioW,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10 * d.ratioW,
  },
  lose: {
    margin: 2,
    width: 20 * d.ratioW,
    height: 20 * d.ratioH,
    backgroundColor: ColorTheme.red500,
    borderRadius: 15,
  },
  hoa: {
    margin: 2,
    width: 20 * d.ratioW,
    height: 20 * d.ratioH,
    backgroundColor: ColorTheme.gray500,
    borderRadius: 15,
  },
  win: {
    margin: 2,
    width: 20 * d.ratioW,
    height: 20 * d.ratioH,
    backgroundColor: ColorTheme.green400,
    borderRadius: 15,
  },
});
