import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import HeaderBubbleWinLose from '../HeaderBubbleWinLose';
const WinLose = ['win', 'lose', 'lose', ''];
class TeamTableScroceRowAll extends PureComponent {
  state = {};

  subTitle = title => {
    return (
      <View style={styles.txt_subTitle}>
        <Text style={[styles.txt, { fontSize: 10 }]}>{title}</Text>
      </View>
    );
  };

  getStylesForBubble = condition => {
    if (condition === 1) {
      return styles.ViewStatus1;
    }
    if (condition === 3) {
      return styles.ViewStatus3;
    }

    return styles.ViewStatus2;
  };

  getStylesForBubbleText = condition => {
    if (condition === 2) {
      return styles.sttNumSimple;
    }

    return styles.sttNum;
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.viewSTT}>
          <View style={styles.view_stt}>
            <View style={this.getStylesForBubble(this.props.data.status)}>
              <Text
                style={[
                  this.getStylesForBubbleText(this.props.data.status),
                  {
                    transform: [{ rotate: '90deg' }],
                  },
                ]}
              >
                {this.props.data.id}
              </Text>
            </View>
          </View>

          <View style={styles.view_txt}>
            <Text
              style={[
                styles.sttNumSimple,
                { transform: [{ rotate: '90deg' }] },
              ]}
            >
              {this.props.data.team_name}
            </Text>
          </View>
        </View>
        {this.subTitle(this.props.data.so_tran)}
        {this.subTitle(this.props.data.W)}
        {this.subTitle(this.props.data.D)}
        {this.subTitle(this.props.data.L)}
        {this.subTitle(this.props.data.GB)}
        {this.subTitle(this.props.data.LL)}
        {this.subTitle(this.props.data.diem_cong)}
        {this.subTitle(this.props.data.diem)}
        <View style={styles.txt_titleEnd}>
          {WinLose.map(item => (
            <HeaderBubbleWinLose type={item} isRotate={1} />
          ))}
        </View>
      </View>
    );
  }
}

export default TeamTableScroceRowAll;
