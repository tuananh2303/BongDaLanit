import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import CardNameTeamLeagh from '../CardNameTeamLeagh';
import CardMatch from '../CardMatch';

class Card extends PureComponent {
  state = {};

  componentDidMount() {
    // console.log(this.props.data.data);
  }

  render() {
    return (
      <View style={styles.container}>
        <CardNameTeamLeagh
          image={this.props.data.image}
          onPressTeam={this.props.onPressTeam}
          teamName={this.props.data.team_name}
        />
        <FlatList
          showsVerticalScrollIndicator={false}
          data={this.props.data.data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => {
            if (item != null) {
              return (
                <CardMatch
                  onPressMatch={data => this.props.onPressMatch(data)}
                  data={item}
                />
              );
            }
          }}
        />
      </View>
    );
  }
}

export default Card;
