import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    // height: 170 * d.ratioH,
    flexDirection: 'column',
    elevation: 3,
    marginTop: 5 * d.ratioW,
    marginBottom: 5 * d.ratioW,
    marginLeft: 7 * d.ratioW,
    marginRight: 7 * d.ratioW,
    backgroundColor: ColorTheme.white,
    borderRadius: 10,
    overflow: 'hidden',
  },
});
