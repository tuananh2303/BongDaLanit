import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  viewMain: {
    height: 45 * d.ratioH,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  viewMain2: {
    height: 45 * d.ratioH,
    backgroundColor: ColorTheme.Teal900,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  viewMain3: {
    height: 45 * d.ratioH,
    backgroundColor: ColorTheme.blue800,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  viewMain4: {
    height: 45 * d.ratioH,
    backgroundColor: ColorTheme.black,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  leftHeader: {
    paddingLeft: 25 * d.ratioW,
  },
  rightHeader: {
    width: 60 * d.ratioW,
    // backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: FontSize.large,
    color: ColorTheme.Teal900,
    fontWeight: '400',
  },
  title2: {
    fontSize: FontSize.large,
    color: ColorTheme.white,
    fontWeight: '400',
  },
});
