import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { ColorTheme } from '../../themes';
import { isIphoneX } from '../../utilities/device';

class Header extends PureComponent {
  state = {};

  isIphoneX;

  getStyleViewTab = () => {
    if (isIphoneX()) {
      if (this.props.settingReducer.data.theme == 2) {
        return [styles.viewMain2, { paddingTop: 30 }];
      }
      if (this.props.settingReducer.data.theme == 3) {
        return [styles.viewMain3, { paddingTop: 30 }];
      }
      if (this.props.settingReducer.data.theme == 4) {
        return [styles.viewMain4, { paddingTop: 30 }];
      }
      return [styles.viewMain, { paddingTop: 30 }];
    }
    if (this.props.settingReducer.data.theme == 2) {
      return styles.viewMain2;
    }
    if (this.props.settingReducer.data.theme == 3) {
      return styles.viewMain3;
    }
    if (this.props.settingReducer.data.theme == 4) {
      return styles.viewMain4;
    }

    if (this.props.settingReducer.data.theme == 5) {
      return [styles.viewMain4, { backgroundColor: ColorTheme.purple700 }];
    }
    return styles.viewMain;

    // return styleCustom;
  };

  getStyleTitle = () => {
    if (this.props.settingReducer.data.theme == 1) {
      return styles.title;
    }
    return styles.title2;
  };

  render() {
    return (
      <View
        style={[
          this.getStyleViewTab(),
          this.props.elevation ? { elevation: 3 } : null,
        ]}
      >
        <View style={styles.rightHeader}>{this.props.icon}</View>
        <View>
          <Text style={this.getStyleTitle()}>{this.props.title}</Text>
        </View>
        <View style={styles.rightHeader}>{this.props.rightHeader}</View>
      </View>
    );
  }
}

// export default Header;

const mapStateToProps = state => ({
  userReducer: state.userReducer,
  settingReducer: state.settingReducer,
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Header);
