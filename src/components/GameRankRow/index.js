import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';

import styles from './styles';
import LanguageVI from '../../constants/Language';
import { Images, ColorTheme } from '../../themes';
import { getIconRank } from '../../utilities/GetRankIcon';

class GameRankRow extends PureComponent {
  state = {};

  getColor = () => {
    if (this.props.settingReducer.data.theme == 2) {
      return ColorTheme.Teal900;
    }
    if (this.props.settingReducer.data.theme == 3) {
      return ColorTheme.blue800;
    }
    if (this.props.settingReducer.data.theme == 4) {
      return ColorTheme.black;
    }
    return ColorTheme.Teal900;
  };

  render() {
    const data = this.props.data;
    return (
      <View style={styles.rowRank}>
        <View style={styles.rank}>
          <Text>{data.rank}</Text>
        </View>
        <TouchableOpacity
          style={styles.avatar}
          onPress={() => this.props.onPress()}
        >
          <Image
            source={{ uri: data.avatar }}
            style={[styles.imgAvatar, { borderColor: this.getColor() }]}
          />
        </TouchableOpacity>
        <View style={styles.info}>
          <View style={styles.name}>
            <View style={styles.nameText}>
              <Text style={styles.txtFullName}>{data.fullname}</Text>
            </View>
          </View>
          <View style={styles.class}>
            <Text style={styles.txtClass} />
          </View>
          <View style={styles.pointGain}>
            <Text style={styles.txtPointGain}>
              {LanguageVI.Point}
              {data.point_gain}
              {' ('}
              {data.class}
              {')'}
            </Text>
          </View>
        </View>
        <View style={styles.nameIconClass}>
          <Image
            source={getIconRank(data.class)}
            style={styles.imgClass}
            resizeMode="contain"
          />
        </View>
      </View>
    );
  }
}

// export default GameRankRow;

const mapStateToProps = state => ({
  settingReducer: state.settingReducer,
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GameRankRow);
