import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

class DetailsFinesRow extends PureComponent {
  state = {};

  render() {
    return (
      <View>
        <View style={styles.row}>
          <View style={styles.teamLeft}>
            <Text style={styles.txt_teamLeft}>{this.props.team1}</Text>
          </View>
          <View style={styles.teamRight}>
            <Text style={styles.txt_teamRight}>{this.props.team2}</Text>
          </View>
        </View>
        <View style={styles.tittle}>
          <Text style={styles.txt_tittle}>{this.props.content}</Text>
        </View>
      </View>
    );
  }
}

export default DetailsFinesRow;
