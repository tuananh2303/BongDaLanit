import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  row: {
    flexDirection: 'row',
    height: 50 * d.ratioH,
    alignItems: 'center',
    paddingLeft: 10 * d.ratioW,
    paddingRight: 10 * d.ratioW,
    borderBottomColor: ColorTheme.white2,
    borderBottomWidth: 1 * d.ratioH,
  },

  txt_teamLeft: {
    color: ColorTheme.black,
    fontSize: FontSize.medium,
  },
  teamLeft: {
    flex: 1,
  },
  teamRight: {
    flex: 1,
    alignItems: 'flex-end',
  },
  txt_teamRight: {
    color: ColorTheme.black,
    fontSize: FontSize.medium,
  },
  tittle: {
    height: 50 * d.ratioH,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
  },
  txt_tittle: {
    fontSize: FontSize.small + 3,
    color: ColorTheme.black,
  },
});
