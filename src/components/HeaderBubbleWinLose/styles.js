import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  viewMain: {
    padding: 6,
  },

  date: {
    color: ColorTheme.white,
    fontSize: FontSize.small,
  },

  dataNullText: {
    color: ColorTheme.black,
    fontSize: FontSize.small,
  },

  win: {
    backgroundColor: ColorTheme.green500,
    height: 20 * d.ratioH,
    width: 20 * d.ratioH,
    borderRadius: 10 * d.ratioH,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: ColorTheme.white,
  },

  lose: {
    backgroundColor: ColorTheme.red500,
    height: 20 * d.ratioH,
    width: 20 * d.ratioH,
    borderRadius: 10 * d.ratioH,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: ColorTheme.white,
  },

  draw: {
    backgroundColor: ColorTheme.gray500,
    height: 20 * d.ratioH,
    width: 20 * d.ratioH,
    borderRadius: 10 * d.ratioH,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: ColorTheme.white,
  },

  dataNull: {
    backgroundColor: ColorTheme.white,
    height: 20 * d.ratioH,
    width: 20 * d.ratioW,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
