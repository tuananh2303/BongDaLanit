import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Image } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Images } from '../../themes';

const WIN = 0;
const LOSE = 2;
const DRAW = 1;

class Home extends PureComponent {
  state = {};

  getrotate = () => {
    if (this.props.isRotate == null) {
      return null;
    }
    return { transform: [{ rotate: '90deg' }] };
  };

  renderBall = condition => {
    if (condition === WIN) {
      return (
        <View style={styles.win}>
          <Text style={[styles.date, this.getrotate()]}>W</Text>
        </View>
      );
    }
    if (condition === LOSE) {
      return (
        <View style={styles.lose}>
          <Text style={[styles.date, this.getrotate()]}>L</Text>
        </View>
      );
    }
    if (condition === DRAW) {
      return (
        <View style={styles.draw}>
          <Text style={[styles.date, this.getrotate()]}>D</Text>
        </View>
      );
    }
    return (
      <View style={styles.dataNull}>
        <Text style={[styles.dataNullText, this.getrotate()]}>-</Text>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.viewMain}>{this.renderBall(this.props.type)}</View>
    );
  }
}

export default Home;
