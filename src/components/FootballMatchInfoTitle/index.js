import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

class FootballMatchInfoTitle extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.title}>
        <Text style={styles.txt_title}> Trận đấu</Text>
      </View>
    );
  }
}

export default FootballMatchInfoTitle;
