import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  ProportionWinChart: {
    // flex: 1,
    flexDirection: 'row',
    paddingTop: 10 * d.ratioH,
    marginLeft: 70 * d.ratioW,

    height: 120 * d.ratioH,
  },
  ProportionWinChartColumn: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  // chartTeam1: {
  //   backgroundColor: ColorTheme.white2,
  //   height: (this.props.chartTeam1 / this.props.chartTeam2) * 100 * d.ratioH,
  //   width: 85 * d.ratioW,
  //   marginRight: 5 * d.ratioW,
  // },
  // chartX: {
  //   backgroundColor: ColorTheme.white2,
  //   height: (this.props.chartX / this.props.chartTeam2) * 100 * d.ratioH,
  //   width: 85 * d.ratioW,
  //   marginRight: 5 * d.ratioW,
  // },
  // chartTeam2: {
  //   backgroundColor: ColorTheme.white2,
  //   width: 85 * d.ratioW,
  //   height: 100 * d.ratioH,
  //   marginRight: 5 * d.ratioW,
  // },
});
