import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

class ProportionWinChart extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.ProportionWinChart}>
        <View style={styles.ProportionWinChartColumn}>
          <View
            style={{
              backgroundColor: ColorTheme.white2,
              height:
                (this.props.chartTeam1 / this.props.chartTeam2) *
                100 *
                d.ratioH,
              width: 85 * d.ratioW,
              marginRight: 5 * d.ratioW,
            }}
          />
          <View>
            <Text>1 : {this.props.chartTeam1} %</Text>
          </View>
        </View>
        <View style={styles.ProportionWinChartColumn}>
          <View
            style={{
              backgroundColor: ColorTheme.white2,
              height:
                (this.props.chartX / this.props.chartTeam2) * 100 * d.ratioH,
              width: 85 * d.ratioW,
              marginRight: 5 * d.ratioW,
            }}
          />
          <View>
            <Text>X: {this.props.chartX} %</Text>
          </View>
        </View>
        <View style={styles.ProportionWinChartColumn}>
          <View
            style={{
              backgroundColor: ColorTheme.white2,
              width: 85 * d.ratioW,
              height: 100 * d.ratioH,
              marginRight: 5 * d.ratioW,
            }}
          />
          <View>
            <Text>2 : {this.props.chartTeam2} %</Text>
          </View>
        </View>
      </View>
    );
  }
}

export default ProportionWinChart;
