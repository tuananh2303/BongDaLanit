import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import FastImage from '../FastImage';
import styles from './styles';
import { Images } from '../../themes';
import { URL_IMAGE } from '../../constants/URL';

class MatchLineUpField extends PureComponent {
  state = {};

  componentDidMount() {}

  render() {
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => this.props.onPressPlayer()}
      >
        <View>
          <FastImage
            linkImage={URL_IMAGE + this.props.playerImage}
            linkImageDefault={Images.avatarPlayerDefault}
            style={styles.icon}
          />
        </View>
        <View style={styles.nameKick}>
          <Text style={styles.nameText}>
            {this.props.playerNumber + ' ' + this.props.playerName}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default MatchLineUpField;
