import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    // flex: 1,
    alignItems: 'center',
    marginTop: 7 * d.ratioH,
  },
  nameKick: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: 'red',
  },
  txt_stt: {
    fontSize: FontSize.small + 2,
    color: ColorTheme.black,
    fontWeight: '500',
  },

  nameText: {
    fontSize: FontSize.small + 2,
    color: ColorTheme.black,
    textAlign: 'center',
  },

  icon: {
    height: 40 * d.ratioH,
    width: 40 * d.ratioH,
    borderRadius: 25 * d.ratioH,
    // borderColor: 'red',
    // borderWidth: 2,
    marginLeft: 10 * d.ratioW,
  },
});
