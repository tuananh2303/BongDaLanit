import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import ProportionWinTitle from '../ProportionWinTitle';

import ProportionStart from '../ProportionStart';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

class ProportionWin extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.container}>
        <ProportionWinTitle txt_top="Ai sẽ thắng" txt_bottom="1887 bình chọn" />
        <ProportionStart />
      </View>
    );
  }
}

export default ProportionWin;
