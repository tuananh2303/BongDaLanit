import React, { PureComponent } from 'react';
import { View, Text, TextInput } from 'react-native';
import styles from './styles';
import { Images, ColorTheme } from '../../themes';
import LanguageVI from '../../constants/Language';
import { converTimeShortGamePlay } from '../../utilities/MomentTime';
import { URL_IMAGE_PREDICT } from '../../constants/URL';
import FastImage from '../FastImage';

class GameHistoryRow extends PureComponent {
  state = {};

  componentDidMount() {
    // console.log(this.props.data);
  }

  getStyles = condition => {
    if (condition == 1) {
      return styles.yourtbetwin;
    }
    if (condition == 2) {
      return styles.yourtbetlose;
    }
    return styles.yourtbet;
  };

  rowGame = data => {
    return (
      <View style={styles.container}>
        <View style={styles.statusMatch}>
          <Text>{converTimeShortGamePlay(data.match.match_date)}</Text>
        </View>

        <View style={styles.team}>
          <View style={styles.teamLeft}>
            <View style={styles.iconTeam}>
              <FastImage
                linkImageDefault={Images.logoTeamDefault}
                linkImage={URL_IMAGE_PREDICT + data.match.homeLogo}
                style={styles.icon}
                resizeMode="contain"
              />
            </View>
            <Text style={styles.nameTeam}>{data.match.home_name}</Text>
          </View>

          <View style={styles.textBox}>
            <View style={styles.textBoxLeft}>
              <TextInput
                style={styles.input}
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                keyboardType="numeric"
                maxLength={2}
                textAlign="center"
                editable={false}
                value={data.match.homeScore}
              />
            </View>

            <View style={styles.textBoxRight}>
              <TextInput
                style={styles.input}
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                keyboardType="numeric"
                maxLength={2}
                textAlign="center"
                editable={false}
                value={data.match.awayScore}
              />
            </View>
          </View>

          <View style={styles.teamRight}>
            <View style={styles.iconTeam}>
              <FastImage
                linkImageDefault={Images.logoTeamDefault}
                linkImage={URL_IMAGE_PREDICT + data.match.awayLogo}
                style={styles.icon}
                resizeMode="contain"
              />
            </View>
            <Text style={styles.nameTeam}>{data.match.away_name}</Text>
          </View>
        </View>
      </View>
    );
  };

  // hang thong tin ban cuoc va thong tin thang cuoc
  yourtBet = (title, value, condition) => {
    if (condition) {
      return (
        <View style={styles.yourtbetLeft}>
          <View style={styles.betTitle}>
            <Text>{title}</Text>
          </View>
          <View style={styles.betTitleSub}>
            <Text>{value}</Text>
          </View>
        </View>
      );
    }

    return <View style={styles.yourtbetLeft} />;
  };

  render() {
    return (
      <View style={styles.endMatch}>
        {this.rowGame(this.props.data)}
        <View style={this.getStyles(this.props.data.status)}>
          {this.yourtBet(
            LanguageVI.Lose,
            this.props.data.point_gain,
            this.props.data.status == 2,
          )}

          <View style={styles.yourtbetLeft}>
            <View style={styles.betTitle}>
              <Text>{LanguageVI.YourBet}</Text>
            </View>
            <View style={styles.betTitleSub}>
              <Text>
                {this.props.data.bet
                  ? '( ' +
                    this.props.data.bet.homeScore +
                    ' - ' +
                    this.props.data.bet.awayScore +
                    ' ) '
                  : null}
              </Text>
              <Text>{this.props.data.point + ' point'}</Text>
            </View>
          </View>
          {this.yourtBet(
            LanguageVI.Win,
            this.props.data.point_gain,
            this.props.data.status == 1,
          )}
        </View>
      </View>
    );
  }
}

export default GameHistoryRow;
