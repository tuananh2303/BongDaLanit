import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    backgroundColor: ColorTheme.white,
  },
  statusMatch: {
    height: 30 * d.ratioH,
    justifyContent: 'center',
    alignItems: 'center',
  },
  team: {
    height: 70 * d.ratioH,
    justifyContent: 'center',
    flexDirection: 'row',
    marginBottom: 10 * d.ratioH,
  },
  teamLeft: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'tomato',
  },
  textBox: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'green',
    flexDirection: 'row',
  },
  teamRight: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'rgb(255,241,118)',
  },
  icon: {
    height: 40 * d.ratioH,
    width: 40 * d.ratioH,
  },
  input: {
    borderColor: ColorTheme.gray500,
    borderWidth: 1,
    height: 35 * d.ratioH,
    width: 40 * d.ratioW,
    borderRadius: 5,
  },
  textBoxLeft: {
    marginRight: 5 * d.ratioW,
  },
  bet: {
    height: 50 * d.ratioH,
    flexDirection: 'row',
  },
  betLeft: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  betMid: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  betRight: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  txt_betRight: {
    marginRight: 20 * d.ratioW,
  },
  ratioContent: {
    height: 30 * d.ratioH,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: 'tomato',
  },
  titleRatio: {
    alignItems: 'center',
    height: 30 * d.ratioH,
    justifyContent: 'center',
    // backgroundColor: 'green',
  },
  homeScore: {
    flex: 4.75,
    alignItems: 'flex-end',
  },
  awayScore: {
    flex: 4.75,
    alignItems: 'flex-start',
  },
  txt: {
    flex: 0.5,
    alignItems: 'center',
  },
  yourtbetwin: {
    flexDirection: 'row',
    backgroundColor: ColorTheme.lightBlue300a,
    height: 80 * d.ratioH,
  },
  yourtbetlose: {
    flexDirection: 'row',
    backgroundColor: ColorTheme.yellow600,
    height: 80 * d.ratioH,
  },
  yourtbet: {
    flexDirection: 'row',
    backgroundColor: ColorTheme.white,
    height: 80 * d.ratioH,
    borderTopColor: ColorTheme.white2,
    borderTopWidth: 1,
  },
  yourtbetLeft: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'tomato',
  },
  yourtbetMid: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  yourtbetRight: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  betTitle: {
    paddingBottom: 10 * d.ratioH,
  },
  // endMatch: {
  //   margin: 10,
  // },
  betNoStart: {
    height: 50 * d.ratioH,
    backgroundColor: ColorTheme.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  betNoStartSub: {
    height: 30 * d.ratioH,
    width: 100 * d.ratioW,
    backgroundColor: ColorTheme.blue900,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  txt_betNoStartSub: {
    color: ColorTheme.white,
  },
  // nostart: {
  //   margin: 10,
  // },
  ratio: {
    backgroundColor: ColorTheme.white2,
  },
  betTitleSub: {
    alignItems: 'center',
  },

  betSubleft: {
    flex: 1,
    alignItems: 'flex-end',
  },
  betSubMid: {
    flex: 1,
    alignItems: 'center',
  },
  betSubRight: {
    flex: 1,
  },
  betLay: {
    flex: 1,
  },
  nameTeam: {
    textAlign: 'center',
  },
});
