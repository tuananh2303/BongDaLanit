import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

const defaultHeight = 640 * d.ratioH;
const defaultWidth = 640 * d.ratioW;
export default StyleSheet.create({
  container: {
    height: defaultHeight,
    width: defaultWidth,
    flexDirection: 'row',
    transform: [{ rotate: '90deg' }],
  },
  icon: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'rgb(63,81,181)',
  },
  view_titleEnd: {
    flex: 1.5,
    // backgroundColor: 'rgb(63,81,181)',
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 5 * d.ratioW,
    paddingRight: 5 * d.ratioW,
  },
  view_icon: {
    height: 20 * d.ratioH,
    width: 20 * d.ratioW,
    backgroundColor: ColorTheme.green400,
    borderRadius: 15,
  },
  view_icon1: {
    height: 20 * d.ratioH,
    width: 20 * d.ratioW,
    backgroundColor: ColorTheme.green900,
    borderRadius: 15,
  },
  view_icon2: {
    height: 20 * d.ratioH,
    width: 20 * d.ratioW,
    backgroundColor: ColorTheme.LightBlue500,
    borderRadius: 15,
  },
  view_icon3: {
    height: 20 * d.ratioH,
    width: 20 * d.ratioW,
    backgroundColor: ColorTheme.red500,
    borderRadius: 15,
  },
  view_icon4: {
    height: 20 * d.ratioH,
    width: 20 * d.ratioW,
    backgroundColor: ColorTheme.red900,
    borderRadius: 15,
  },
});
