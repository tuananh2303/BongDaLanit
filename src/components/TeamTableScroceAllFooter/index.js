import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { ColorTheme } from '../../themes';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import HeaderBubbleWinLose from '../HeaderBubbleWinLose';
const WinLose = ['win', 'lose', 'lose', ''];
class TeamTableScroceAllFooter extends PureComponent {
  state = {};

  row = (type, data) => {
    // console.log(data);
    
    if (type === 'xanh') {
      return (
        <View style={styles.container}>
          <View style={styles.icon}>
            <View style={styles.view_icon} />
          </View>
          <View style={styles.view_titleEnd}>
            <Text>{data}</Text>
          </View>
        </View>
      );
    }
    if (type === 'green900') {
      return (
        <View style={styles.container}>
          <View style={styles.icon}>
            <View style={styles.view_icon1} />
          </View>
          <View style={styles.view_titleEnd}>
            <Text>{data}</Text>
          </View>
        </View>
      );
    }
    if (type === 'LightBlue500') {
      return (
        <View style={styles.container}>
          <View style={styles.icon}>
            <View style={styles.view_icon2} />
          </View>
          <View style={styles.view_titleEnd}>
            <Text>{data}</Text>
          </View>
        </View>
      );
    }
    if (type === 'red500') {
      return (
        <View style={styles.container}>
          <View style={styles.icon}>
            <View style={styles.view_icon3} />
          </View>
          <View style={styles.view_titleEnd}>
            <Text>{data}</Text>
          </View>
        </View>
      );
    }
    if (type === 'red900') {
      return (
        <View style={styles.container}>
          <View style={styles.icon}>
            <View style={styles.view_icon4} />
          </View>
          <View style={styles.view_titleEnd}>
            <Text>{data}</Text>
          </View>
        </View>
      );
    }
  };

  render() {
    return <View>{this.row(this.props.color, this.props.text)}</View>;
  }
}

export default TeamTableScroceAllFooter;
