import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import InfoKickRow from '../InfoKickRow';
import InfoKichTitle from '../InfoKichTitle';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

class InfoKick extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.container}>
        <InfoKichTitle />
        <InfoKickRow />
      </View>
    );
  }
}

export default InfoKick;
