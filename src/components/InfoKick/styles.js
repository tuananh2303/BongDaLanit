import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    height: 170 * d.ratioH,
    flexDirection: 'column',
    elevation: 3,
    marginTop: 10 * d.ratioH,
    marginBottom: 10 * d.ratioH,
    backgroundColor: ColorTheme.white,
    borderBottomColor: ColorTheme.white2,
    borderBottomWidth: 1,
  },
});
