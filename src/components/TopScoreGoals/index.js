import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Image, FlatList } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import TopScoreRow from '../TopScoreRow';
import Images from '../../themes/Images';
class TopScoreGoals extends PureComponent {
  state = {};

  componentDidMount() {
    // console.log(this.props.data);
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={this.props.data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => (
            <TopScoreRow
              title={item.title}
              sub_title={item.sub_title}
              goals={item.goals}
            />
          )}
        />
      </View>
    );
  }
}

export default TopScoreGoals;
