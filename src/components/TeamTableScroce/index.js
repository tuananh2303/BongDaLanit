import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import TeamTableScroceName from '../TeamTableScroceName';
// import Icon from 'react-native-vector-icons/Ionicons';
import TeamTableScroceRow from '../TeamTableScroceRow';
import styles from './styles';
import LanguageVI from '../../constants/Language';

class TeamTableScroce extends PureComponent {
  state = {};

  // renderItem = () => <View />;

  render() {
    return (
      <View style={styles.container}>
        <TeamTableScroceName leaghName={this.props.data.leagh} />
        {/* <TeamTableScroceRow />
        <TeamTableScroceRow /> */}
        <FlatList
          showsVerticalScrollIndicator={false}
          data={this.props.data.data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => (
            <TeamTableScroceRow
              onPressTeam={() => this.props.onPressTeam()}
              data={item}
              teamMain1={this.props.teamMain1}
              teamMain2={this.props.teamMain2}
            />
          )}
        />
        {/* <View style={styles.allteam}>
          <Text style={styles.txt_allteam}>{LanguageVI.SHOWALLTABLE}</Text>
        </View> */}
      </View>
    );
  }
}

export default TeamTableScroce;
