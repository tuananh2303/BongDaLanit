import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import ChartPieRow from '../ChartPieRow';

class ChartPie extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.container}>
        <ChartPieRow />
      </View>
    );
  }
}

export default ChartPie;
