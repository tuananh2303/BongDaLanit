import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  ViewMain: {
    backgroundColor: 'white',
    margin: 5 * d.ratioH,
    marginVertical: 10 * d.ratioH,
    borderRadius: 7 * d.ratioH,
    overflow: 'hidden',
  },
  ViewHeader: {
    flexDirection: 'row',
    height: 40,
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10 * d.ratioH,
  },
  Text: {
    fontSize: FontSize.medium,
    color: ColorTheme.black,
  },
  Image: {
    height: 160 * d.ratioH,
    width: d.windowSize.width,
  },
});
