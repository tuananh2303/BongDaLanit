import React, { PureComponent } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';

import { View, Text, Image, ImageBackground } from 'react-native';
import FastImage from '../FastImage';

import { Images, ColorTheme } from '../../themes';
import styles from './styles';

export default class Banner extends PureComponent {
  state = {
    visible: true,
  };

  render() {
    if (this.state.visible) {
      return (
        <View style={styles.ViewMain}>
          <View style={styles.ViewHeader}>
            <Text style={styles.Text}>Quang cao</Text>
            <Icon
              name="md-close"
              size={22}
              color={ColorTheme.gray}
              onPress={() =>
                this.setState({
                  visible: false,
                })
              }
            />
          </View>
          <Image
            source={Images.teamImage}
            resizeMode="cover"
            style={styles.Image}
          />
        </View>
      );
    }
    return null;
  }
}
