import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

class GameShowBetNoStartRow extends PureComponent {
  state = {};

  componentDidMount() {}

  renderRow = data => {
    return (
      <View style={styles.petWin}>
        <View style={styles.petWinLeft}>
          <View style={styles.table}>
            <View>
              <Text>{data.homeScore}</Text>
            </View>
            <View style={styles.tableMid}>
              <Text>:</Text>
            </View>
            <View>
              <Text>{data.awayScore}</Text>
            </View>
          </View>
        </View>

        <View style={styles.petWinRight}>
          <Text>{this.props.bet}</Text>
        </View>

        <View style={styles.petWinRight1}>
          <Text style={styles.reward}>
            {(data.odds * this.props.bet) / 100}
          </Text>
        </View>
      </View>
    );
  };

  render() {
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => this.props.callback(this.props.data)}
      >
        {this.renderRow(this.props.data)}
      </TouchableOpacity>
    );
  }
}

export default GameShowBetNoStartRow;
