import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

class ProportionWinTitle extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.ProportionWinTitle}>
        <View style={styles.icon}>
          <Icon name="ios-stats" size={30} />
        </View>
        <View style={styles.content}>
          <Text style={styles.txt_top}>{this.props.txt_top}</Text>
          <Text style={styles.txt_bottom}>{this.props.txt_bottom}</Text>
        </View>
      </View>
    );
  }
}

export default ProportionWinTitle;
