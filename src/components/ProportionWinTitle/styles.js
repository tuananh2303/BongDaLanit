import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  ProportionWinTitle: {
    flexDirection: 'row',
    paddingTop: 10 * d.ratioH,
  },
  icon: {
    width: 60 * d.ratioW,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    marginLeft: 10 * d.ratioW,
    width: 310 * d.ratioW,
  },
  txt_top: {
    fontSize: 14,
    color: ColorTheme.black,
    fontWeight: '500',
  },
  txt_bottom: {
    fontSize: 10,
    paddingBottom: 5 * d.ratioH,
  },
});
