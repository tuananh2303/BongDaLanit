import React, { PureComponent } from 'react';
import { View, Switch } from 'react-native';
import styles from './styles';

class SwitchRow1 extends PureComponent {
  state = {};

  // toggleSwitch1 = () => {
  //   this.setState({
  //     SwitchOnValueHolder: !this.state.SwitchOnValueHolder,
  //   });
  // };

  render() {
    return (
      // <View style={styles.MainContainer}>
      //   <Switch
      //     onValueChange={() => this.toggleSwitch1()}
      //     style={{ marginBottom: 10 }}
      //     value={this.state.SwitchOnValueHolder}
      //   />
      // </View>
      <View style={styles.MainContainer}>
        <Switch
          onValueChange={() => this.props.toggleSwitch1()}
          style={{ marginBottom: 10 }}
          value={this.props.value}
        />
      </View>
    );
  }
}

export default SwitchRow1;
