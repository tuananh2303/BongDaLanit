import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  MainContainer: {
    flex: 1,
    alignItems: 'center',
  },
});
