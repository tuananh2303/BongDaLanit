import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  ScrollView,
  ToastAndroid,
} from 'react-native';
import { connect } from 'react-redux';
import Axios from 'axios';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Images, ColorTheme } from '../../themes';
import GameShowBetNoStart from '../GameShowBetNoStart';
import LanguageVI from '../../constants/Language';
import { converTimeShortGamePlay } from '../../utilities/MomentTime';
import { URL_IMAGE_PREDICT, URL_POST_PREDICT } from '../../constants/URL';
import FastImage from '../FastImage';
import { refreshData } from '../../redux/actions/userAction';

class GameRow extends PureComponent {
  state = {
    showBet: false,
    input1: '',
    input2: '',
    bet: false,
    value: 100,
    bet_id: null,
    selected: false,
  };

  componentDidMount() {
    // console.log(this.props.data);
    if (this.props.data.selected != null) {
      this.setState({
        selected: true,
        input1: this.props.data.selected.bet.homeScore,
        input2: this.props.data.selected.bet.awayScore,
        value: parseInt(this.props.data.selected.point_bet, 10),
      });
    }
  }

  ToggleShow = () => {
    this.setState({
      showBet: !this.state.showBet,
    });
  };

  getValue = data => {
    this.setState({
      input1: data.homeScore.toString(),
      input2: data.awayScore.toString(),
      bet_id: data.id,
    });
    console.log(data);
  };

  // phan giong nhau giua tran dau  da ket thuc va tran dau  chua bat dau
  rowGame = data => {
    return (
      <View style={styles.container}>
        <View style={styles.statusMatch}>
          <Text>{converTimeShortGamePlay(data.match_date)}</Text>
        </View>

        <View style={styles.team}>
          <View style={styles.teamLeft}>
            <View style={styles.iconTeam}>
              <FastImage
                linkImageDefault={Images.logoTeamDefault}
                linkImage={URL_IMAGE_PREDICT + data.homeLogo}
                style={styles.icon}
                resizeMode="contain"
              />
            </View>

            <Text style={styles.nameTeam}>{data.home_name}</Text>
          </View>

          <View style={styles.textBox}>
            <View style={styles.textBoxLeft}>
              <TextInput
                style={styles.input}
                underlineColorAndroid="transparent"
                keyboardType="numeric"
                textAlign="center"
                value={this.state.input1}
                onChangeText={input1 => {
                  this.setState({ input1 });
                }}
                // editable={!this.state.selected}
                editable={false}
              />
            </View>

            <View style={styles.textBoxRight}>
              <TextInput
                style={styles.input}
                underlineColorAndroid="transparent"
                keyboardType="numeric"
                textAlign="center"
                value={this.state.input2}
                onChangeText={input2 => {
                  this.setState({ input2 });
                }}
                // editable={!this.state.selected}
                editable={false}
              />
            </View>
          </View>

          <View style={styles.teamRight}>
            <View style={styles.iconTeam}>
              <Image
                source={{ uri: URL_IMAGE_PREDICT + data.awayLogo }}
                style={styles.icon}
                resizeMode="contain"
              />
            </View>

            <Text style={styles.nameTeam}>{data.away_name}</Text>
          </View>
        </View>

        <TouchableOpacity style={styles.bet} onPress={() => this.ToggleShow()}>
          <View style={styles.betMid}>
            <Text>
              {this.state.showBet
                ? LanguageVI.HideTableBet
                : LanguageVI.ShowTableBet}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  minusPointBet = () => {
    if (this.state.selected) {
      if (this.state.value > parseInt(this.props.data.selected.point_bet, 10)) {
        this.setState({
          value: this.state.value - 100,
        });
      }
    }
    if (!this.state.selected) {
      if (this.state.value > 100) {
        this.setState({
          value: this.state.value - 100,
        });
      }
    }
  };

  // phan NoStart
  yourtBetMid = value => {
    return (
      <View style={styles.yourtbetLeft}>
        <View style={styles.betTitle}>
          <Text>{LanguageVI.YourBet}</Text>
        </View>
        <View style={styles.betTitleSub}>
          <TouchableOpacity
            style={styles.betSubleft}
            onPress={() => this.minusPointBet()}
          >
            <Icon name="md-remove" size={12} color={ColorTheme.white} />
          </TouchableOpacity>
          <View style={styles.betSubMid}>
            <Text>{value}</Text>
          </View>
          <TouchableOpacity
            style={styles.betSubRight}
            onPress={() =>
              this.setState({
                value: this.state.value + 100,
              })
            }
          >
            <Icon name="md-add" size={12} color={ColorTheme.white} />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  confirmBet = () => {
    const param = {
      user_id: this.props.userReducer.dataFull.user.id,
      match_id: this.props.data.id,
      bet_id: this.state.bet_id,
      point_bet: this.state.value,
    };

    Axios.post(URL_POST_PREDICT + this.props.userReducer.data.token, param)
      .then(response => {
        console.log(response.data);
        if (response.data.status == 'success') {
          ToastAndroid.show(LanguageVI.BetSuccess, ToastAndroid.SHORT);
          this.props.userConfirmBet();
          this.props.refreshData(this.props.userReducer.data);
        }
        if (response.data.status == 'error') {
          ToastAndroid.show(response.data.error, ToastAndroid.SHORT);
        }
      })
      .catch(error => console.log(error));
  };

  rederNoStart = (input1, input2, data) => {
    if (input1 != '' && input2 != '') {
      return (
        <View>
          <View>{this.rowGame(data)}</View>
          <View style={styles.betNoStart}>
            <TouchableOpacity
              style={styles.betNoStartSub}
              onPress={() => this.confirmBet()}
            >
              <Text style={styles.txt_betNoStartSub}>{LanguageVI.Confirm}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.yourtbet}>
            {this.yourtBetMid(this.state.value)}
          </View>
        </View>
      );
    }
    return <View>{this.rowGame(data)}</View>;
  };

  render() {
    return (
      <View style={styles.nostart}>
        {this.rederNoStart(
          this.state.input1,
          this.state.input2,
          this.props.data,
        )}
        {this.state.showBet ? (
          <GameShowBetNoStart
            data={this.props.data.bets}
            bet={this.state.value}
            callback={dataCallback => {
              if (!this.state.selected) {
                this.getValue(dataCallback);
              }
            }}
          />
        ) : null}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  userReducer: state.userReducer,
});

const mapDispatchToProps = dispatch => ({
  refreshData: data => dispatch(refreshData(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GameRow);
