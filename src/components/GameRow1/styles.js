import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  // container: {
  //   backgroundColor: ColorTheme.white,
  //   borderRadius: 15,
  //   borderTopStartRadius: 10,
  //   borderTopEndRadius: 10,
  // },
  statusMatch: {
    height: 30 * d.ratioH,
    justifyContent: 'center',
    alignItems: 'center',
  },
  team: {
    height: 70 * d.ratioH,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  teamLeft: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'tomato',
  },
  textBox: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'green',
    flexDirection: 'row',
  },
  teamRight: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'rgb(255,241,118)',
  },
  icon: {
    height: 40 * d.ratioH,
    width: 40 * d.ratioH,
  },
  input: {
    borderColor: ColorTheme.gray500,
    borderWidth: 1,
    height: 35 * d.ratioH,
    width: 40 * d.ratioW,
    borderRadius: 5,
  },
  textBoxLeft: {
    marginRight: 5 * d.ratioW,
  },
  bet: {
    marginTop: 10 * d.ratioH,
    height: 40 * d.ratioH,
    flexDirection: 'row',
    backgroundColor: ColorTheme.yellow600,
  },
  betLeft: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  betMid: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  betRight: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  txt_betRight: {
    marginRight: 20 * d.ratioW,
  },
  ratioContent: {
    height: 30 * d.ratioH,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: 'tomato',
  },
  titleRatio: {
    alignItems: 'center',
    height: 30 * d.ratioH,
    justifyContent: 'center',
    // backgroundColor: 'green',
  },
  homeScore: {
    flex: 4.75,
    alignItems: 'flex-end',
  },
  awayScore: {
    flex: 4.75,
    alignItems: 'flex-start',
  },
  txt: {
    flex: 0.5,
    alignItems: 'center',
  },
  yourtbet: {
    flexDirection: 'row',
    // backgroundColor: ColorTheme.white,
    height: 80 * d.ratioH,
    // borderBottomEndRadius: 10,
    // borderBottomStartRadius: 10,
  },
  yourtbetLeft: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'tomato',
  },
  yourtbetMid: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  yourtbetRight: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  betTitle: {
    paddingBottom: 10 * d.ratioH,
  },
  // endMatch: {
  //   margin: 10,
  // },
  betNoStart: {
    height: 50 * d.ratioH,
    backgroundColor: ColorTheme.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  betNoStartSub: {
    height: 30 * d.ratioH,
    width: 100 * d.ratioW,
    backgroundColor: ColorTheme.blue900,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  txt_betNoStartSub: {
    color: ColorTheme.white,
  },
  // nostart: {
  //   margin: 10,
  // },
  ratio: {
    backgroundColor: ColorTheme.white2,
  },
  betTitleSub: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  betSubleft: {
    height: 30 * d.ratioH,
    width: 30 * d.ratioH,
    marginLeft: 100 * d.ratioW,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 15 * d.ratioH,
    backgroundColor: ColorTheme.LightBlue500,
  },
  betSubMid: {
    flex: 1,
    alignItems: 'center',
  },
  betSubRight: {
    height: 30 * d.ratioH,
    width: 30 * d.ratioH,
    backgroundColor: ColorTheme.LightBlue500,
    marginRight: 100 * d.ratioW,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 15 * d.ratioH,
  },
  betLay: {
    flex: 1,
  },

  nameTeam: {
    textAlign: 'center',
  },
});
