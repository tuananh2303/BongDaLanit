import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';


class Match1Title extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.nameteam}>
        <View style={styles.teamLeft}>
          <Text style={styles.txt_teamLeft}>{this.props.match_name}</Text>
        </View>
        <View style={styles.teamRight}>
          <Text style={styles.txt_teamRight}>{this.props.ratio}</Text>
        </View>
      </View>
    );
  }
}

export default Match1Title;
