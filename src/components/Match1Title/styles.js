import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  nameteam: {
    flexDirection: 'row',
    height: 50 * d.ratioH,
    alignItems: 'center',
    paddingLeft: 10 * d.ratioW,
    paddingRight: 10 * d.ratioW,
  },

  txt_teamLeft: {
    color: ColorTheme.black,
    fontSize: 16,
  },
  teamLeft: {
    flex: 1,
  },
  teamRight: {
    flex: 1,
    alignItems: 'flex-end',
  },
  txt_teamRight: {
    color: ColorTheme.black,
    fontSize: 16,
  },
});
