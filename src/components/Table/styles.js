import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    // height: 170 * d.ratioH,
    flexDirection: 'column',
    elevation: 3,
    marginTop: 5 * d.ratioW,
    marginBottom: 5 * d.ratioW,
    marginLeft: 7 * d.ratioW,
    marginRight: 7 * d.ratioW,
    backgroundColor: 'white',
    borderRadius: 10,
  },
  allteam: {
    fontSize: 16,
    borderTopColor: ColorTheme.white2,
    borderTopWidth: 0.6,
    padding: 15,
  },
  txt_allteam: {
    color: ColorTheme.black,
  },

  viewTitle: {
    // height: 50,
    justifyContent: 'center',
    paddingLeft: 20 * d.ratioW,
    paddingTop: 10 * d.ratioH,
    paddingBottom: 10 * d.ratioH,
  },
  title: {
    color: ColorTheme.black,
    fontSize: FontSize.medium + 3,
  },
  subTitle: {
    color: ColorTheme.gray500,
    fontSize: FontSize.medium,
  },
});
