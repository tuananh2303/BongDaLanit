import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import styles from './styles';

class Table extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.viewTitle}>
          <Text style={styles.title}>{this.props.title}</Text>
          <Text style={styles.subTitle}>{this.props.subTitle}</Text>
        </View>
        {this.props.content}
        <TouchableOpacity
          onPress={() => this.props.onPressShowAll()}
          style={styles.allteam}
        >
          <Text style={styles.txt_allteam}>{this.props.showAllText}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Table;
