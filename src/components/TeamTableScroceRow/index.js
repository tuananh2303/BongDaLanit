import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { ColorTheme } from '../../themes';

class TeamTableScroceRow extends PureComponent {
  state = {};

  getStylesForBubble = condition => {
    if (condition === 1) {
      return styles.ViewStatus1;
    }
    if (condition === 3) {
      return styles.ViewStatus3;
    }

    return styles.ViewStatus2;
  };

  getStylesForBubbleText = condition => {
    if (condition === 2) {
      return styles.sttNumSimple;
    }

    return styles.sttNum;
  };

  getStyleTeamMain = (teamMain1, teamMain2) => {
    if (teamMain1 != null && teamMain2 != null) {
      if (this.props.data.id == teamMain1 || this.props.data.id == teamMain2) {
        return [
          styles.viewSTT,
          {
            backgroundColor: ColorTheme.lightBlue300a,
            borderTopRightRadius: 20,
            borderBottomRightRadius: 20,
          },
        ];
      }
    }
    return styles.viewSTT;
  };

  render() {
    // console.log(this.props.data.id, this.props.teamMain1, this.props.teamMain2);
    // console.log(
    //   this.props.data.id == this.props.teamMain1 ||
    //     this.props.data.id == this.props.teamMain2,
    // );

    return (
      <TouchableOpacity
        style={styles.nameteam}
        // onPress={() => this.props.onPressTeam()}
        onPress={() => {}}
      >
        <View
          style={this.getStyleTeamMain(
            this.props.teamMain1,
            this.props.teamMain2,
          )}
        >
          <View style={this.getStylesForBubble(this.props.data.status)}>
            <Text style={this.getStylesForBubbleText(this.props.data.status)}>
              {this.props.data.rank}
            </Text>
          </View>
          <Text style={styles.sttNumSimple}>{this.props.data.team_name}</Text>
        </View>
        <View style={styles.viewMatch}>
          <Text style={styles.sttNumSimple}>{this.props.data.so_tran}</Text>
        </View>
        <View style={styles.viewMatch}>
          <Text style={styles.sttNumSimple}>{this.props.data.hieu_so}</Text>
        </View>
        <View style={styles.viewMatch}>
          <Text style={styles.sttNumSimple}>{this.props.data.diem}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default TeamTableScroceRow;
