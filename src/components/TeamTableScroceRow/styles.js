import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  nameteam: {
    flexDirection: 'row',
    paddingTop: 6 * d.ratioH,
    paddingBottom: 6 * d.ratioH,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  stt: {
    fontSize: 16,
    color: ColorTheme.black,
  },

  sttNum: {
    fontSize: 16,
    color: ColorTheme.white,
  },

  sttNumSimple: {
    fontSize: FontSize.medium - 3,
    color: ColorTheme.black,
  },

  viewSTT: {
    width: 200 * d.ratioW,
    height: 30 * d.ratioH,
    // backgroundColor: 'red',
    flexDirection: 'row',
    paddingLeft: 12 * d.ratioW,
    alignItems: 'center',
  },

  textMatch: {
    fontSize: 10,
  },

  viewMatch: {
    width: 40 * d.ratioW,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: 'blue',
  },

  ViewStatus1: {
    backgroundColor: ColorTheme.lightGreen500,
    height: 30 * d.ratioH,
    width: 30 * d.ratioW,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10 * d.ratioW,
  },

  ViewStatus3: {
    backgroundColor: ColorTheme.red500,
    height: 30 * d.ratioH,
    width: 30 * d.ratioW,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10 * d.ratioW,
  },

  ViewStatus2: {
    // backgroundColor: ColorTheme.lightGreen500,
    height: 30 * d.ratioH,
    width: 30 * d.ratioW,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10 * d.ratioW,
  },
});
