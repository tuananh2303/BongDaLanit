import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import Match1Title from '../Match1Title';
import Match1Row from '../Match1Row';

class Match1 extends PureComponent {
  state = {};

  componentDidMount() {
    // console.log(this.props.data);
    // console.log(this.props.data.data);
  }

  render() {
    return (
      <View style={styles.container}>
        {/* <Match1Title
          match_name={this.props.data.match_name}
          ratio={this.props.data.ratio}
        /> */}
        <FlatList
          showsVerticalScrollIndicator={false}
          data={this.props.data.data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => (
            <Match1Row type={item.doi} dataMatch={item} />
          )}
        />
      </View>
    );
  }
}

export default Match1;
