import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    flexDirection: 'column',
    paddingBottom: 7 * d.ratioH,
  },
});
