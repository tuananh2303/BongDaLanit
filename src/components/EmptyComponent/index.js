import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
// import styles from './styles';
import LanguageVI from '../../constants/Language';
import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

const EmptyComponent = () => (
  <View
    style={{
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 10 * d.ratioH,
      backgroundColor: ColorTheme.white,
      elevation: 3,
      margin: 15 * d.ratioH,
      padding: 15 * d.ratioH,
    }}
  >
    <Text>{LanguageVI.NotInfomation}</Text>
  </View>
);

export default EmptyComponent;
