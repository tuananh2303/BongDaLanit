import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

class DetailsFinesNameTeam extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.nameteam}>
        <View style={styles.teamLeft}>
          <Text style={styles.txt_teamLeft}>{this.props.team1}</Text>
        </View>
        <View style={styles.teamRight}>
          <Text style={styles.txt_teamRight}>{this.props.team2}</Text>
        </View>
      </View>
    );
  }
}

export default DetailsFinesNameTeam;
