import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  nameteam: {
    flexDirection: 'row',
    height: 50 * d.ratioH,
    alignItems: 'center',
    paddingLeft: 10 * d.ratioW,
    paddingRight: 10 * d.ratioW,
    borderBottomColor: ColorTheme.white2,
    borderBottomWidth: 1 * d.ratioH,
  },

  txt_teamLeft: {
    color: ColorTheme.black,
    fontSize: FontSize.medium,
  },
  teamLeft: {
    flex: 1,
  },
  teamRight: {
    flex: 1,
    alignItems: 'flex-end',
  },
  txt_teamRight: {
    color: ColorTheme.black,
    fontSize: FontSize.medium,
  },
});
