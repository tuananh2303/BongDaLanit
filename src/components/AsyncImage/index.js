import React, { Component } from 'react';
import { View, Text, Image, ImageBackground } from 'react-native';
import FastImage from 'react-native-fast-image';

import { Images } from '../../themes';

export default class AsyncImage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <ImageBackground
        source={Images.defaultImage}
        style={{ position: 'relative' }}
      >
        <FastImage
          style={this.props.style}
          source={this.props.source}
          resizeMode={this.props.resizeMode}
        />
      </ImageBackground>
    );
  }
}
