import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
// import firebase from 'react-native-firebase';
import Icon from 'react-native-vector-icons/Ionicons';

import {
  getDayOfWeekAndTimeShot,
  getTimeFromNow,
} from '../../utilities/MomentTime';
import styles from './styles';
import { ColorTheme } from '../../themes';

class CardMatch extends PureComponent {
  state = {
    minute: '',
    highLight: false,
  };

  componentDidMount() {}

  cutStringMinute = (data, type) => {
    // hiep 1 thi tinh so phut binh thuong
    if (type == 'h1') {
      this.setState({
        minute: getTimeFromNow(data) + "'",
      });
    }

    if (type == 'h2') {
      // hiep 2 lay so phut cong them voi 45
      // console.log(getTimeFromNow(data));
      const minute = parseInt(getTimeFromNow(data), 10) + 45;
      // console.log(minute);
      this.setState({
        minute: minute + "'",
      });
    }
  };

  renderCard = dataCard => {
    const statusDataCard = parseInt(dataCard.status, 10);
    if (statusDataCard == 10) {
      return (
        <View style={styles.issue}>
          <View style={styles.txt_issue}>
            <Text style={styles.txt_end}>KT</Text>
          </View>
          <View style={styles.score}>
            <Text style={styles.txt_win}>{dataCard.score1}</Text>
            <Text style={styles.txt_lose}>{dataCard.score2}</Text>
          </View>
          <View style={styles._2team}>
            <Text style={styles.teamWin}>{dataCard.team1_name}</Text>
            <Text style={styles.teamWin}>{dataCard.team2_name}</Text>
          </View>
        </View>
      );
    }
    if (statusDataCard == 0) {
      // console.log(getDayOfWeekAndTimeShot(dataCard.match_date));
      return (
        <View style={styles.startdate}>
          <View style={styles.txt_statrdate}>
            <Text style={styles.txt_Date}>
              {getDayOfWeekAndTimeShot(dataCard.match_date).day}
            </Text>
            <Text style={styles.txt_Time}>
              {getDayOfWeekAndTimeShot(dataCard.match_date).time}
            </Text>
          </View>
          <View style={styles.start_2team}>
            <Text style={styles.txt_statrTeam}>{dataCard.team1_name}</Text>
            <Text style={styles.txt_statrTeam}>{dataCard.team2_name}</Text>
          </View>
        </View>
      );
    }

    if (statusDataCard >= 20) {
      return (
        <View style={styles.startdate}>
          <View style={styles.txt_statrdate}>
            <Icon name="ios-timer" color={ColorTheme.gray500} size={26} />
          </View>
          <View style={styles.start_2team}>
            <Text style={styles.txt_statrTeam}>{dataCard.team1_name}</Text>
            <Text style={styles.txt_statrTeam}>{dataCard.team2_name}</Text>
          </View>
        </View>
      );
    }

    if (statusDataCard >= 1 && statusDataCard <= 4) {
      let titleText = '';
      if (statusDataCard == 1) {
        this.cutStringMinute(dataCard.match_date_h1_start, 'h1');
        titleText = this.state.minute;
      }
      if (statusDataCard == 3) {
        this.cutStringMinute(dataCard.match_date_h2_start, 'h2');
        titleText = this.state.minute;
      }
      if (statusDataCard == 2) {
        titleText = 'H1';
      }
      if (statusDataCard == 4) {
        titleText = 'HP';
      }

      return (
        <View style={styles.issue}>
          <View
            style={
              this.state.highlight
                ? styles.txt_issue_doing_hightlight
                : styles.txt_issue_doing
            }
          >
            <Text style={styles.txt_end}>{titleText}</Text>
          </View>

          <View
            style={
              this.state.highlight
                ? styles.score_doing_hightlight
                : styles.score_doing
            }
          >
            <Text style={styles.txt_win}>{dataCard.score1}</Text>
            <Text style={styles.txt_lose}>{dataCard.score2}</Text>
          </View>
          <View style={styles._2team_doing}>
            <Text style={styles.teamWin}>{dataCard.team1_name}</Text>
            <Text style={styles.teamWin}>{dataCard.team2_name}</Text>
          </View>
        </View>
      );
    }
  };

  render() {
    return (
      <TouchableOpacity
        onPress={() => this.props.onPressMatch(this.props.data.match_id, 10)}
      >
        {this.renderCard(this.props.data)}
      </TouchableOpacity>
    );
  }
}

export default CardMatch;
