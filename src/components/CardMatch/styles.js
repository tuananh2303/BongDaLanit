import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  issue: {
    flexDirection: 'row',
    height: 60 * d.ratioH,
  },
  txt_issue: {
    width: 40 * d.ratioW,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: ColorTheme.white1,
  },

  txt_issue_doing: {
    width: 40 * d.ratioW,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: ColorTheme.lightBlue300a,
  },
  txt_issue_doing_hightlight: {
    width: 40 * d.ratioW,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: ColorTheme.orange200,
  },
  txt_end: {
    fontSize: 16,
    color: ColorTheme.black,
    fontWeight: '400',
  },
  score: {
    // flex: 0.5,
    width: 30 * d.ratioW,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: ColorTheme.white2,
  },

  score_doing: {
    // flex: 0.5,
    width: 30 * d.ratioW,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: ColorTheme.LightBlue500,
  },
  score_doing_hightlight: {
    width: 30 * d.ratioW,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: ColorTheme.orange400,
  },

  txt_win: {
    fontSize: 14,
    color: ColorTheme.black,
    fontWeight: '400',
  },
  txt_lose: {
    fontSize: 14,
    color: ColorTheme.gray,
  },
  teamWin: {
    fontSize: 14,
    color: ColorTheme.black,
    fontWeight: '400',
  },
  teamLose: {
    fontSize: 14,
    color: ColorTheme.gray,
  },
  _2team: {
    // flex: 3.5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 20 * d.ratioW,
  },
  _2team_doing: {
    // flex: 3.5,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 20 * d.ratioW,
    backgroundColor: ColorTheme.lightBlue300a,
  },
  _2team_doing_hightlight: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 20 * d.ratioW,
    backgroundColor: ColorTheme.orange200,
  },
  startdate: {
    // flex: 1,
    flexDirection: 'row',
    // backgroundColor: 'blue',
    height: 60 * d.ratioH,
  },
  txt_statrdate: {
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: 70 * d.ratioW,
    backgroundColor: ColorTheme.white1,
  },
  start_2team: {
    // flex: 3.5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 20,
  },
  txt_Date: {
    fontSize: 10,
    color: ColorTheme.gray,
  },
  txt_Time: {
    fontSize: 16,
    color: ColorTheme.gray,
  },
  txt_statrTeam: {
    fontSize: 14,
    color: ColorTheme.gray,
  },
});
