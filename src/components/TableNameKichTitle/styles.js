import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  tableTitle: {
    flexDirection: 'row',
    height: 50 * d.ratioH,
    alignItems: 'center',
    paddingLeft: 10 * d.ratioW,
    paddingRight: 10 * d.ratioW,
  },

  txt_title: {
    // color: ColorTheme.black,
    fontSize: 16,
  },
  title: {
    flex: 1,
  },
  nameTeam: {
    flex: 1,
    alignItems: 'flex-end',
  },
  txt_nameTeam: {
    // color: ColorTheme.black,
    fontSize: 12,
  },
});
