import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

class TableNameKichTitle extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.tableTitle}>
        <View style={styles.title}>
          <Text style={styles.txt_title}>Thay người</Text>
        </View>
        <View style={styles.nameTeam}>
          <Text style={styles.txt_nameTeam}>Arsenal</Text>
        </View>
      </View>
    );
  }
}

export default TableNameKichTitle;
