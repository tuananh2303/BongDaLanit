import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { ColorTheme } from '../../themes';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import HeaderBubbleWinLose from '../HeaderBubbleWinLose';
const WinLose = ['win', 'lose', 'lose', ''];
class TeamTableScroceRowAll1 extends PureComponent {
  state = {};

  subTitle = title => {
    return (
      <View style={styles.view_subTitle}>
        <Text style={{ fontSize: 10 }}>{title}</Text>
      </View>
    );
  };

  getStylesForBubble = condition => {
    if (condition === 1) {
      return styles.ViewStatus1;
    }
    if (condition === 3) {
      return styles.ViewStatus3;
    }

    return styles.ViewStatus2;
  };

  getStylesForBubbleText = condition => {
    if (condition === 2) {
      return styles.sttNumSimple;
    }

    return styles.sttNum;
  };

  upDown = type => {
    if (type === 'up') {
      return (
        <Icon name="md-arrow-dropup" size={20} color={ColorTheme.green500} />
      );
    }
    if (type === 'down') {
      return (
        <Icon name="md-arrow-dropdown" size={20} color={ColorTheme.red500} />
      );
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.view_STT}>
          <View style={styles.stt}>
            <View style={this.getStylesForBubble(this.props.data.status)}>
              <Text style={this.getStylesForBubbleText(this.props.data.status)}>
                {this.props.data.id}
              </Text>
            </View>
            <View>{this.upDown(this.props.data.up_dow)}</View>
          </View>
          <View style={styles.name}>
            <Text style={styles.sttNumSimple}>{this.props.data.team_name}</Text>
          </View>
        </View>
        {this.subTitle(this.props.data.so_tran)}
        {this.subTitle(this.props.data.W)}
        {this.subTitle(this.props.data.D)}
        {this.subTitle(this.props.data.L)}
        {this.subTitle(this.props.data.GB)}
        {this.subTitle(this.props.data.LL)}
        {this.subTitle(this.props.data.diem_cong)}
        {this.subTitle(this.props.data.diem)}
        <View style={styles.view_titleEnd}>
          {WinLose.map(item => (
            <HeaderBubbleWinLose type={item} isRotate={null} />
          ))}
        </View>
      </View>
    );
  }
}

export default TeamTableScroceRowAll1;
