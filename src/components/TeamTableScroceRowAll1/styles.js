import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

const defaultHeight = 640 * d.ratioH;
const defaultWidth = 640 * d.ratioW;
export default StyleSheet.create({
  container: {
    height: defaultHeight,
    width: defaultWidth,
    flexDirection: 'row',
    transform: [{ rotate: '90deg' }],
  },
  view_STT: {
    flex: 1.75,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  view_subTitle: {
    flex: 0.25,
    // backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
    // transform: [{ rotate: '90deg' }],
  },
  view_titleEnd: {
    flex: 1,
    // backgroundColor: 'rgb(63,81,181)',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    // transform: [{ rotate: '90deg' }],
    paddingLeft: 5 * d.ratioW,
    paddingRight: 5 * d.ratioW,
  },
  stt: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  name: {
    flex: 3,
    // justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  sttNum: {
    fontSize: 13,
    color: ColorTheme.white,
  },

  sttNumSimple: {
    fontSize: 16,
    color: ColorTheme.black,
  },

  ViewStatus1: {
    backgroundColor: ColorTheme.lightGreen500,
    height: 20 * d.ratioH,
    width: 20 * d.ratioW,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
  },

  ViewStatus3: {
    backgroundColor: ColorTheme.red500,
    height: 20 * d.ratioH,
    width: 20 * d.ratioW,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10 * d.ratioW,
  },

  ViewStatus2: {
    // backgroundColor: ColorTheme.lightGreen500,
    height: 20 * d.ratioH,
    width: 20 * d.ratioW,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10 * d.ratioW,
  },
});
