import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Images } from '../../themes';

class SearchRowTeam extends PureComponent {
  state = {};

  render() {
    return (
      <TouchableOpacity
        style={styles.InfoKickRow}
        onPress={() => this.props.onPressPlayer()}
      >
        <Image source={Images.hedera} style={styles.icon} />

        <View style={styles.content}>
          <Text style={styles.txt_top}>{this.props.title}</Text>
        </View>
        <View style={styles.rightContent}>{this.props.rightContent}</View>
      </TouchableOpacity>
    );
  }
}

export default SearchRowTeam;
