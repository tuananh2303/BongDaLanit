import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  InfoKickRow: {
    flexDirection: 'row',
    paddingTop: 7 * d.ratioH,
    paddingBottom: 7 * d.ratioH,
    paddingLeft: 10 * d.ratioW,
  },
  icon: {
    height: 40 * d.ratioH,
    width: 40 * d.ratioH,
    borderRadius: 25 * d.ratioH,
    // borderColor: 'red',
    // borderWidth: 2,
    marginLeft: 10 * d.ratioW,
  },

  content: {
    marginLeft: 10 * d.ratioW,
    // width: 310 * d.ratioW,
  },
  txt_top: {
    fontSize: 14,
    color: ColorTheme.black,
    fontWeight: '500',
  },
  txt_bottom: {
    fontSize: 10,
    paddingBottom: 5 * d.ratioH,
  },

  rightContent: {
    flex: 1,
    // backgroundColor: 'red',
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: 10 * d.ratioW,
  },
});
