import React, { PureComponent } from 'react';
import FastImage from 'react-native-fast-image';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Images } from '../../themes';
import { URL_IMAGE } from '../../constants/URL';

class InfoKickRow extends PureComponent {
  state = {
    linkImage: URL_IMAGE + this.props.playerImage,
    Error: false,
  };

  render() {
    return (
      <TouchableOpacity
        style={styles.InfoKickRow}
        onPress={() => this.props.onPressPlayer()}
      >
        <FastImage
          source={
            this.state.Error
              ? Images.avatarPlayerDefault
              : { uri: this.state.linkImage }
          }
          style={styles.icon}
          onError={Error => this.setState({ Error: true })}
        />

        <View style={styles.content}>
          <Text style={styles.txt_top}>{this.props.title}</Text>
          {this.props.sub_title}
        </View>
        <View style={styles.rightContent}>{this.props.rightContent}</View>
      </TouchableOpacity>
    );
  }
}

export default InfoKickRow;
