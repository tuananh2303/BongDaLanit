import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import LanguageVI from '../../constants/Language';
import styles from './styles';

class InfoKichTitle extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.title}>
        <Text style={styles.txt_title}>{LanguageVI.Biography}</Text>
      </View>
    );
  }
}

export default InfoKichTitle;
