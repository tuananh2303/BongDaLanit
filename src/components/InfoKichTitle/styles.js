import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  title: {
    marginLeft: 15 * d.ratioW,
    marginTop: 15 * d.ratioH,
  },
  txt_title: {
    fontSize: 14,
  },
});
