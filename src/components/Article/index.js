import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';
import FastImage from '../FastImage';
import { Images } from '../../themes';

class Article extends PureComponent {
  state = {};

  componentDidMount() {
    // console.log(this.props.data);
  }

  renderType1 = () => {
    return (
      <TouchableOpacity
        activeOpacity={1}
        style={styles.category}
        onPress={() => this.props.onPress(this.props.data)}
      >
        {/* <AsyncImage
          source={{ uri: this.props.data.image_full_size }}
          style={styles.category_img}
          resizeMode="cover"
        /> */}
        <FastImage
          linkImageDefault={Images.defaultImage}
          linkImage={this.props.data.image_full_size}
          style={styles.category_img}
          resizeMode="cover"
        />
        <View style={styles.content}>
          <Text style={styles.title}>{this.props.data.title}</Text>

          <Text style={styles.time_ago}>
            {this.props.data.base_link + ' - ' + this.props.data.time_ago}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  renderType2 = () => {
    return (
      <TouchableOpacity
        activeOpacity={1}
        style={styles.category2}
        onPress={() => this.props.onPress(this.props.data)}
      >
        <FastImage
          linkImageDefault={Images.defaultImage}
          linkImage={this.props.data.image_full_size}
          style={styles.category_img2}
          resizeMode="cover"
        />
        {/* <AsyncImage
          source={{ uri: this.props.data.image_full_size }}
          style={styles.category_img2}
          resizeMode="cover"
        /> */}
        <View style={styles.content}>
          <Text style={styles.title}>{this.props.data.title}</Text>

          <Text style={styles.time_ago}>
            {this.props.data.base_link + ' - ' + this.props.data.time_ago}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  renderType11 = () => {
    // Article type 1 for nothing
    return (
      <View style={styles.category11}>
        <View style={styles.category_img} />
        <View style={styles.content11} />
      </View>
    );
  };

  renderType22 = () => {
    // Article type 2 for nothing
    return (
      <View style={styles.category22}>
        <View style={styles.category_img2} />
        <View style={styles.content22} />
      </View>
    );
  };

  render() {
    if (this.props.type === 1) {
      return this.renderType1();
    }
    if (this.props.type === 2) {
      return this.renderType2();
    }
    if (this.props.type === 11) {
      return this.renderType11();
    }
    if (this.props.type === 22) {
      return this.renderType22();
    }
    return null;
  }
}

export default Article;
