import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  category: {
    flex: 1,
    backgroundColor: 'white',

    marginLeft: 7 * d.ratioW,
    marginRight: 7 * d.ratioW,
    marginTop: 5 * d.ratioH,
    marginBottom: 5 * d.ratioH,
    elevation: 3,
    borderRadius: 10,
    overflow: 'hidden',
  },
  category11: {
    backgroundColor: ColorTheme.white1,
    marginLeft: 7 * d.ratioW,
    marginRight: 7 * d.ratioW,
    marginTop: 5 * d.ratioH,
    marginBottom: 5 * d.ratioH,
    elevation: 3,
    borderRadius: 10,
    overflow: 'hidden',
  },
  category_img: {
    width: d.windowSize.width - 14,
    height: 180 * d.ratioH,
  },
  content: {
    flex: 1,
    flexDirection: 'column',
  },
  content11: {
    height: 90 * d.ratioH,
    backgroundColor: 'white',
  },

  title: {
    // flex: 1,
    margin: 7,
    fontSize: 16,
    color: ColorTheme.black,
    fontWeight: 'normal',
    // fontFamily: 'tahoma',
  },
  time_ago: {
    padding: 10,
    fontSize: 12,
  },

  category2: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'row',
    marginLeft: 7 * d.ratioW,
    marginRight: 7 * d.ratioW,
    marginTop: 5 * d.ratioH,
    marginBottom: 5 * d.ratioH,
    elevation: 3,
    borderRadius: 10,
    overflow: 'hidden',
  },
  category22: {
    backgroundColor: 'white',
    flexDirection: 'row',
    marginLeft: 7 * d.ratioW,
    marginRight: 7 * d.ratioW,
    marginTop: 5 * d.ratioH,
    marginBottom: 5 * d.ratioH,
    elevation: 3,
    borderRadius: 10,
    overflow: 'hidden',
  },

  category_img2: {
    width: 140 * d.ratioW,
    height: 130 * d.ratioH,
    backgroundColor: ColorTheme.white1,
  },
  content22: {
    height: 90 * d.ratioH,
    // backgroundColor: 'red',
  },
});
