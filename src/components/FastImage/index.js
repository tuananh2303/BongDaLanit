import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import FastImage from 'react-native-fast-image';

class FastImageCustom extends PureComponent {
  state = {
    linkImage: this.props.linkImage,
    Error: false,
  };

  componentDidMount() {
    // console.log(this.props.playerImage);
  }

  render() {
    return (
      <FastImage
        source={
          this.state.Error
            ? this.props.linkImageDefault
            : { uri: this.state.linkImage }
        }
        style={this.props.style}
        resizeMode={this.props.resizeMode}
        onError={Error => this.setState({ Error: true })}
      />
    );
  }
}

export default FastImageCustom;
