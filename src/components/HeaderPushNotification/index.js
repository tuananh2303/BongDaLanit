import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

class HeaderPushNotification extends PureComponent {
  state = {};

  render() {
    return (
      <View
        style={[
          styles.viewMain,
          this.props.elevation ? { elevation: 3 } : null,
        ]}
      >
        <View style={styles.leftHeader}>{this.props.icon}</View>
        <View style={styles.viewTitle}>
          <Text style={styles.title}>{this.props.title}</Text>
        </View>
        <View style={styles.rightHeader}>{this.props.rightHeader}</View>
      </View>
    );
  }
}

export default HeaderPushNotification;
