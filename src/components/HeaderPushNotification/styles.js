import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  viewMain: {
    height: 50 * d.ratioH,
    // backgroundColor: ColorTheme.Teal900,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  //   leftHeader: {
  //     paddingLeft: 25,
  //   },
  leftHeader: {
    width: 60 * d.ratioW,
    // backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewTitle: {
    flex: 2,
  },
  rightHeader: {
    flex: 1,
    // backgroundColor: 'red',
    // alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: FontSize.large,
    color: ColorTheme.Teal900,
    fontWeight: '400',
  },
});
