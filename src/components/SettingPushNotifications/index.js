import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Image, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import SettingPushNotificationsRow from '../SettingPushNotificationsRow';

class SettingPushNotifications extends PureComponent {
  state = {};

  componentDidMount() {
    console.log(this.props.data);
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={this.props.data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => (
            <TouchableOpacity
              onPress={() => this.props.onPressTeamNotification()}
            >
              <SettingPushNotificationsRow
                title={item.title}
                sub_title={item.sub_title}
              />
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }
}

export default SettingPushNotifications;
