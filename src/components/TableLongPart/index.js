import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import styles from './styles';

class TableLongPart extends PureComponent {
  state = {};

  render() {
    return <View style={styles.container}>{this.props.content}</View>;
  }
}

export default TableLongPart;
