import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    height: 170 * d.ratioH,
    flexDirection: 'column',
    elevation: 3,
    margin: 10,
    marginLeft: 5 * d.ratioW,
    marginRight: 5 * d.ratioW,
    backgroundColor: ColorTheme.white,
  },
});
