import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import FootballMatchInfoTitle from '../FootballMatchInfoTitle';
import InfoTeamRow from '../InfoTeamRow';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

class InfoTeam extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.container}>
        <FootballMatchInfoTitle />
        <InfoTeamRow />
        <InfoTeamRow />
      </View>
    );
  }
}

export default InfoTeam;
