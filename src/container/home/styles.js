import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  viewMain: {
    flex: 1,
    backgroundColor: ColorTheme.gray300,
  },
  viewContent: {
    flex: 1,
  },
  viewTab: {
    height: 50 * d.ratioH,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-around',
    elevation: 10,
  },
  viewTab2: {
    height: 45 * d.ratioH,
    backgroundColor: ColorTheme.Teal900,
    flexDirection: 'row',
    justifyContent: 'space-around',
    elevation: 10,
  },

  viewTab3: {
    height: 45 * d.ratioH,
    backgroundColor: ColorTheme.blue800,
    flexDirection: 'row',
    justifyContent: 'space-around',
    elevation: 10,
  },
  viewTab4: {
    height: 45 * d.ratioH,
    backgroundColor: ColorTheme.black,
    flexDirection: 'row',
    justifyContent: 'space-around',
    elevation: 10,
  },

  tabViewMain: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  textActive: {
    color: ColorTheme.Teal900,
    fontSize: FontSize.medium - 3,
  },

  textNotActive: {
    color: ColorTheme.gray500,
    fontSize: FontSize.medium - 3,
  },

  ModalInside: {
    backgroundColor: ColorTheme.grayModalTransparent,
    // flex: 1,
    height: d.windowSize.height,
    width: d.windowSize.width,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },

  closeModal: {
    height: 50 * d.ratioH,
    width: 50 * d.ratioH,
    borderRadius: 25 * d.ratioH,
    backgroundColor: ColorTheme.white2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  infoPopup: {
    flex: 1,
    alignItems: 'center',
    // marginBottom: 20 * d.ratioH,
    width: 250 * d.ratioW,
    borderRadius: 25,
    backgroundColor: ColorTheme.white,
  },
  txt_infoPopup: {
    fontSize: FontSize.large,
    color: ColorTheme.black,
  },
  lottieView: {
    // marginBottom: -40 * d.ratioH,
    // backgroundColor: 'red',
    flex: 1.5,
  },
  btnClose: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  rowInfoPopup: {
    height: 35 * d.ratioH,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
