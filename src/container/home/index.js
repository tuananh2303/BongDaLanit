import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Modal, Platform } from 'react-native';
import { connect } from 'react-redux';
import LottieView from 'lottie-react-native';
import firebase from 'react-native-firebase';

import Icon from 'react-native-vector-icons/Ionicons';

import styles from './styles';
import LanguageVI from '../../constants/Language';
import Calendar from '../calendar';
import Game from '../game';
import News from '../news';
import Setting from '../setting';
import { ColorTheme } from '../../themes';
import Lottie from '../../themes/Lottie';
import Chat from '../chat';

// const HOME = 'HOME';
const CALENDAR = 'CALENDAR';
const GAME = 'GAME';
const NEWS = 'NEWS';
const SETTING = 'SETTING';
// const CHAT = 'CHAT';

class Home extends PureComponent {
  state = {
    tab: GAME,
    show_popup: false,
    is_new_member: false,
    point_reward: null,
  };

  componentWillMount() {
    if (Platform.OS != 'ios') {
      firebase
        .messaging()
        .hasPermission()
        .then(enabled => {
          if (enabled) {
            // user has permissions
            console.log('enable');
          } else {
            // user doesn't have permission
            firebase
              .messaging()
              .requestPermission()
              .then(() => {
                // User has authorised
                console.log('User has authorised');
              })
              .catch(error => {
                // User has rejected permissions
                console.log(error);
              });
          }
        });
    }
  }

  fetchPostDetail = data => {
    console.log(data);
  };

  notifications = async () => {
    try {
      firebase.messaging().subscribeToTopic('BONGDA');
      // Receive messege when app close
      const initialNotification = await firebase
        .notifications()
        .getInitialNotification();
      this.fetchPostDetail(initialNotification);
      firebase.notifications().getInitialNotification(notification => {
        this.fetchPostDetail(notification);
      });

      // Receive messege when app on foreground and background
      firebase.notifications().onNotificationOpened(notification => {
        this.fetchPostDetail(notification);
      });
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {
    if (Platform.OS != 'ios') {
      this.notifications();
    }

    console.log('Home', this.props.userReducer.dataFull);
    if (this.props.userReducer.dataFull != null) {
      if (this.props.userReducer.dataFull.reward.is_new_member) {
        this.setState({
          is_new_member: this.props.userReducer.dataFull.reward.is_new_member,
          point_reward: this.props.userReducer.dataFull.reward.point,
        });
      }
      if (
        !this.props.userReducer.dataFull.reward.is_new_member &&
        this.props.userReducer.dataFull.reward.show_popup
      ) {
        this.setState({
          show_popup: this.props.userReducer.dataFull.reward.show_popup,
          point_reward: this.props.userReducer.dataFull.reward.point,
        });
      }
    }
  }

  renderCalendar = () => <Calendar navigation={this.props.navigation} />;

  renderGame = () => <Game navigation={this.props.navigation} />;

  renderNews = () => <News navigation={this.props.navigation} />;

  renderSetting = () => <Setting navigation={this.props.navigation} />;

  renderChat = () => <Chat />;

  renderSwitchTab = type => {
    // if (type === HOME) {
    //   return this.renderHome();
    // }
    if (type === CALENDAR) {
      return this.renderCalendar();
    }
    if (type === GAME) {
      return this.renderGame();
    }
    if (type === NEWS) {
      return this.renderNews();
    }
    if (type === SETTING) {
      return this.renderSetting();
    }

    return null;
  };

  getStyleViewTab = () => {
    if (this.props.settingReducer.data.theme == 2) {
      return styles.viewTab2;
    }
    if (this.props.settingReducer.data.theme == 3) {
      return styles.viewTab3;
    }
    if (this.props.settingReducer.data.theme == 4) {
      return styles.viewTab4;
    }
    if (this.props.settingReducer.data.theme == 5) {
      return [styles.viewTab4, { backgroundColor: ColorTheme.purple700 }];
    }
    return styles.viewTab;
  };

  getStyleIconActive = () => {
    if (this.props.settingReducer.data.theme == 1) {
      return ColorTheme.Teal900;
    }
    return ColorTheme.white;
  };

  getStyleIconNotActive = () => {
    if (this.props.settingReducer.data.theme == 1) {
      return ColorTheme.gray500;
    }
    return ColorTheme.gray300;
  };

  renderTabIcon = (type, iconName, lableText) => {
    if (this.state.tab === type) {
      return (
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => {
            this.setState({
              tab: type,
            });
          }}
          style={styles.tabViewMain}
        >
          <Icon name={iconName} size={22} color={this.getStyleIconActive()} />
          {/* <Text style={styles.textActive}>{lableText}</Text> */}
        </TouchableOpacity>
      );
    }
    return (
      <TouchableOpacity
        activeOpacity={1}
        style={styles.tabViewMain}
        onPress={() =>
          this.setState({
            tab: type,
          })
        }
      >
        <Icon name={iconName} size={22} color={this.getStyleIconNotActive()} />
        {/* <Text style={styles.textNotActive}>{lableText}</Text> */}
      </TouchableOpacity>
    );
  };

  renderModal = () => {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.show_popup || this.state.is_new_member}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}
      >
        <View>
          <View style={styles.ModalInside}>
            <View style={styles.lottieView}>
              <LottieView
                source={Lottie.reward}
                autoPlay
                loop
                style={{ height: 250, width: 250 }}
              />
            </View>
            <View style={styles.infoPopup}>
              <View style={styles.rowInfoPopup}>
                <Text style={styles.txt_infoPopup}>Chúc mừng</Text>
              </View>
              <View style={styles.rowInfoPopup}>
                <Text style={styles.txt_infoPopup}>
                  {this.state.is_new_member
                    ? 'Tham gia game'
                    : 'điểm danh ngày mới'}
                </Text>
              </View>
              <View style={styles.rowInfoPopup}>
                <Text style={styles.txt_infoPopup}>Bạn nhận được</Text>
              </View>
              <View style={styles.rowInfoPopup}>
                <Text style={styles.txt_infoPopup}>
                  {this.state.point_reward}
                  {' point'}
                </Text>
              </View>
            </View>
            <View style={styles.btnClose}>
              <TouchableOpacity
                style={styles.closeModal}
                onPress={() => this.setState({ show_popup: false })}
              >
                <Icon name="ios-close" size={30} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    return (
      <View style={styles.viewMain}>
        {this.renderModal()}
        <View style={styles.viewContent}>
          {this.renderSwitchTab(this.state.tab)}
        </View>

        <View style={this.getStyleViewTab()}>
          {this.renderTabIcon(NEWS, 'md-paper', LanguageVI.News)}
          {this.renderTabIcon(CALENDAR, 'md-calendar', LanguageVI.Calendar)}
          {this.renderTabIcon(GAME, 'logo-game-controller-b', LanguageVI.Game)}
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('Chat');
            }}
            style={styles.tabViewMain}
          >
            <Icon
              name="ios-chatbubbles"
              size={22}
              color={this.getStyleIconNotActive()}
            />
          </TouchableOpacity>
          {this.renderTabIcon(SETTING, 'ios-settings', LanguageVI.Setting)}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  userReducer: state.userReducer,
  settingReducer: state.settingReducer,
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
