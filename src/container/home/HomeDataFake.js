const data = [
  {
    id: 1,
    team_name: 'Cordelie Logue',
    data: [
      {
        end: true,
        team1_name: 'Rafa Yedy',
        team2_name: 'Kalina May',
        score1: 4,
        score2: 2,
      },
      {
        end: false,
        team1_name: 'Rafa Yedy',
        team2_name: 'Kalina May',
        score1: 4,
        score2: 2,
      },
      {
        end: false,
        team1_name: 'Rafa Yedy',
        team2_name: 'Kalina May',
        score1: 3,
        score2: 2,
      },
    ],
  },
  {
    id: 2,
    team_name: 'Cordelie Martinot',
    data: [
      {
        end: false,
        team1_name: 'Rafa Yedy',
        team2_name: 'Kalina May',
        score1: 4,
        score2: 2,
      },
      {
        end: true,
        team1_name: 'Rafa Yedy',
        team2_name: 'Kalina May',
        score1: 4,
        score2: 2,
      },
    ],
  },
  {
    id: 3,
    team_name: 'Rafi Huertas',
    data: [
      {
        end: true,
        team1_name: 'Rafa Yedy',
        team2_name: 'Kalina May',
        score1: 4,
        score2: 2,
      },
      {
        end: false,
        team1_name: 'Rafa Yedy',
        team2_name: 'Kalina May',
        score1: 4,
        score2: 2,
      },
    ],
  },
];

export default data;
