import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  compose_avatar_main: {
    flexDirection: 'row',
  },

  avatar: {
    height: 40 * d.ratioH,
    width: 40 * d.ratioH,
    borderRadius: 40,
  },
});
