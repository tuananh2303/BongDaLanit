import React, { PureComponent } from 'react';
import {
  GiftedChat,
  Bubble,
  Actions,
  SystemMessage,
  Send,
  Composer,
  MessageText,
  CustomBubbleView,
} from 'react-native-gifted-chat';
import { LoginButton, AccessToken, LoginManager } from 'react-native-fbsdk';

import Axios from 'axios';
import { connect } from 'react-redux';
import { Buffer } from 'buffer';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  AsyncStorage,
  Alert,
} from 'react-native';
import * as d from '../../utilities/Tranform';
import styles from './styles';
import Header from '../../components/Header';
import {
  URL_CHAT_SEND,
  URL_CHAT_GET_MESS,
  URL_SIGNIN,
  URL_GET_FB_INFO,
  URL_IMAGE,
  URL_CHAT_DEL_MESS,
} from '../../constants/URL';
import LanguageVI from '../../constants/Language';
import { ColorTheme, Images } from '../../themes';

import { addUserInfo, userInfo } from '../../redux/actions/userAction';
import { LOGIN_REF } from '../../constants/AsyncStorage';
import IconBackCustom from '../../components/IconBackCustom';

class Chat extends PureComponent {
  state = {
    messages: [],
    user_id: 1,
  };

  componentDidMount() {
    this.checkLogin();
    this.getMess();
    this._interval_msg = setInterval(() => {
      this.getMess();
    }, 10000);
  }

  checkLogin = () => {
    if (this.props.userReducer.dataFull != null) {
      this.setState({
        user_id: this.props.userReducer.dataFull.user.id,
      });
    }
  };

  facebookLogin = () => {
    LoginManager.logInWithReadPermissions(['public_profile']).then(
      result => {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          AccessToken.getCurrentAccessToken().then(data => {
            this.getFacebookInfo(data.accessToken.toString());
          });
        }
      },
      error => {
        console.log('Login fail with error: ' + error);
      },
    );
  };

  getFacebookInfo = async accessToken => {
    await Axios.get(URL_GET_FB_INFO + accessToken)
      .then(response => {
        this.pushDataFacebookToServer(
          response.data.id,
          response.data.name,
          accessToken,
        );
      })
      .catch(error => {
        console.log(error);
      });
  };

  pushDataFacebookToServer = async (fbid, name, token) => {
    const dataFB = {
      fbid,
      name,
      token,
    };
    await Axios.post(URL_SIGNIN, dataFB)
      .then(response => {
        if (response.data.status == 'success') {
          AsyncStorage.setItem(LOGIN_REF, JSON.stringify(dataFB));
          this.props.addUserInfo(dataFB);
          this.props.userInfo(response.data);
          this.checkLogin();
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  getMess = () => {
    Axios.get(URL_CHAT_GET_MESS)
      .then(response => {
        // handle success
        // console.log(response);
        if (response.data.status == 'success') {
          this.setState({
            messages: response.data.data,
          });
        }
      })
      .catch(error => {
        // handle error
        console.log(error);
      });
  };

  componentWillUnmount() {
    clearInterval(this._interval_msg);
  }

  onSend(messages = []) {
    // console.log(messages[0]);
    const postData = {
      _id: messages[0]._id,
      user_id: this.props.userReducer.dataFull.user.id,
      text: messages[0].text,
    };
    Axios.post(
      URL_CHAT_SEND + this.props.userReducer.dataFull.user.token,
      postData,
    )
      .then(response => {
        // handle success
        console.log(response);
        this.setState({
          messages: response.data.data,
        });
      })
      .catch(error => {
        // handle error
        console.log(error);
      });
  }

  renderMessageText = props => {
    if (props.currentMessage.text) {
      props.currentMessage.text = Buffer.from(
        props.currentMessage.text,
        'base64',
      ).toString('utf-8');
    }
    return (
      <View>
        <MessageText {...props} />
      </View>
    );
  };

  getColor = () => {
    if (this.props.settingReducer.data.theme == 3) {
      return '#0084ff';
    }
    if (this.props.settingReducer.data.theme == 4) {
      return ColorTheme.black;
    }
    if (this.props.settingReducer.data.theme == 5) {
      return ColorTheme.purple700;
    }
    return ColorTheme.Teal900;
  };

  renderName = props => {
    let previousMessageId = null;
    if (props.previousMessage.user != null) {
      previousMessageId = props.previousMessage.user._id;
    }

    if (props.currentMessage.user._id == this.state.user_id) {
      return null;
    }
    if (props.currentMessage.user._id == previousMessageId) {
      return null;
    }
    return <Text>{props.currentMessage.user.name}</Text>;
  };

  renderBubble = props => {
    if (props.currentMessage.text) {
      props.currentMessage.text = Buffer.from(
        props.currentMessage.text,
        'base64',
      ).toString('utf-8');
    }
    return (
      <View>
        {this.renderName(props)}
        <Bubble
          {...props}
          wrapperStyle={{
            left: {
              backgroundColor: ColorTheme.white1,
            },
            right: {
              backgroundColor: this.getColor(),
            },
          }}
        />
      </View>
    );
  };

  renderComposer = props => {
    if (this.props.userReducer.dataFull != null) {
      if (this.props.userReducer.dataFull.class == 'banned') {
        return null;
      }
      return (
        <Composer
          ref={input => {
            this._composer = input;
          }}
          {...props}
          value={props.text}
        />
      );
    }
    return (
      <TouchableOpacity onPress={() => this.facebookLogin()}>
        <Image
          source={Images.Facebook_Login}
          style={{
            height: 60,
            width: d.windowSize.width,
          }}
          resizeMode="contain"
        />
      </TouchableOpacity>
    );
  };

  findIndexMess = message => {
    return this.state.messages.findIndex(element => element._id == message._id);
  };

  alertMess = message => {
    Alert.alert(
      LanguageVI.DeleteComment,
      LanguageVI.confirmDelete,
      [
        {
          text: LanguageVI.Cancel,
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            Axios.get(
              URL_CHAT_DEL_MESS +
                this.findIndexMess(message) +
                '/' +
                this.props.userReducer.dataFull.user.id +
                '?token=' +
                this.props.userReducer.dataFull.user.token,
            )
              .then(response => {
                console.log(response);
                if (response.data.status == 'success') {
                  this.setState({
                    messages: response.data.data,
                  });
                }
              })
              .catch(error => {
                console.log(error);
              });
          },
        },
      ],
      { cancelable: false },
    );
  };

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <Header
          elevation={true}
          title={LanguageVI.Chat}
          icon={
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <IconBackCustom />
            </TouchableOpacity>
          }
        />
        <GiftedChat
          messages={this.state.messages}
          onSend={messages => this.onSend(messages)}
          user={{
            _id: this.state.user_id,
          }}
          placeholder={LanguageVI.TypeYourMessage}
          renderBubble={this.renderBubble}
          dateFormat="L"
          locale="vi"
          renderComposer={this.renderComposer}
          onLongPress={(context, message) => {
            if (this.props.userReducer.dataFull.class == 'admin') {
              this.alertMess(message);
            }
          }}
          onPressAvatar={user => {
            console.log(user);

            this.props.navigation.navigate('Profile', { id: user._id });
          }}
        />
      </View>
    );
  }
}
const mapStateToProps = state => ({
  settingReducer: state.settingReducer,
  userReducer: state.userReducer,
});

const mapDispatchToProps = dispatch => ({
  addUserInfo: data => dispatch(addUserInfo(data)),
  userInfo: data => dispatch(userInfo(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Chat);
