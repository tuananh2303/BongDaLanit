import React, { PureComponent } from 'react';
import styles from './styles';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Switch,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { rowIcon, row } from '../../components/Row';
import LanguageVI from '../../constants/Language';
import Header from '../../components/Header';

import { ColorTheme } from '../../themes';
import IconBackCustom from '../../components/IconBackCustom';

class SettingPushNotificationTroubleshoot extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.container}>
        <Header
          title={LanguageVI.TroubleshootNotifications}
          rightHeader
          icon={
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <IconBackCustom />
              {/* <Icon name="md-arrow-back" size={22} color={ColorTheme.Teal900} /> */}
            </TouchableOpacity>
          }
          // elevation={true}
        />

        <View style={styles.row}>
          <TouchableOpacity
            style={styles.rowsub}
            onPress={() =>
              this.props.navigation.navigate('SettingMatchHideShow')
            }
          >
            {row('md-sync', LanguageVI.SyncNotifications)}
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.rowsub}
            onPress={() =>
              this.props.navigation.navigate('SettingMatchHideShow')
            }
          >
            {row('md-help-circle', LanguageVI.FAQ)}
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.rowsub}
            onPress={() =>
              this.props.navigation.navigate('SettingMatchHideShow')
            }
          >
            {row('md-chatboxes', LanguageVI.ContactUs)}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default SettingPushNotificationTroubleshoot;
