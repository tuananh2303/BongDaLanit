import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    // height: 50 * d.ratioH,
    // justifyContent: 'center',
    // elevation: 2,
    borderBottomColor: ColorTheme.gray500,
    borderBottomWidth: 0.25,
    backgroundColor: 'white',
    borderTopColor: ColorTheme.gray500,
    borderTopWidth: 0.25,
  },
  stylecontainer: {
    borderColor: 'white',
    backgroundColor: 'white',
  },
  Row: {
    flexDirection: 'row',
    paddingTop: 7 * d.ratioH,
    paddingBottom: 7 * d.ratioH,
  },
  iconRadio: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'orange',
  },
  textIconRadio: {
    flex: 4,
    // justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 10 * d.ratioW,
    flexDirection: 'row',
    // backgroundColor: 'green',
  },
  viewTextRadio: {
    flex: 3,
    marginTop: 14 * d.ratioH,
    // backgroundColor: 'green',
  },
  buttonRadio: {
    flex: 1,
  },
  txt_nd: {
    color: ColorTheme.black,
    fontSize: 14,
    // fontWeight: '500',
    // backgroundColor: 'blue',
  },
  ShowCustomise: {
    borderTopWidth: 0.25,
    borderTopColor: ColorTheme.gray500,
  },
});
