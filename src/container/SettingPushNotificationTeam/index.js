import React, { PureComponent } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import LanguageVI from '../../constants/Language';
import Header from '../../components/Header';
import Switch from '../../components/Switch';
import styles from './styles';
import { ColorTheme } from '../../themes';
import { CheckBox } from 'react-native-elements';
import IconBackCustom from '../../components/IconBackCustom';

const TOGGLESWITCH = 'toggleSwitch';
const TOGGLESWITCH1 = 'toggleSwitch1';
const TOGGLESWITCH2 = 'toggleSwitch2';
const TOGGLESWITCH3 = 'toggleSwitch3';
const TOGGLESWITCH4 = 'toggleSwitch4';
const TOGGLESWITCH5 = 'toggleSwitch5';
const TOGGLESWITCH6 = 'toggleSwitch6';
const TOGGLESWITCH7 = 'toggleSwitch7';
const TOGGLESWITCH8 = 'toggleSwitch8';
const TOGGLESWITCH9 = 'toggleSwitch9';

class SettingPushNotificationTeam extends PureComponent {
  state = {
    checked: false,
    checked2: false,
    checked1: true,
    SwitchOnValueHolder: false,
    SwitchOnValueHolder1: false,
    SwitchOnValueHolder2: false,
    SwitchOnValueHolder3: false,
    SwitchOnValueHolder4: false,
    SwitchOnValueHolder5: false,
    SwitchOnValueHolder6: false,
    SwitchOnValueHolder7: false,
    SwitchOnValueHolder8: false,
    SwitchOnValueHolder9: false,
  };

  // xet toggleSwitch.....................................

  toggleSwitch = type => {
    if (type === TOGGLESWITCH) {
      return this.setState({
        SwitchOnValueHolder: !this.state.SwitchOnValueHolder,
      });
    }

    if (type === TOGGLESWITCH1) {
      return this.setState({
        SwitchOnValueHolder1: !this.state.SwitchOnValueHolder1,
      });
    }

    if (type === TOGGLESWITCH2) {
      return this.setState({
        SwitchOnValueHolder2: !this.state.SwitchOnValueHolder2,
      });
    }

    if (type === TOGGLESWITCH3) {
      return this.setState({
        SwitchOnValueHolder3: !this.state.SwitchOnValueHolder3,
      });
    }

    if (type === TOGGLESWITCH4) {
      return this.setState({
        SwitchOnValueHolder4: !this.state.SwitchOnValueHolder4,
      });
    }

    if (type === TOGGLESWITCH5) {
      return this.setState({
        SwitchOnValueHolder5: !this.state.SwitchOnValueHolder5,
      });
    }

    if (type === TOGGLESWITCH6) {
      return this.setState({
        SwitchOnValueHolder6: !this.state.SwitchOnValueHolder6,
      });
    }

    if (type === TOGGLESWITCH7) {
      return this.setState({
        SwitchOnValueHolder7: !this.state.SwitchOnValueHolder7,
      });
    }

    if (type === TOGGLESWITCH8) {
      return this.setState({
        SwitchOnValueHolder8: !this.state.SwitchOnValueHolder8,
      });
    }

    if (type === TOGGLESWITCH9) {
      return this.setState({
        SwitchOnValueHolder9: !this.state.SwitchOnValueHolder9,
      });
    }
  };

  // xet hien an nut checkbox...................................

  onPressSelectChecke = type => {
    if (type === 'checked') {
      return this.setState({
        checked: !this.state.checked,
        checked2: false,
        checked1: false,
      });
    }

    if (type === 'checked1') {
      return this.setState({
        checked1: !this.state.checked1,
        checked2: false,
        checked: false,
      });
    }

    if (type === 'checked2') {
      return this.setState({
        checked2: !this.state.checked2,
        checked: false,
        checked1: false,
      });
    }
  };

  rowToggleSwitch = (toggleSwitch, icon, title, stateValue) => {
    return (
      <TouchableOpacity
        style={styles.rowsub}
        onPress={() => this.toggleSwitch(toggleSwitch)}
      >
        {/* {rowIcon(icon, title, stateValue)} */}
        <Switch
          icon={icon}
          title={title}
          value={stateValue}
          toggleSwitch1={() => this.toggleSwitch(toggleSwitch)}
        />
      </TouchableOpacity>
    );
  };

  // xet hien an Customise.......................................

  ShowCustomise = type => {
    if (type == false) {
      return (
        <TouchableOpacity onPress={() => this.onPressSelectChecke('checked2')}>
          {this.rowIconChecked(
            'md-create',
            LanguageVI.Customise,
            this.state.checked2,
            'checked2',
          )}
        </TouchableOpacity>
      );
    }

    if (type == true) {
      return (
        <View style={styles.ShowCustomise}>
          <TouchableOpacity
            onPress={() => this.onPressSelectChecke('checked2')}
          >
            {this.rowIconChecked(
              'md-create',
              LanguageVI.Customise,
              this.state.checked2,
              'checked2',
            )}
          </TouchableOpacity>

          {this.rowToggleSwitch(
            TOGGLESWITCH,
            'md-time',
            LanguageVI.MatchReminder,
            this.state.SwitchOnValueHolder,
          )}

          {this.rowToggleSwitch(
            TOGGLESWITCH1,
            'ios-star',
            LanguageVI.Lineup,
            this.state.SwitchOnValueHolder1,
          )}

          {this.rowToggleSwitch(
            TOGGLESWITCH2,
            'ios-star',
            LanguageVI.MatchStart,
            this.state.SwitchOnValueHolder2,
          )}

          {this.rowToggleSwitch(
            TOGGLESWITCH3,
            'ios-star',
            LanguageVI.Goals,
            this.state.SwitchOnValueHolder3,
          )}

          {this.rowToggleSwitch(
            TOGGLESWITCH4,
            'ios-star',
            LanguageVI.VideoHighlights,
            this.state.SwitchOnValueHolder4,
          )}

          {this.rowToggleSwitch(
            TOGGLESWITCH5,
            'ios-star',
            LanguageVI.RedCards,
            this.state.SwitchOnValueHolder5,
          )}

          {this.rowToggleSwitch(
            TOGGLESWITCH6,
            'ios-star',
            LanguageVI.HalfTimeResult,
            this.state.SwitchOnValueHolder6,
          )}

          {this.rowToggleSwitch(
            TOGGLESWITCH7,
            'ios-star',
            LanguageVI.FullTimeResult,
            this.state.SwitchOnValueHolder7,
          )}

          {this.rowToggleSwitch(
            TOGGLESWITCH8,
            'ios-star',
            LanguageVI.TransferNew,
            this.state.SwitchOnValueHolder8,
          )}

          {this.rowToggleSwitch(
            TOGGLESWITCH9,
            'ios-star',
            LanguageVI.Questions,
            this.state.SwitchOnValueHolder9,
          )}
        </View>
      );
    }
  };

  // 1 hang checked

  rowIconChecked = (icon, text, checked, selectChecked) => {
    return (
      <View style={styles.Row}>
        <View style={styles.iconRadio}>
          <Icon name={icon} size={23} />
        </View>
        <View style={styles.textIconRadio}>
          <View style={styles.viewTextRadio}>
            <Text style={styles.txt_nd}>{text}</Text>
          </View>
          <View style={styles.buttonRadio}>
            <CheckBox
              center
              checkedIcon="dot-circle-o"
              uncheckedIcon="circle-o"
              checked={checked}
              size={20}
              containerStyle={styles.stylecontainer}
              onPress={() => this.onPressSelectChecke(selectChecked)}
            />
          </View>
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Header
          title={LanguageVI.TeamNotifications}
          rightHeader
          icon={
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <IconBackCustom />
              {/* <Icon name="md-arrow-back" size={22} color={ColorTheme.Teal900} /> */}
            </TouchableOpacity>
          }
          // elevation={true}
        />

        <ScrollView style={styles.row}>
          <TouchableOpacity onPress={() => this.onPressSelectChecke('checked')}>
            {this.rowIconChecked(
              'ios-notifications-outline',
              LanguageVI.None,
              this.state.checked,
              'checked',
            )}
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.onPressSelectChecke('checked1')}
          >
            {this.rowIconChecked(
              'md-link',
              LanguageVI.UseDefault,
              this.state.checked1,
              'checked1',
            )}
          </TouchableOpacity>

          {this.ShowCustomise(this.state.checked2)}
        </ScrollView>
      </View>
    );
  }
}

export default SettingPushNotificationTeam;
