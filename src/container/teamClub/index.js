import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
// import DetailsFines from '../../components/DetailsFines';
import TeamTableScroce from '../../components/TeamTableScroce';
import styles from './styles';

import data from './dataFake';
import dataNewsFake from './dataNewsFake';
import dataTransfer from './dataTransfer';
import dataFixtureFake from './dataFixtureFake';

import Table from '../../components/Table';
import LanguageVI from '../../constants/Language';
import NewsRow from '../../components/NewsRow';
import InfoKickRow from '../../components/InfoKickRow';
import FixtureRow from '../../components/FixturesRow';
import { rowsubImg } from '../../components/Row';
import { ColorTheme, Images, FontSize } from '../../themes';

class TeamClub extends PureComponent {
  state = {};

  // componentDidMount() {
  //   console.log('ren club');
  // }

  renderNews = data => {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => <NewsRow data={item} />}
      />
    );
  };

  renderTransferRumours = data => {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => (
          <InfoKickRow
            onPressPlayer={() => this.props.onPressPlayer()}
            title={item.title}
            sub_title={<Text>{item.sub_title}</Text>}
          />
        )}
      />
    );
  };

  renderFixture = data => {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => (
          <FixtureRow
            onPressMatch={() => this.props.onPressMatch()}
            data={item}
          />
        )}
      />
    );
  };

  renderCompe = data => {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => (
          <TouchableOpacity onPress={() => this.props.onPressLeague()}>
            {rowsubImg(Images.england, item.sub_title, LanguageVI.Ongoing)}
          </TouchableOpacity>
        )}
      />
    );
  };

  render() {
    return (
      <View style={styles.ViewMain}>
        <TeamTableScroce
          data={data}
          onPressTeam={() => this.props.onPressTeam()}
        />

        <Table
          title={LanguageVI.News}
          subTitle={LanguageVI.SortByPopularity}
          showAllText={LanguageVI.SHOWALL}
          content={this.renderNews(dataNewsFake)}
          onPressShowAll={() => this.props.onPressShowAllNews()}
        />

        <Table
          title={LanguageVI.TransferRumours}
          subTitle={LanguageVI.SortByPopularity}
          showAllText={LanguageVI.SHOWALL}
          content={this.renderTransferRumours(dataTransfer)}
          onPressShowAll={() => this.props.onPressShowAllTransfer()}
        />

        <Table
          title={LanguageVI.Fixtures}
          showAllText={LanguageVI.SHOWALL}
          content={this.renderFixture(dataFixtureFake)}
          onPressShowAll={() => this.props.onPressShowAllFixture()}
        />
        <Table
          title={LanguageVI.CompetesIn}
          showAllText={LanguageVI.SHOWALL}
          content={this.renderCompe(dataNewsFake)}
          onPressShowAll={() => console.log('press show all')}
        />
      </View>
    );
  }
}

export default TeamClub;
