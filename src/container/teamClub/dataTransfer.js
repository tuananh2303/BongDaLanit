const dataTransfer = [
  {
    id: 1,
    title: 'Bria Glaisner',
    sub_title: 'Glaisner',
    data: {
      end: true,
      team1: 'Rafa Yedy',
      team2: 'Kalina May',
      score1: 4,
      score2: 2,
    },
  },
  {
    id: 2,
    title: 'Derry Belliveau',
    sub_title: 'Belliveau',
    data: {
      end: true,
      team1: 'Rafa Yedy',
      team2: 'Kalina May',
      score1: 4,
      score2: 2,
    },
  },
  {
    id: 3,
    title: 'Ivette McCafferky',
    sub_title: 'McCafferky',
    data: {
      end: true,
      team1: 'Rafa Yedy',
      team2: 'Kalina May',
      score1: 4,
      score2: 2,
    },
  },
];
export default dataTransfer;
