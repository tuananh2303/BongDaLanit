const data = {
  leagh: 'Sidoney',
  data: [
    {
      id: 1,
      team_name: 'Coral',
      status: 1,
      so_tran: 12,
      hieu_so: 18,
      diem: 5,
    },
    {
      id: 2,
      team_name: 'Blake',
      status: 1,
      so_tran: 26,
      hieu_so: 35,
      diem: 4,
    },
    {
      id: 3,
      team_name: 'Shamus',
      status: 2,
      so_tran: 3,
      hieu_so: 4,
      diem: 24,
    },
    {
      id: 4,
      team_name: 'Rina',
      status: 3,
      so_tran: 9,
      hieu_so: 33,
      diem: 30,
    },
  ],
};

export default data;
