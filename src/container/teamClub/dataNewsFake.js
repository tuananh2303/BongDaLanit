const dataNews = [
  {
    id: 1,
    title: 'Morbi non lectus.',
    sub_title: 'Sherwin',
  },
  {
    id: 2,
    title: 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat.',
    sub_title: 'Royal',
  },
  {
    id: 3,
    title: 'Vivamus vestibulum sagittis sapien.',
    sub_title: 'Bobbye',
  },
  {
    id: 4,
    title: 'Vestibulum ac est lacinia nisi venenatis tristique.',
    sub_title: 'Hewitt',
  },
];
export default dataNews;
