import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';
// import { ColorTheme, FontSize } from '../../themes';

export default StyleSheet.create({
  viewMain: {
    flex: 1,
    backgroundColor: 'white',
  },
  top: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottom: {
    flex: 1,
    // justifyContent: 'flex-end',
    alignItems: 'center',
  },
  Image: {
    height: 200 * d.ratioH,
    width: d.windowSize.width,
  },
  WelcomeText: {
    color: ColorTheme.Teal900,
    fontSize: FontSize.extraLarge - 5,
  },

  QickSetupbtn: {
    marginTop: 60 * d.ratioH,
    height: 50 * d.ratioH,
    width: d.windowSize.width / 3,
    backgroundColor: ColorTheme.Teal900,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    elevation: 3,
  },
  QickSetupText: {
    color: ColorTheme.white,
    fontSize: FontSize.medium,
  },
  Privacy: {
    color: ColorTheme.Teal900,
    textDecorationLine: 'underline',
    fontSize: FontSize.medium - 6,
    marginTop: 30 * d.ratioH,
  },
});
