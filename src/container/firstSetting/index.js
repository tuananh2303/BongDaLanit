import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Images } from '../../themes';
import LanguageVI from '../../constants/Language';

class FirstSetting extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.viewMain}>
        <View style={styles.top}>
          <Image
            resizeMode="contain"
            source={Images.startUpImage}
            style={styles.Image}
          />
        </View>
        <View style={styles.bottom}>
          <Text style={styles.WelcomeText}>{LanguageVI.WelcomeTo}</Text>
          <Text style={styles.WelcomeText}>{LanguageVI.LanitFootball}</Text>

          <TouchableOpacity
            style={styles.QickSetupbtn}
            onPress={() => this.props.navigation.navigate('FirstSettingTab')}
          >
            <Text style={styles.QickSetupText}>{LanguageVI.QickSetup}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Privacy')}
          >
            <Text style={styles.Privacy}>
              {LanguageVI.TermsofUsePrivacyPolicy}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default FirstSetting;
