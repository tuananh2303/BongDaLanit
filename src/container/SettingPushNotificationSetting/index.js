import React, { PureComponent } from 'react';
import styles from './styles';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { rowIcon, row } from '../../components/Row';
import LanguageVI from '../../constants/Language';
import Header from '../../components/Header';
import Switch from '../../components/Switch';
import { ColorTheme } from '../../themes';
import IconBackCustom from '../../components/IconBackCustom';

const TOGGLESWITCH = 'toggleSwitch';
const TOGGLESWITCH1 = 'toggleSwitch1';

class SettingPushNotificationSetting extends PureComponent {
  state = { SwitchOnValueHolder: false, SwitchOnValueHolder1: false };

  toggleSwitch = type => {
    if (type === TOGGLESWITCH) {
      return this.setState({
        SwitchOnValueHolder: !this.state.SwitchOnValueHolder,
      });
    }

    if (type === TOGGLESWITCH1) {
      return this.setState({
        SwitchOnValueHolder1: !this.state.SwitchOnValueHolder1,
      });
    }
  };

  rowToggleSwitch = (toggleSwitch, icon, title, stateValue) => {
    return (
      <TouchableOpacity
        style={styles.rowsub}
        onPress={() => this.toggleSwitch(toggleSwitch)}
      >
        {/* {rowIcon(icon, title, stateValue)} */}
        <Switch
          icon={icon}
          title={title}
          value={stateValue}
          toggleSwitch1={() => this.toggleSwitch(toggleSwitch)}
        />
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Header
          title={LanguageVI.NotificationSettings}
          rightHeader
          icon={
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <IconBackCustom />
              {/* <Icon name="md-arrow-back" size={22} color={ColorTheme.Teal900} /> */}
            </TouchableOpacity>
          }
          // elevation={true}
        />

        <View style={styles.row}>
          {this.rowToggleSwitch(
            TOGGLESWITCH,
            'ios-phone-portrait',
            LanguageVI.Vibration,
            this.state.SwitchOnValueHolder,
          )}

          {this.rowToggleSwitch(
            TOGGLESWITCH1,
            'md-volume-mute',
            LanguageVI.Sound,
            this.state.SwitchOnValueHolder1,
          )}

          <TouchableOpacity
            style={styles.rowsub}
            onPress={() =>
              this.props.navigation.navigate('SettingMatchHideShow')
            }
          >
            {row('ios-musical-note', LanguageVI.NotificationSound)}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default SettingPushNotificationSetting;
