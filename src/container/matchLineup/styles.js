import { StyleSheet, Platform } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  viewMain: {
    flex: 1,
    backgroundColor: ColorTheme.gray300,
  },
  ImageBackground: {
    width: d.windowSize.width,
  },
  viewFieldTop: {
    flex: 1,

    backgroundColor: ColorTheme.gray300,
  },
  viewFieldBottom: {
    paddingTop: 10 * d.ratioH,
    justifyContent: 'center',
    flex: 1,
    backgroundColor: ColorTheme.gray300,
  },
  viewPart: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: ColorTheme.white2,
  },

  titleText: {
    color: ColorTheme.gray,
    marginLeft: 10 * d.ratioW,
    marginBottom: 10 * d.ratioH,
    fontSize: FontSize.medium,
  },
});
