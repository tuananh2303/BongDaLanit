import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  FlatList,
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import MatchLineUpField from '../../components/MatchLineUpField';
import { Images, ColorTheme } from '../../themes';
import InfoKickRow from '../../components/InfoKickRow';
import LanguageVI from '../../constants/Language';
import TableLongPart from '../../components/TableLongPart';
import * as d from '../../utilities/Tranform';
import EmptyComponent from '../../components/EmptyComponent';

class MatchLineup extends PureComponent {
  state = {};

  renderMatchField = (data, type) => {
    if (type == 'home') {
      data.pop();
      console.log(data);
    }

    if (type == 'away') {
      data.pop();
      data.reverse();
    }

    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => (
          <View style={{ flexDirection: 'row' }}>
            {item.map((item1, index1) => (
              <View style={{ flex: 1 }} key={index1}>
                <MatchLineUpField
                  onPressPlayer={() => this.props.onPressPlayer()}
                  playerNumber={item1[3]}
                  playerName={item1[1]}
                  playerImage={item1[2]}
                />
              </View>
            ))}
          </View>
        )}
      />
    );
  };

  renderDefender = (data, title) => {
    return (
      <View style={styles.viewPart}>
        <Text style={styles.titleText}>{title}</Text>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => (
            <InfoKickRow
              playerImage={item[2]}
              onPressPlayer={() => this.props.onPressPlayer()}
              title={item[3] + '. ' + item[1]}
              // sub_title={<Text>{LanguageVI.NoMatchesYet}</Text>}
            />
          )}
        />
      </View>
    );
  };

  getHeightImage = numberItem => {
    return numberItem * 80 * d.ratioH;
  };

  render() {
    // console.log(this.props.data.lineup.home);

    if (this.props.data.lineup.home.length == 0) {
      return (
        <View style={styles.viewMain}>
          <EmptyComponent />
        </View>
      );
    }
    return (
      <View style={styles.viewMain}>
        <ImageBackground
          source={Images.SoccerFieldTop}
          style={[
            styles.ImageBackground,
            {
              height: this.getHeightImage(
                Object.values(this.props.data.lineup.home).length - 1,
              ),
            },
          ]}
          resizeMode="stretch"
        >
          <View style={styles.viewFieldTop}>
            <Text>{this.props.data.teams[1]}</Text>
            {this.renderMatchField(
              Object.values(this.props.data.lineup.home),
              'home',
            )}
          </View>
        </ImageBackground>

        <ImageBackground
          resizeMode="stretch"
          source={Images.SoccerFieldBottom}
          style={[
            styles.ImageBackground,
            {
              height: this.getHeightImage(
                Object.values(this.props.data.lineup.away).length - 1,
              ),
            },
          ]}
        >
          <View style={styles.viewFieldBottom}>
            {this.renderMatchField(
              Object.values(this.props.data.lineup.away),
              'away',
            )}
            <Text>{this.props.data.teams[4]}</Text>
          </View>
        </ImageBackground>

        <TableLongPart
          content={
            <View>
              {this.renderDefender(
                this.props.data.lineup.home.Substitution,
                LanguageVI.Subtitutes + ' (' + this.props.data.teams[1] + ')',
              )}
              {/* {this.renderDefender(injury, LanguageVI.Injuries)} */}
            </View>
          }
        />
        <TableLongPart
          content={
            <View>
              {this.renderDefender(
                this.props.data.lineup.away.Substitution,
                LanguageVI.Subtitutes + ' (' + this.props.data.teams[4] + ')',
              )}
              {/* {this.renderDefender(injury2, LanguageVI.Injuries)} */}
            </View>
          }
        />
      </View>
    );
  }
}

export default MatchLineup;
