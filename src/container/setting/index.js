import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import Header from '../../components/Header';
import LanguageVI from '../../constants/Language';
import { row, rowsub, title, rowIcon } from '../../components/Row';

class Setting extends PureComponent {
  state = {};

  render() {
    return (
      <View style={styles.viewMain}>
        <Header title={LanguageVI.Setting} rightHeader elevation={true} />

        <View style={styles.container}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('SettingMatch')}
          >
            {row('ios-settings', LanguageVI.GeneralSetting)}
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('SettingTheme')}
          >
            {row('ios-color-fill', LanguageVI.Theme)}
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('SettingData')}
          >
            {row('ios-globe', LanguageVI.UpdateFrequency)}
          </TouchableOpacity>
        </View>

        <View style={styles.Add}>
          {title(LanguageVI.Add)}
          {row('ios-information-circle-outline', LanguageVI.AppNews)}
          {row('ios-gift', LanguageVI.Promotions)}
          {row('ios-heart', LanguageVI.FollowUs)}
          {row('ios-share', LanguageVI.TellaFriend)}
          {row('ios-star', LanguageVI.ReviewOurApp)}
          {row('ios-football', LanguageVI.AboutApp)}
          {/* {row('ios-football', LanguageVI.AboutApp)} */}
        </View>
      </View>
    );
  }
}

export default Setting;
