import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  viewMain: {
    flex: 1,
    backgroundColor: 'white',
  },
  container: {
    marginTop: 10,
    borderBottomColor: ColorTheme.white2,
    borderBottomWidth: 1 * d.ratioH,
  },
  Row: {
    flexDirection: 'row',
    paddingTop: 20 * d.ratioH,
    paddingBottom: 10 * d.ratioH,
  },
  icon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    flex: 4,
    // justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 10 * d.ratioW,
    // backgroundColor: 'green',
  },
  txt_nd: {
    color: ColorTheme.black,
    fontSize: 14,
    fontWeight: '500',
    // backgroundColor: 'blue',
  },
  txt_bottom: {
    // backgroundColor: 'red',
    fontSize: 10,
  },
  help: {
    paddingTop: 20 * d.ratioH,
    borderBottomColor: ColorTheme.white2,
    borderBottomWidth: 1 * d.ratioH,
    paddingBottom: 10 * d.ratioH,
  },
  title: {
    paddingLeft: 15 * d.ratioW,
  },
  txt_title: {
    fontWeight: '500',
    fontSize: 12,
  },
  Add: {
    paddingTop: 20 * d.ratioH,
    paddingBottom: 10 * d.ratioH,
  },
});
