import React, { PureComponent } from 'react';
import { Text, View, TouchableOpacity, BackHandler } from 'react-native';
import { connect } from 'react-redux';

import Icon from 'react-native-vector-icons/Ionicons';
import LanguageVI from '../../constants/Language';
import Header from '../../components/Header';
import { ColorTheme } from '../../themes';
import styles from './styles';
import storeData from '../../utilities/AsynStorage';

import { settingTheme } from '../../redux/actions/settingAction';
import { SETTING_APP } from '../../constants/AsyncStorage';
import IconBackCustom from '../../components/IconBackCustom';

const SWITCH1 = 'Switch';
const SWITCH2 = 'SWITCH2';
const SWITCH3 = 'SWITCH3';
const SWITCH4 = 'SWITCH4';
const SWITCH5 = 'SWITCH5';

class SettingTheme extends PureComponent {
  state = {};

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack();
    storeData(SETTING_APP, this.props.settingReducer.data);
    return true;
  };

  Switch = type => {
    if (type === SWITCH1) {
      this.props.settingTheme(1);
    }
    if (type === SWITCH2) {
      this.props.settingTheme(2);
    }
    if (type === SWITCH3) {
      this.props.settingTheme(3);
    }
    if (type === SWITCH4) {
      this.props.settingTheme(4);
    }
    if (type === SWITCH5) {
      this.props.settingTheme(5);
    }
  };

  bubbleColor = (color1, color2) => {
    return (
      <View style={styles.bubbleMain}>
        <View style={[styles.bubbleBack, { backgroundColor: color1 }]} />
        <View style={[styles.bubbleFront, { backgroundColor: color2 }]} />
      </View>
    );
  };

  boxColor = type => {
    if (type === SWITCH1) {
      return this.bubbleColor(ColorTheme.white, ColorTheme.Teal500);
    }
    if (type === SWITCH2) {
      return this.bubbleColor(ColorTheme.Teal500, ColorTheme.white);
    }
    if (type === SWITCH3) {
      return this.bubbleColor(ColorTheme.blue700, ColorTheme.white);
    }
    if (type === SWITCH4) {
      return this.bubbleColor(ColorTheme.black, ColorTheme.white);
    }
    if (type === SWITCH5) {
      return this.bubbleColor(ColorTheme.purple700, ColorTheme.white);
    }
    return null;
  };

  rederSelectTheme = (title, type) => {
    return (
      <TouchableOpacity style={styles.row} onPress={() => this.Switch(type)}>
        <View style={styles.colorText}>
          <Text style={styles.txt_colorText}>{title}</Text>
        </View>
        {this.boxColor(type)}
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Header
          title={LanguageVI.SettingTheme}
          rightHeader
          icon={
            <TouchableOpacity onPress={() => this.handleBackPress()}>
              <IconBackCustom />
            </TouchableOpacity>
          }
          elevation={true}
        />
        {this.rederSelectTheme('Purewhite', SWITCH1)}
        {this.rederSelectTheme('Magic green', SWITCH2)}
        {this.rederSelectTheme('Deep ocean', SWITCH3)}
        {this.rederSelectTheme('All black', SWITCH4)}
        {this.rederSelectTheme('Only purple', SWITCH5)}
      </View>
    );
  }
}

// export default SettingTheme;

const mapStateToProps = state => ({
  settingReducer: state.settingReducer,
});

const mapDispatchToProps = dispatch => ({
  settingTheme: data => dispatch(settingTheme(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SettingTheme);
