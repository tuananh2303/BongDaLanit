import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  row: {
    paddingLeft: 30 * d.ratioW,
    flexDirection: 'row',
    // height: 50 * d.ratioH,
    // borderBottomColor: ColorTheme.gray500,
    // borderBottomWidth: 0.25,
    backgroundColor: 'white',
    // borderTopColor: ColorTheme.gray500,
    // borderTopWidth: 0.25,
  },
  checkbox: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  colorText: {
    flex: 4,
    justifyContent: 'center',
  },
  color: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txt_colorText: {
    color: ColorTheme.black,
    fontSize: FontSize.medium + 2,
  },
  boxColor: {
    height: 25 * d.ratioH,
    width: 60 * d.ratioW,
    flexDirection: 'row',
    borderColor: ColorTheme.Teal900,
    borderWidth: 0.5,
  },
  boxColorLeft: {
    flex: 1,
    backgroundColor: ColorTheme.white2,
  },
  boxColorRight: {
    flex: 1,
    backgroundColor: ColorTheme.green,
  },
  boxColorLeft1: {
    flex: 1,
    backgroundColor: ColorTheme.black,
  },
  boxColorRight1: {
    flex: 1,
    backgroundColor: ColorTheme.red500,
  },
  boxColorLeft2: {
    flex: 1,
    backgroundColor: ColorTheme.orange400,
  },
  boxColorRight2: {
    flex: 1,
    backgroundColor: ColorTheme.lightGreen400a,
  },

  bubbleMain: {
    flex: 1,
    // backgroundColor: 'red',
    padding: 10 * d.ratioH,
  },
  bubbleBack: {
    height: 50 * d.ratioH,
    width: 50 * d.ratioH,
    borderRadius: 30 * d.ratioH,
    elevation: 2,
  },
  bubbleFront: {
    height: 20 * d.ratioH,
    width: 20 * d.ratioH,
    borderRadius: 30 * d.ratioH,
    position: 'absolute',
    elevation: 3,
    bottom: 5,
    right: 15,
  },
});
