import React, { PureComponent } from 'react';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';

import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Dimensions,
} from 'react-native';
import { connect } from 'react-redux';

import Icon from 'react-native-vector-icons/Ionicons';
import Header from '../../components/Header';
import LanguageVI from '../../constants/Language';
import NewsTab from '../newsTab';
import { URL_NEWS } from '../../constants/URL';
import styles from './styles';
import { changeNewsTab } from '../../redux/actions/getNewsConfigTabActions';
import { ColorTheme } from '../../themes';

class News extends PureComponent {
  state = {
    /* eslint-disable */
    index: 0,
    // routes: this.props.dataNewsConfigTab.data,
    routes: [
      {
        key: 'tab1',
        label: 'PremierLeague',
        category: 65,
        index: 0,
      },
      {
        key: 'tab2',
        label: 'VLeague',
        category: 65,
        index: 1,
      },
      {
        key: 'tab3',
        label: 'Laliga',
        category: 65,
        index: 2,
      },
      {
        key: 'tab4',
        label: 'SerieA',
        category: 65,
        index: 3,
      },
      {
        key: 'tab5',
        label: 'Bundesliga',
        category: 65,
        index: 4,
      },
      {
        key: 'tab6',
        label: 'Ligue1',
        category: 65,
        index: 5,
      },
      /* eslint-enable */
    ],
  };

  indicatorStyle = () => {
    if (this.props.settingReducer.data.theme == 1) {
      return styles.indicator;
    }
    return styles.indicator2;
  };

  _renderTabBarGetStyle = () => {
    if (this.props.settingReducer.data.theme == 2) {
      return styles.tabbar2;
    }
    if (this.props.settingReducer.data.theme == 3) {
      return styles.tabbar3;
    }
    if (this.props.settingReducer.data.theme == 4) {
      return styles.tabbar4;
    }
    if (this.props.settingReducer.data.theme == 5) {
      return [styles.tabbar4, { backgroundColor: ColorTheme.purple700 }];
    }
    return styles.tabbar;
  };

  renderLabelGetStyle = () => {
    if (this.props.settingReducer.data.theme == 1) {
      return styles.labelStyle;
    }
    return styles.labelStyle2;
  };

  renderLabel = ({ route }) => (
    <View style={styles.viewTab}>
      <Text style={this.renderLabelGetStyle()}>{route.label}</Text>
    </View>
  );

  _renderTabBar = props => {
    return (
      <TabBar
        {...props}
        scrollEnabled
        indicatorStyle={this.indicatorStyle()}
        style={this._renderTabBarGetStyle()}
        tabStyle={styles.tab}
        renderLabel={this.renderLabel}
      />
    );
  };

  onIndexChange = index => {
    this.setState({ index });
    this.props.changeNewsTab(index);
  };

  getData = route => {
    return route.index == this.state.index;
  };

  render() {
    return (
      <View style={styles.viewMain}>
        <Header title={LanguageVI.News} />
        <TabView
          navigationState={this.state}
          renderTabBar={this._renderTabBar}
          renderScene={({ route }) => (
            <NewsTab
              url={URL_NEWS}
              category={route.category}
              isLoadData={this.getData(route)}
              routeKey={route.index}
              navigation={this.props.navigation}
            />
          )}
          onIndexChange={index => this.onIndexChange(index)}
          useNativeDriver
          initialLayout={{
            height: 0,
            width: Dimensions.get('window').width,
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  dataNewsConfigTab: state.getNewsConfigTabReducers,
  settingReducer: state.settingReducer,
});

const mapDispatchToProps = dispatch => ({
  changeNewsTab: index => dispatch(changeNewsTab(index)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(News);
