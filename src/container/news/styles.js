import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  viewMain: {
    flex: 1,
    backgroundColor: ColorTheme.white,
  },
  tabbar: {
    backgroundColor: 'white',
  },
  tabbar2: {
    backgroundColor: ColorTheme.Teal900,
  },
  tabbar3: {
    backgroundColor: ColorTheme.blue800,
  },
  tab: {
    height: 30 * d.ratioH,
    width: d.windowSize.width / 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  indicator: {
    backgroundColor: ColorTheme.Teal900,
  },
  indicator2: {
    backgroundColor: ColorTheme.white,
  },
  labelStyle: {
    fontSize: 14,
    fontWeight: 'normal',
    color: ColorTheme.Teal900,
  },
  labelStyle2: {
    fontSize: 14,
    fontWeight: 'normal',
    color: ColorTheme.white,
  },
  viewTab: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabbar4: {
    backgroundColor: ColorTheme.black,
  },
});
