import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Image, FlatList } from 'react-native';
// import firebase from 'react-native-firebase';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Images, ColorTheme } from '../../themes';
import LanguageVI from '../../constants/Language';
import Header from '../../components/Header';
import FirstSettingScreen from '../firstSettingScreen';

// const database = firebase.database();

const TEAM = 'TEAM';
const LEAGUE = 'LEAGUE';

class FirstSetting extends PureComponent {
  state = {
    curentTab: 1,
    tabData: [
      {
        index: 1,
        label: LanguageVI.PopularTeam,
      },
      {
        index: 2,
        label: LanguageVI.PopularLeague,
      },
      {
        index: 3,
        label: LanguageVI.LocalLeague,
      },
      {
        index: 4,
        label: LanguageVI.LocalTeam,
      },
    ],
    dataEachTab: {
      data: [],
      dataSuccess: false,
    },
  };

  componentDidMount() {
    // database.ref('config/first_setting').on('value', snapShot => {
    //   console.log(snapShot.val());
    //   if (snapShot.val() != null) {
    //     this.setState({
    //       dataEachTab: {
    //         data: snapShot.val(),
    //         dataSuccess: true,
    //       },
    //     });
    //   }
    // });
  }

  renderNumberTab = () => {
    return (
      <View style={styles.ViewNumTab}>
        {this.state.tabData.map((item, index) => (
          <View style={this.getStyles(item.index)} key={index.toString()}>
            <Text style={this.getStylesText(item.index)}>{item.index}</Text>
          </View>
        ))}
      </View>
    );
  };

  getStyles = numTab => {
    if (numTab == this.state.curentTab) {
      return [styles.tab, styles.activeTab];
    }
    if (numTab < this.state.curentTab) {
      return [styles.tab, styles.backTab];
    }
    return styles.tab;
  };

  getStylesText = numTab => {
    if (numTab <= this.state.curentTab) {
      return styles.TextActive;
    }
    return styles.TextNotActive;
  };

  renderList = data => (
    <View style={styles.ListItemView}>
      <View style={styles.ListItemLeft}>
        <Image source={{ uri: data.image }} style={styles.ListItemImage} />
        <Text style={styles.ListItemText}>{data.name}</Text>
      </View>
      <TouchableOpacity style={styles.ListItemRight}>
        <Icon
          name={data.favorite ? 'md-checkbox' : 'md-square-outline'}
          size={22}
          color={ColorTheme.Teal900}
        />
      </TouchableOpacity>
    </View>
  );

  changeDataTab = numTab => {
    if (numTab === 1) {
      return this.state.dataEachTab.data.popular_team;
    }
    if (numTab === 2) {
      return this.state.dataEachTab.data.popular_league;
    }
    if (numTab === 3) {
      return this.state.dataEachTab.data.location_league;
    }
    if (numTab === 4) {
      return this.state.dataEachTab.data.location_team;
    }
  };

  changeTypeTab = numTab => {
    if (numTab === 1 || numTab === 4) {
      return TEAM;
    }
    if (numTab === 2 || numTab === 3) {
      return LEAGUE;
    }
  };

  renderContent = numTab => {
    if (this.state.dataEachTab.dataSuccess) {
      return (
        <FirstSettingScreen
          data={this.changeDataTab(numTab)}
          type={this.changeTypeTab(numTab)}
        />
      );
    }
    return <View />;
  };

  next = () => {
    if (this.state.curentTab < 4) {
      this.setState({
        curentTab: this.state.curentTab + 1,
      });
    }
    if (this.state.curentTab === 4) {
      this.props.navigation.navigate('AppStack');
    }
  };

  back = () => {
    if (this.state.curentTab > 1) {
      this.setState({
        curentTab: this.state.curentTab - 1,
      });
    }
  };

  render() {
    return (
      <View style={styles.viewMain}>
        <Header
          title={this.state.tabData[this.state.curentTab - 1].label}
          rightHeader={
            <TouchableOpacity>
              <Icon
                name="md-checkbox-outline"
                size={22}
                color={ColorTheme.Teal900}
              />
            </TouchableOpacity>
          }
          elevation={true}
        />
        <View style={styles.ViewContent}>
          {this.renderContent(this.state.curentTab)}
        </View>
        <View style={styles.Footer}>
          {this.renderNumberTab()}

          <View style={styles.ViewNext}>
            {/* <TouchableOpacity
              style={styles.NextBtn}
              onPress={() => this.back()}
            >
              <Text style={styles.NextText}>{LanguageVI.Back}</Text>
            </TouchableOpacity> */}

            <TouchableOpacity
              style={styles.NextBtn}
              onPress={() => this.next()}
            >
              <Text style={styles.NextText}>{LanguageVI.Next}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default FirstSetting;
