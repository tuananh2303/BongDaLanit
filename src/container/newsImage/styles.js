import { StyleSheet } from 'react-native';
import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  ViewMain: {
    backgroundColor: 'black',
    flex: 1,
  },
  ViewSwiper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: 'red',
  },
  Image: {
    height: 400,
    width: d.windowSize.width,
    marginBottom: 50 * d.ratioH,
  },
  Caption: {
    fontSize: FontSize.medium,
    color: ColorTheme.white,
    textAlign: 'center',
    marginBottom: 10 * d.ratioH,
  },
  ViewClose: {
    marginTop: 20 * d.ratioH,
    marginLeft: 20 * d.ratioW,
  },
  Icon: {
    fontSize: FontSize.large + 10,
    color: ColorTheme.white,
  },

  ViewCaption: {
    position: 'absolute',
    bottom: 40,
  },
});
