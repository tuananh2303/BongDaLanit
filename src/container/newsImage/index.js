import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, Dimensions } from 'react-native';
import Swiper from 'react-native-swiper';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

class ImageGallery extends Component {
  state = {
    data: this.props.navigation.getParam('data'),
    item: this.props.navigation.getParam('initialImage'),
  };

  componentDidMount() {}

  initialImage = () => {
    const indexImage = this.state.data.findIndex(
      item => item == this.state.item,
    );
    return indexImage;
  };

  render() {
    return (
      <View style={styles.ViewMain}>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.ViewClose}
          onPress={() => this.props.navigation.goBack()}
        >
          <Icon name="md-close" style={styles.Icon} />
        </TouchableOpacity>
        <Swiper
          showsButtons={false}
          showsPagination={true}
          dotColor="rgb(224,224,224)"
          index={this.initialImage()}
        >
          {this.state.data.map((Item, index) => (
            <View style={styles.ViewSwiper} key={index.toString()}>
              {Item.data.caption == 'null' ? null : (
                <View style={styles.ViewCaption}>
                  <Text style={styles.Caption}>{Item.data.caption}</Text>
                </View>
              )}
              <Image
                resizeMode="contain"
                style={styles.Image}
                source={{
                  uri: Item.data.src,
                }}
              />
            </View>
          ))}
        </Swiper>
      </View>
    );
  }
}

export default ImageGallery;
