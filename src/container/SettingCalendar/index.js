import React, { PureComponent } from 'react';
import styles from './styles';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Switch from '../../components/Switch';
import LanguageVI from '../../constants/Language';
import Header from '../../components/Header';

import { ColorTheme } from '../../themes';
import IconBackCustom from '../../components/IconBackCustom';

class CalendarSettings extends PureComponent {
  state = { SwitchOnValueHolder: false };

  toggleSwitch = () => {
    this.setState({
      SwitchOnValueHolder: !this.state.SwitchOnValueHolder,
    });
  };

  rowToggleSwitch = (icon, title, stateValue) => {
    return (
      <TouchableOpacity
        style={styles.rowsub}
        onPress={() => this.toggleSwitch()}
      >
        {/* {rowIcon(icon, title, stateValue)} */}
        <Switch
          icon={icon}
          title={title}
          value={stateValue}
          toggleSwitch1={() => this.toggleSwitch()}
        />
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Header
          title={LanguageVI.CalendarSetting}
          rightHeader
          icon={
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <IconBackCustom />
              {/* <Icon name="md-arrow-back" size={22} color={ColorTheme.Teal900} /> */}
            </TouchableOpacity>
          }
          // elevation={true}
        />

        {this.rowToggleSwitch(
          'ios-star',
          LanguageVI.ShowFollowedTeam,
          this.state.SwitchOnValueHolder,
        )}
      </View>
    );
  }
}

export default CalendarSettings;
