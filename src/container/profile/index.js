import React, { PureComponent } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  FlatList,
  ActivityIndicator,
  Alert,
  ToastAndroid,
} from 'react-native';
import { connect } from 'react-redux';
import Axios from 'axios';
import LottieView from 'lottie-react-native';

import styles from './styles';
import { Images, ColorTheme } from '../../themes';
import Header from '../../components/Header';
import LanguageVI from '../../constants/Language';
import * as d from '../../utilities/Tranform';
import {
  URL_LOGIN,
  URL_IMAGE,
  URL_PREDICT_HISTORY,
  URL_USER_BAN,
  URL_USER_UNBAN,
} from '../../constants/URL';
import GameHistoryRow from '../../components/GameHistoryRow';
import FastImage from '../../components/FastImage';
import IconBackCustom from '../../components/IconBackCustom';
import Lottie from '../../themes/Lottie';

class Profile extends PureComponent {
  state = {
    status: false,
    email: this.props.userReducer.dataFull.user.email,
    std: this.props.userReducer.dataFull.user.phone,
    idUser: this.props.navigation.getParam('id'),
    data: null,
    dataSuccess: false,
    isFetching: false,
    error: false,
  };

  componentDidMount() {
    this.setState({
      isFetching: true,
    });
    Axios.get(URL_PREDICT_HISTORY + this.state.idUser)
      .then(response => {
        if (response.data.status == 'success') {
          console.log(response.data);

          this.setState({
            isFetching: false,
            data: response.data,
            dataSuccess: true,
          });
        }
        if (response.data.status == 'error') {
          console.log(response.data);
          this.setState({
            isFetching: false,
            error: true,
            dataSuccess: false,
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  rederInfo = (tittle, value) => {
    return (
      <View style={styles.correct}>
        <View style={styles.tittle}>
          <Text>{tittle}</Text>
        </View>
        <View style={styles.value}>
          <Text>{value}</Text>
        </View>
      </View>
    );
  };

  rederFixInfo = type => {
    if (type == true) {
      return (
        <View style={styles.fixInfo}>
          <View style={styles.infoEmail}>
            <View style={styles.infoEmailLeft}>
              <Text>{LanguageVI.Email}</Text>
            </View>
            <View style={styles.infoEmailRight}>
              <TextInput
                style={styles.input}
                underlineColorAndroid="transparent"
                value={this.state.email}
                onChangeText={email => {
                  this.setState({ email });
                }}
              />
            </View>
          </View>
          <View style={styles.infoEmail}>
            <View style={styles.infoEmailLeft}>
              <Text>{LanguageVI.Phone}</Text>
            </View>
            <View style={styles.infoEmailRight}>
              <TextInput
                style={styles.input}
                underlineColorAndroid="transparent"
                value={this.state.std}
                onChangeText={std => {
                  this.setState({ std });
                }}
              />
            </View>
          </View>
          <View style={styles.btn_xacnhan}>
            <TouchableOpacity
              style={styles.xacnhan}
              onPress={() => {
                this.hidePostData(this.props.userReducer);
              }}
            >
              <Text>{LanguageVI.Confirm}</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
    return (
      <View>
        {this.rederInfo(LanguageVI.Email, this.state.email)}
        {this.rederInfo(LanguageVI.Phone, this.state.std)}
      </View>
    );
  };

  hidePostData = data => {
    this.setState({
      status: false,
    });
    Axios.post(URL_LOGIN + 'update?token=' + data.data.token, {
      user_id: data.dataFull.user.id,
      phone: this.state.std,
      email: this.state.email,
    })
      .then(response => {
        console.log(response.data);
      })
      .catch(error => console.log(error));
  };

  getColor = () => {
    if (this.props.settingReducer.data.theme == 2) {
      return ColorTheme.Teal900;
    }
    if (this.props.settingReducer.data.theme == 3) {
      return ColorTheme.blue800;
    }
    if (this.props.settingReducer.data.theme == 4) {
      return ColorTheme.black;
    }
    return ColorTheme.Teal900;
  };

  banAndUnban = url => {
    Axios.get(
      url +
        this.props.userReducer.dataFull.user.id +
        '/' +
        this.state.idUser +
        '?token=' +
        this.props.userReducer.dataFull.user.token,
    )
      .then(response => {
        console.log(response.data);
        if (response.data.status == 'success') {
          ToastAndroid.show(response.data.msg, ToastAndroid.SHORT);
        }
        if (response.data.status == 'error') {
          ToastAndroid.show(response.data.msg, ToastAndroid.SHORT);
        }
      })
      .catch(error => console.log(error));
  };

  banUser = () => {
    Alert.alert(
      LanguageVI.BanUser,
      LanguageVI.BanUserConfirm,
      [
        {
          text: LanguageVI.Cancel,
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            this.banAndUnban(URL_USER_BAN);
          },
        },
      ],
      { cancelable: false },
    );
  };

  unbanUser = () => {
    Alert.alert(
      LanguageVI.UnbanUser,
      LanguageVI.UnbanUserConfirm,
      [
        {
          text: LanguageVI.Cancel,
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            this.banAndUnban(URL_USER_UNBAN);
          },
        },
      ],
      { cancelable: false },
    );
  };

  getColorIcon = () => {
    if (this.props.settingReducer.data.theme == 1) {
      return ColorTheme.Teal900;
    }
    return ColorTheme.white;
  };

  renderRightHeader = () => {
    if (this.state.idUser == this.props.userReducer.dataFull.user.id) {
      return (
        <TouchableOpacity style={styles.iconGift}>
          <Icon name="ios-gift" size={22} color={this.getColorIcon()} />
        </TouchableOpacity>
      );
    }
    if (this.props.userReducer.dataFull.class == 'admin') {
      if (this.state.data.user.class == 'banned') {
        return (
          <TouchableOpacity
            style={styles.iconGift}
            onPress={() => this.unbanUser()}
          >
            <Icon name="md-checkmark" size={22} color={this.getColorIcon()} />
          </TouchableOpacity>
        );
      }
      return (
        <TouchableOpacity
          style={styles.iconGift}
          onPress={() => this.banUser()}
        >
          <Icon name="md-close" size={22} color={this.getColorIcon()} />
        </TouchableOpacity>
      );
    }
    return null;
  };

  header = () => {
    return (
      <Header
        elevation={true}
        title={LanguageVI.Profile}
        icon={
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <IconBackCustom />
          </TouchableOpacity>
        }
        rightHeader={this.renderRightHeader()}
      />
    );
  };

  render() {
    if (this.state.error) {
      return (
        <View style={{ flex: 1 }}>
          <Header
            elevation={true}
            title={LanguageVI.Profile}
            icon={
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <IconBackCustom />
              </TouchableOpacity>
            }
          />
          <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <LottieView
              source={Lottie.error}
              autoPlay
              loop
              style={{ height: 250, width: 250 }}
            />
            <Text>Có lỗi xảy ra</Text>
          </View>
        </View>
      );
    }
    if (this.state.dataSuccess) {
      return (
        <View style={{ flex: 1 }}>
          {this.header()}
          <ScrollView style={styles.container}>
            <View style={styles.header}>
              <FastImage
                linkImageDefault={Images.defaultImage}
                linkImage={this.state.data.user.avatar}
                resizeMode="contain"
                style={[styles.avatar, { borderColor: this.getColor() }]}
              />
              <Image
                source={Images.rankIcon10}
                style={styles.rankIcon}
                resizeMode="contain"
              />
              <View style={styles.viewName}>
                <View style={styles.userName}>
                  <Text style={styles.txt_userName}>
                    {this.state.data.user.fullname}
                  </Text>
                </View>
                <View style={styles.points}>
                  <Text style={styles.txt_points}>
                    {this.state.data.user.point}
                    {' point'}
                  </Text>
                </View>
                {/* <TouchableOpacity
                style={[styles.followBtn, { backgroundColor: this.getColor() }]}
              >
                <Text style={styles.followText}>Theo dõi</Text>
              </TouchableOpacity> */}
              </View>
            </View>

            {this.state.idUser == this.props.userReducer.dataFull.user.id ? (
              <View style={styles.contentInfo}>
                <View style={styles.infoUser}>
                  <View style={styles.infoUserTitle}>
                    <View style={styles.infoUserLeft}>
                      <Text style={styles.txt_info}>
                        {LanguageVI.InfoProfile}
                      </Text>
                    </View>
                    <TouchableOpacity
                      style={styles.infoUserRight}
                      onPress={() =>
                        this.setState({
                          status: true,
                        })
                      }
                    >
                      <Icon
                        name="md-create"
                        size={22}
                        color={ColorTheme.gray}
                      />
                    </TouchableOpacity>
                  </View>
                  {this.rederFixInfo(this.state.status)}
                </View>
              </View>
            ) : (
              <FlatList
                showsVerticalScrollIndicator={false}
                data={this.state.data.data}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => {
                  if (item != null) {
                    return (
                      <View style={styles.row}>
                        <GameHistoryRow data={item} />
                      </View>
                    );
                  }
                }}
              />
            )}
          </ScrollView>
        </View>
      );
    }
    if (this.state.isFetching) {
      return (
        <View>
          <Header
            elevation={true}
            title={LanguageVI.Profile}
            icon={
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <IconBackCustom />
              </TouchableOpacity>
            }
          />
          <ActivityIndicator size="large" color={ColorTheme.Teal900} />
        </View>
      );
    }
    return null;
  }
}

const mapStateToProps = state => ({
  userReducer: state.userReducer,
  settingReducer: state.settingReducer,
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Profile);
