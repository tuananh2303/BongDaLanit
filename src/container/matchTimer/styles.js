import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  ViewMain: {
    backgroundColor: ColorTheme.white2,
    // backgroundColor: 'red',
    flex: 1,
    paddingTop: 10 * d.ratioH,
  },
  viewPart: {
    padding: 15 * d.ratioH,

    borderBottomWidth: 1,
    borderBottomColor: ColorTheme.white2,
    backgroundColor: 'white',
  },

  titleText: {
    color: ColorTheme.black,
    marginLeft: 10 * d.ratioW,
    marginBottom: 10 * d.ratioH,
    fontSize: FontSize.medium + 3,
  },
  viewForm: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginLeft: 20 * d.ratioW,
  },
  teamForm: {
    color: ColorTheme.gray,
    marginLeft: 10 * d.ratioW,
    marginBottom: 10 * d.ratioH,
    fontSize: FontSize.medium,
  },

  metaView: {
    marginHorizontal: 20 * d.ratioW,
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 7 * d.ratioH,
  },
  metaItem: {
    width: 30 * d.ratioW,
  },
  metaText: {
    marginLeft: 20 * d.ratioW,
  },
  metaTextTop: {
    color: ColorTheme.black,
    fontSize: FontSize.medium,
  },
  metaTextBottom: {},
});
