import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';

import TableLongPart from '../../components/TableLongPart';
import styles from './styles';
import LanguageVI from '../../constants/Language';
import FixtureRow from '../../components/FixturesRow';
import HeaderBubbleWinLose from '../../components/HeaderBubbleWinLose';
import Match1Row from '../../components/Match1Row';

class MatchTimer extends PureComponent {
  state = {};

  renderFixture = (dataInput, title) => {
    return (
      <View style={styles.viewPart}>
        <Text style={styles.titleText}>{title}</Text>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={dataInput}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => (
            <FixtureRow
              onPressMatch={data => this.props.onPressMatch(data)}
              data={item}
              teams={this.props.data.teams}
            />
          )}
        />
      </View>
    );
  };

  renderForm = title => {
    console.log(this.props.data);

    return (
      <View style={styles.viewPart}>
        <Text style={styles.titleText}>{title}</Text>
        <Text style={styles.teamForm}>{this.props.data.teams[1]}</Text>
        <View style={styles.viewForm}>
          {this.props.data.preview.homeRecentMatches.map((item, index) => (
            <HeaderBubbleWinLose type={item} key={index.toString()} />
          ))}
        </View>
        <Text style={styles.teamForm}>{this.props.data.teams[4]}</Text>
        <View style={styles.viewForm}>
          {this.props.data.preview.awayRecentMatches.map((item, index) => (
            <HeaderBubbleWinLose type={item} key={index.toString()} />
          ))}
        </View>
      </View>
    );
  };

  metaItem = (icon, textTop, textBottom) => {
    return (
      <View style={styles.metaView}>
        <View style={styles.metaItem}>
          <Icon name={icon} size={22} />
        </View>
        <View style={styles.metaText}>
          <Text style={styles.metaTextTop}>{textTop}</Text>
          <Text style={styles.metaTextBottom}>{textBottom}</Text>
        </View>
      </View>
    );
  };

  renderMeta = title => {
    return (
      <View style={styles.viewPart}>
        <Text style={styles.titleText}>{title}</Text>
        {this.metaItem(
          'md-locate',
          LanguageVI.Stadium,
          this.props.data.meta[0],
        )}
        {this.metaItem('md-sunny', LanguageVI.Weather, this.props.data.meta[1])}
        {this.metaItem(
          'md-thermometer',
          LanguageVI.Temperature,
          this.props.data.meta[2],
        )}
        {this.metaItem(
          'md-apps',
          LanguageVI.TeamHomeLineup,
          this.props.data.meta[3],
        )}
        {this.metaItem(
          'md-apps',
          LanguageVI.TeamAwayLineup,
          this.props.data.meta[4],
        )}
      </View>
    );
  };

  render() {
    if (this.props.data == null) {
      return null;
    }
    return (
      <View style={styles.ViewMain}>
        {this.props.settingReducer.data.hideShowEvent ? (
          <TableLongPart
            content={
              <FlatList
                showsVerticalScrollIndicator={false}
                data={this.props.data.events.reverse()}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => <Match1Row data={item} />}
              />
            }
          />
        ) : null}

        {/* <ProportionWin /> */}
        <TableLongPart
          content={<View>{this.renderMeta(LanguageVI.matchMeta)}</View>}
        />
        <TableLongPart
          content={
            <View>
              {this.renderForm(LanguageVI.Form)}
              {this.renderFixture(
                this.props.data.preview.headtohead[1],
                LanguageVI.PreviousMeetings,
              )}
            </View>
          }
        />
      </View>
    );
  }
}

// export default MatchTimer;

const mapStateToProps = state => ({
  settingReducer: state.settingReducer,
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MatchTimer);
