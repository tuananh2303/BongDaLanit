import React, { PureComponent } from 'react';
import styles from './styles';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Switch from '../../components/Switch';
import LanguageVI from '../../constants/Language';
import Header from '../../components/Header';
import { ColorTheme } from '../../themes';
import IconBackCustom from '../../components/IconBackCustom';

const TOGGLESWITCH = 'toggleSwitch';
const TOGGLESWITCH1 = 'toggleSwitch1';
const TOGGLESWITCH2 = 'toggleSwitch2';
const TOGGLESWITCH3 = 'toggleSwitch3';
const TOGGLESWITCH4 = 'toggleSwitch4';
const TOGGLESWITCH5 = 'toggleSwitch5';
const TOGGLESWITCH6 = 'toggleSwitch6';
const TOGGLESWITCH7 = 'toggleSwitch7';
const TOGGLESWITCH8 = 'toggleSwitch8';
const TOGGLESWITCH9 = 'toggleSwitch9';

class SettingPushNotificationEditDeffault extends PureComponent {
  state = {
    // SwitchOnValueHolder_TYPE : 0
    SwitchOnValueHolder: false,
    SwitchOnValueHolder1: false,
    SwitchOnValueHolder2: false,
    SwitchOnValueHolder3: false,
    SwitchOnValueHolder4: false,
    SwitchOnValueHolder5: false,
    SwitchOnValueHolder6: false,
    SwitchOnValueHolder7: false,
    SwitchOnValueHolder8: false,
    SwitchOnValueHolder9: false,
  };

  // xet toggleSwitch.....................................

  toggleSwitch = type => {
    if (type === TOGGLESWITCH) {
      return this.setState({
        SwitchOnValueHolder: !this.state.SwitchOnValueHolder,
      });
    }

    if (type === TOGGLESWITCH1) {
      return this.setState({
        SwitchOnValueHolder1: !this.state.SwitchOnValueHolder1,
      });
    }

    if (type === TOGGLESWITCH2) {
      return this.setState({
        SwitchOnValueHolder2: !this.state.SwitchOnValueHolder2,
      });
    }

    if (type === TOGGLESWITCH3) {
      return this.setState({
        SwitchOnValueHolder3: !this.state.SwitchOnValueHolder3,
      });
    }

    if (type === TOGGLESWITCH4) {
      return this.setState({
        SwitchOnValueHolder4: !this.state.SwitchOnValueHolder4,
      });
    }

    if (type === TOGGLESWITCH5) {
      return this.setState({
        SwitchOnValueHolder5: !this.state.SwitchOnValueHolder5,
      });
    }

    if (type === TOGGLESWITCH6) {
      return this.setState({
        SwitchOnValueHolder6: !this.state.SwitchOnValueHolder6,
      });
    }

    if (type === TOGGLESWITCH7) {
      return this.setState({
        SwitchOnValueHolder7: !this.state.SwitchOnValueHolder7,
      });
    }

    if (type === TOGGLESWITCH8) {
      return this.setState({
        SwitchOnValueHolder8: !this.state.SwitchOnValueHolder8,
      });
    }

    if (type === TOGGLESWITCH9) {
      return this.setState({
        SwitchOnValueHolder9: !this.state.SwitchOnValueHolder9,
      });
    }
  };

  rowToggleSwitch = (toggleSwitch, icon, title, stateValue) => {
    return (
      <TouchableOpacity
        style={styles.rowsub}
        onPress={() => this.toggleSwitch(toggleSwitch)}
      >
        {/* {rowIcon(icon, title, stateValue)} */}
        <Switch
          icon={icon}
          title={title}
          value={stateValue}
          toggleSwitch1={() => this.toggleSwitch(toggleSwitch)}
        />
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Header
          title={LanguageVI.DefaultNotifications}
          rightHeader
          icon={
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <IconBackCustom />
              {/* <Icon name="md-arrow-back" size={22} color={ColorTheme.Teal900} /> */}
            </TouchableOpacity>
          }
          // elevation={true}
        />
        <ScrollView>
          <View style={styles.note}>
            <View style={styles.icon}>
              <Icon name="md-information-circle" size={23} />
            </View>
            <View style={styles.text}>
              <Text style={styles.txt_title}>
                {LanguageVI.NoteDefaultNotification}
              </Text>
              <Text style={styles.txt_subTitle}>
                {LanguageVI.NoteDefaultNotificationSub}
              </Text>
            </View>
          </View>
          <View style={styles.row}>
            {this.rowToggleSwitch(
              TOGGLESWITCH,
              'md-time',
              LanguageVI.MatchReminder,
              this.state.SwitchOnValueHolder,
            )}

            {this.rowToggleSwitch(
              TOGGLESWITCH1,
              'ios-star',
              LanguageVI.Lineup,
              this.state.SwitchOnValueHolder1,
            )}

            {this.rowToggleSwitch(
              TOGGLESWITCH2,
              'ios-star',
              LanguageVI.MatchStart,
              this.state.SwitchOnValueHolder2,
            )}

            {this.rowToggleSwitch(
              TOGGLESWITCH3,
              'ios-star',
              LanguageVI.Goals,
              this.state.SwitchOnValueHolder3,
            )}

            {this.rowToggleSwitch(
              TOGGLESWITCH4,
              'ios-star',
              LanguageVI.VideoHighlights,
              this.state.SwitchOnValueHolder4,
            )}

            {this.rowToggleSwitch(
              TOGGLESWITCH5,
              'ios-star',
              LanguageVI.RedCards,
              this.state.SwitchOnValueHolder5,
            )}

            {this.rowToggleSwitch(
              TOGGLESWITCH6,
              'ios-star',
              LanguageVI.HalfTimeResult,
              this.state.SwitchOnValueHolder6,
            )}

            {this.rowToggleSwitch(
              TOGGLESWITCH7,
              'ios-star',
              LanguageVI.FullTimeResult,
              this.state.SwitchOnValueHolder7,
            )}

            {this.rowToggleSwitch(
              TOGGLESWITCH8,
              'ios-star',
              LanguageVI.TransferNew,
              this.state.SwitchOnValueHolder8,
            )}

            {this.rowToggleSwitch(
              TOGGLESWITCH9,
              'ios-star',
              LanguageVI.Questions,
              this.state.SwitchOnValueHolder9,
            )}
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default SettingPushNotificationEditDeffault;
