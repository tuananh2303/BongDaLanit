import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    // height: 60 * d.ratioH,
    // elevation: 3,
    backgroundColor: 'white',
    borderTopColor: ColorTheme.gray500,
    borderTopWidth: 0.25,
  },
  rowsub: {
    height: 50 * d.ratioH,
    justifyContent: 'center',
  },
  note: {
    height: 100 * d.ratioH,
    backgroundColor: 'white',
    elevation: 3,
    margin: 10,
    flexDirection: 'row',
  },
  icon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  txt_title: {
    color: ColorTheme.black,
    fontWeight: '500',
    fontSize: FontSize.medium,
  },
  txt_subTitle: {
    fontSize: FontSize.medium -2,
  },
});
