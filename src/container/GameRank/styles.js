import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  ViewMain: {
    flex: 1,
    backgroundColor: 'white',
  },
  rowRank: {
    height: 70 * d.ratioH,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  rank: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  info: {
    flex: 5,
    flexDirection: 'column',
    paddingLeft: 20 * d.ratioW,
  },
  imgAvatar: {
    height: 50 * d.ratioH,
    width: 50 * d.ratioW,
    borderRadius: 25,
  },
  name: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  class: {
    flex: 1,
    justifyContent: 'center',
  },
  pointGain: {
    flex: 1,
    justifyContent: 'center',
  },
  imgClass: {
    height: 20 * d.ratioH,
    width: 40 * d.ratioW,
    borderRadius: 5,
  },
  nameText: {
    flex: 3,
    justifyContent: 'center',
  },
  nameIconClass: {
    flex: 1,
    justifyContent: 'center',
  },
  txtFullName: {
    fontSize: FontSize.medium + 2,
    color: ColorTheme.black,
  },
  txtClass: {
    fontSize: FontSize.medium,
    color: ColorTheme.black,
  },
  txtPointGain: {
    fontSize: FontSize.medium,
    color: ColorTheme.black,
  },
});
