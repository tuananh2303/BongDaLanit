import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import Axios from 'axios';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Images, ColorTheme } from '../../themes';
import LanguageVI from '../../constants/Language';
import { URL_RANK_THANHDU } from '../../constants/URL';
import GameRankRow from '../../components/GameRankRow';

class GameRank extends PureComponent {
  state = {
    isFetching: false,
    error: false,
    data: [],
    dataSuccess: false,
  };

  componentDidMount() {
    this.getDataGameRank();
  }

  getDataGameRank = () => {
    this.setState({ isFetching: true });
    Axios.get(URL_RANK_THANHDU)
      .then(response => {
        this.setState({
          data: response.data,
          dataSuccess: true,
          isFetching: false,
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({
          error: true,
        });
      });
  };

  render() {
    if (this.state.isFetching) {
      return (
        <View>
          <ActivityIndicator size="large" color={ColorTheme.Teal900} />
        </View>
      );
    }
    if (this.state.dataSuccess === true) {
      return (
        <View style={styles.ViewMain}>
          {/* {this.rederRankPlayer(dataGameRankFake.player)} */}
          <FlatList
            showsVerticalScrollIndicator={false}
            data={this.state.data.data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => {
              if (item != null) {
                return (
                  <GameRankRow
                    data={item}
                    onPress={() =>
                      this.props.navigation.navigate('Profile', { id: item.id })
                    }
                  />
                );
              }
            }}
          />
        </View>
      );
    }
    return null;
  }
}

export default GameRank;
