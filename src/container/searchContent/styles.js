import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  search: {
    height: 60 * d.ratioH,
    backgroundColor: 'white',
  },
  input: {
    backgroundColor: 'white',
    margin: 10,
    height: 40 * d.ratioH,
    flexDirection: 'row',
    borderRadius: 20,
    elevation: 3,
  },
  inputLeft: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputMid: {
    flex: 6,
    justifyContent: 'center',
  },
  textInput: {
    height: 40 * d.ratioH,
  },
  inputRight: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    height: 50 * d.ratioH,
    justifyContent: 'center',
    marginLeft: 15 * d.ratioW,
  },
  txt_title: {
    fontSize: FontSize.medium + 2,
    color: ColorTheme.black,
    fontWeight: '500',
  },
  row: {
    margin: 10,
    borderWidth: 0.5,
    borderColor: ColorTheme.gray200,
    backgroundColor: 'white',
    borderRadius: 15,
  },
});
