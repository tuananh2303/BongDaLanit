const dataSearchFake = [
  {
    id: 1,
    title: 'Player',
    status: 'row',
    data: [
      {
        id: 1,
        images: 'http://dummyimage.com/129x219.bmp/ff4444/ffffff',
        name: 'Emmye Sweedland',
        sub_name: 'Avrit Fishley',
      },
      {
        id: 2,
        images: 'http://dummyimage.com/235x143.png/dddddd/000000',
        name: 'Freddi Keward',
        sub_name: 'Peg Essery',
      },
      {
        id: 3,
        images: 'http://dummyimage.com/174x216.jpg/dddddd/000000',
        name: 'Rogers Copperwaite',
        sub_name: 'Hatty Harbor',
      },
      {
        id: 4,
        images: 'http://dummyimage.com/129x172.jpg/ff4444/ffffff',
        name: 'Daniel Gerrets',
        sub_name: 'Rafi De Marchi',
      },
      {
        id: 5,
        images: 'http://dummyimage.com/167x238.bmp/dddddd/000000',
        name: 'Arne Creevy',
        sub_name: 'Zorina Trineman',
      },
    ],
  },
  {
    id: 2,
    title: 'Team',
    status: 'sub_row',
    data: [
      {
        id: 1,
        images: 'http://dummyimage.com/129x219.bmp/ff4444/ffffff',
        name: 'Emmye Sweedland',
        sub_name: 'Avrit Fishley',
      },
      {
        id: 2,
        images: 'http://dummyimage.com/235x143.png/dddddd/000000',
        name: 'Freddi Keward',
        sub_name: 'Peg Essery',
      },
      {
        id: 3,
        images: 'http://dummyimage.com/174x216.jpg/dddddd/000000',
        name: 'Rogers Copperwaite',
        sub_name: 'Hatty Harbor',
      },
      {
        id: 4,
        images: 'http://dummyimage.com/129x172.jpg/ff4444/ffffff',
        name: 'Daniel Gerrets',
        sub_name: 'Rafi De Marchi',
      },
      {
        id: 5,
        images: 'http://dummyimage.com/167x238.bmp/dddddd/000000',
        name: 'Arne Creevy',
        sub_name: 'Zorina Trineman',
      },
    ],
  },
  {
    id: 3,
    title: 'Competitions',
    status: 'sub_row',
    data: [
      {
        id: 1,
        images: 'http://dummyimage.com/129x219.bmp/ff4444/ffffff',
        name: 'Emmye Sweedland',
        sub_name: 'Avrit Fishley',
      },
      {
        id: 2,
        images: 'http://dummyimage.com/235x143.png/dddddd/000000',
        name: 'Freddi Keward',
        sub_name: 'Peg Essery',
      },
      {
        id: 3,
        images: 'http://dummyimage.com/174x216.jpg/dddddd/000000',
        name: 'Rogers Copperwaite',
        sub_name: 'Hatty Harbor',
      },
      {
        id: 4,
        images: 'http://dummyimage.com/129x172.jpg/ff4444/ffffff',
        name: 'Daniel Gerrets',
        sub_name: 'Rafi De Marchi',
      },
      {
        id: 5,
        images: 'http://dummyimage.com/167x238.bmp/dddddd/000000',
        name: 'Arne Creevy',
        sub_name: 'Zorina Trineman',
      },
    ],
  },
];
export default dataSearchFake;
