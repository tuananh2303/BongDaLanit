import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  FlatList,
  ScrollView,
  Switch,
} from 'react-native';
import styles from './styles';
import Icon from 'react-native-vector-icons/Ionicons';
import dataSearchFake from './dataSearchFake';
import { ColorTheme } from '../../themes';
import InfoKickRow from '../../components/InfoKickRow';
import SearchRowTeam from '../../components/SearchRowTeam';

class searchContent extends PureComponent {
  state = {};

  renderRow = (type, data, title) => {
    if (type === 'row') {
      return (
        <View>
          <View style={styles.title}>
            <Text style={styles.txt_title}>{title}</Text>
          </View>
          {data.map(item => (
            <SearchRowTeam
              //   onPressPlayer={() => this.props.onPressPlayer()}
              title={item.name}
              //   sub_title={<Text>{item.sub_name}</Text>}
            />
          ))}
        </View>
      );
    }
    if (type === 'sub_row') {
      return (
        <View>
          <View style={styles.title}>
            <Text style={styles.txt_title}>{title}</Text>
          </View>
          {data.map(item => (
            <InfoKickRow
              //   onPressPlayer={() => this.props.onPressPlayer()}
              title={item.name}
              sub_title={<Text>{item.sub_name}</Text>}
            />
          ))}
        </View>
      );
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.search}>
          <View style={styles.input}>
            <TouchableOpacity
              style={styles.inputLeft}
              onPress={() => this.props.navigation.navigate('Search')}
            >
              <Icon
                name="ios-arrow-back"
                size={23}
                color={ColorTheme.Teal900}
              />
            </TouchableOpacity>
            <View style={styles.inputMid}>
              <TextInput
                style={styles.textInput}
                onSubmitEditing={this.submitEdit}
                value={this.props.navigation.getParam('value')}
                
              />
            </View>
            <TouchableOpacity
              style={styles.inputRight}
              onPress={() => this.setState({ value: '' })}
            >
              <Icon name="ios-close" size={25} color={ColorTheme.Teal900} />
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView>
          <FlatList
            data={dataSearchFake}
            renderItem={({ item }) => (
              <View style={styles.row}>
                {this.renderRow(item.status, item.data, item.title)}
              </View>
            )}
            keyExtractor={item => item.id}
            ItemSeparatorComponent={this.renderSeparator}
          />
        </ScrollView>
      </View>
    );
  }
}

export default searchContent;
