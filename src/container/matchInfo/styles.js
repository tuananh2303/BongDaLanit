import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    height: d.windowSize / 4,
    // flexDirection: 'column',
    // elevation: 3,
    marginTop: 5 * d.ratioH,
    backgroundColor: ColorTheme.white,
  },
});
