import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
// import DetailsFines from '../../components/DetailsFines';
import DetailsFinesNameTeam from '../../components/DetailsFinesNameTeam';
import DetailsFinesHoldBall from '../../components/DetailsFinesHoldBall';
import DetailsFinesRowColor from '../../components/DetailsFinesRowColor';
import Icon from 'react-native-vector-icons/Ionicons';
import DetailsFinesRow from '../../components/DetailsFinesRow';
import styles from './styles';
import LanguageVI from '../../constants/Language';
import { ColorTheme } from '../../themes';
import { statistics } from '../../constants/MatchStatisticsConst';
import EmptyComponent from '../../components/EmptyComponent';

class MatchInfo extends PureComponent {
  state = {
    color: Math.floor(Math.random() * 5 + 1),
  };

  componentDidMount() {
    // console.log(this.props.data.stats);
    // console.log(statistics);
  }

  getColor = random => {
    if (random === 1) {
      return ColorTheme.blueGrey200a;
    }
    if (random === 2) {
      return ColorTheme.deepOrange400a;
    }
    if (random === 3) {
      return ColorTheme.lightGreen400a;
    }
    if (random === 4) {
      return ColorTheme.lightBlue300a;
    }
    if (random === 5) {
      return ColorTheme.deepPurple400a;
    }
    return ColorTheme.lightGreen400a;
  };

  renderStatis = data => {
    if (data.length != 0) {
      return (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => (
            <DetailsFinesRowColor
              content={
                statistics.find(x => x.id === parseInt(item[0], 10)).label
              }
              team1={parseInt(item[1], 10)}
              team2={parseInt(item[2], 10)}
              color={this.getColor(this.state.color)}
            />
          )}
        />
      );
    }
    return <EmptyComponent />;
  };

  render() {
    return (
      <View style={styles.container}>
        <DetailsFinesNameTeam
          team1={this.props.data.teams[1]}
          team2={this.props.data.teams[4]}
        />
        {this.renderStatis(this.props.data.stats)}
      </View>
    );
  }
}

export default MatchInfo;
