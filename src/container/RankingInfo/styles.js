import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  row: {
    flexDirection: 'row',
    height: 50 * d.ratioH,
    backgroundColor: ColorTheme.white,
  },
  stt: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconRank: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  nameRank: {
    flex: 4,
    justifyContent: 'center',
    paddingLeft: 20 * d.ratioW,
  },
  imgIcon: {
    height: 40 * d.ratioH,
    width: 30 * d.ratioW,
  },
  txtNameRank: {
    fontSize: FontSize.large,
    color: ColorTheme.black,
  },
});
