import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import Axios from 'axios';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Images, ColorTheme } from '../../themes';
import LanguageVI from '../../constants/Language';

class RankingInfo extends PureComponent {
  state = {};

  componentDidMount() {}

  rederRow = (stt, image, nameRank) => {
    return (
      <View style={styles.row}>
        <View style={styles.stt}>
          <Text style={styles.txtNameRank}>{stt}</Text>
        </View>
        <View style={styles.iconRank}>
          <Image source={image} style={styles.imgIcon} />
        </View>
        <View style={styles.nameRank}>
          <Text style={styles.txtNameRank}>{nameRank}</Text>
        </View>
      </View>
    );
  };

  render() {
    return (
      <ScrollView>
        {this.rederRow(17, Images.rankIcon17, 'Huyền thoại')}
        {this.rederRow(16, Images.rankIcon16, 'Đại tướng')}
        {this.rederRow(15, Images.rankIcon15, 'Thượng tướng')}
        {this.rederRow(14, Images.rankIcon14, 'Trung tướng')}
        {this.rederRow(13, Images.rankIcon13, 'Thiếu tướng')}
        {this.rederRow(12, Images.rankIcon12, 'Đại tá')}
        {this.rederRow(11, Images.rankIcon11, 'Thượng tá')}
        {this.rederRow(10, Images.rankIcon10, 'Trung tá')}
        {this.rederRow(9, Images.rankIcon09, 'Thiếu tá')}
        {this.rederRow(8, Images.rankIcon08, 'Đại úy')}
        {this.rederRow(7, Images.rankIcon07, 'Thượng úy')}
        {this.rederRow(6, Images.rankIcon06, 'Trung úy')}
        {this.rederRow(5, Images.rankIcon05, 'Thiếu úy')}
        {this.rederRow(4, Images.rankIcon04, 'Thượng sĩ')}
        {this.rederRow(3, Images.rankIcon03, 'Trung sĩ')}
        {this.rederRow(2, Images.rankIcon02, 'Hạ sĩ')}
        {this.rederRow(1, Images.rankIcon01, 'Binh nhất')}
      </ScrollView>
    );
  }
}

export default RankingInfo;
