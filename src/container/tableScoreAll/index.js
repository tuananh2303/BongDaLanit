import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import TeamTableScroceAll from '../../components/TeamTableScroceAll';
import Icon from 'react-native-vector-icons/Ionicons';
import data from './matchDataFake';

class Home extends PureComponent {
  state = {};

  render() {
    return (
      <View>
        <TeamTableScroceAll data={data} />
      </View>
    );
  }
}

export default Home;
