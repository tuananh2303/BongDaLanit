import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import Table from '../../components/Table';
import LanguageVI from '../../constants/Language';
import TableLongPart from '../../components/TableLongPart';
import {
  forward,
  dataInjury,
  manager,
  goalKeeper,
  defender,
  midfieldder,
} from './dataInjuriesFake';
import InfoKickRow from '../../components/InfoKickRow';

class TeamSquad extends PureComponent {
  state = {};

  // componentDidMount() {
  //   console.log('ren squad');
  // }

  renderInjury = data => {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => (
          <InfoKickRow
            onPressPlayer={() => this.props.onPressPlayer()}
            title={item.playerNumber + '. ' + item.playerName}
            sub_title={
              <View>
                <Text>{item.injuryDetail}</Text>
                <Text>
                  {LanguageVI.ExpectedReturn + ': ' + item.dateComeback}
                </Text>
              </View>
            }
          />
        )}
      />
    );
  };

  renderManager = data => {
    return (
      <View style={styles.viewPart}>
        <Text style={styles.titleText}>{LanguageVI.Mananger}</Text>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => (
            <InfoKickRow
              onPressPlayer={() => this.props.onPressPlayer()}
              title={item.name}
            />
          )}
        />
      </View>
    );
  };

  renderGoalkeeper = data => {
    return (
      <View style={styles.viewPart}>
        <Text style={styles.titleText}>{LanguageVI.Goalkeepers}</Text>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => (
            <InfoKickRow
              onPressPlayer={() => this.props.onPressPlayer()}
              title={item.playerNumber + '. ' + item.playerName}
              sub_title={
                item.matchNumber == 0 ? (
                  <Text>{LanguageVI.NoMatchesYet}</Text>
                ) : (
                  <Text>
                    {item.matchNumber + ' ' + LanguageVI.matchesInLeague}
                  </Text>
                )
              }
            />
          )}
        />
      </View>
    );
  };

  renderDefender = (data, title) => {
    return (
      <View style={styles.viewPart}>
        <Text style={styles.titleText}>{title}</Text>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => (
            <InfoKickRow
              onPressPlayer={() => this.props.onPressPlayer()}
              title={item.playerNumber + '. ' + item.playerName}
              sub_title={
                item.matchNumber == 0 ? (
                  <Text>{LanguageVI.NoMatchesYet}</Text>
                ) : (
                  <Text>
                    {item.matchNumber +
                      ' ' +
                      LanguageVI.matches +
                      ' & ' +
                      item.goalNumber +
                      ' ' +
                      LanguageVI.goalsInLeague}
                  </Text>
                )
              }
            />
          )}
        />
      </View>
    );
  };

  render() {
    return (
      <View style={styles.ViewMain}>
        <Table
          title={LanguageVI.Injuries}
          showAllText={LanguageVI.SHOWALL}
          content={this.renderInjury(dataInjury)}
        />

        <TableLongPart
          content={
            <View>
              {this.renderManager(manager)}
              {this.renderGoalkeeper(goalKeeper)}
              {this.renderDefender(defender, LanguageVI.Defenders)}
              {this.renderDefender(midfieldder, LanguageVI.Midfielders)}
              {this.renderDefender(forward, LanguageVI.Forwards)}
            </View>
          }
        />
      </View>
    );
  }
}

export default TeamSquad;
