const dataInjury = [
  {
    id: 1,
    playerNumber: 15,
    playerName: 'Webb Bolino',
    injuryDetail: 'Bolino',
    dateComeback: '20/03/2018',
  },
  {
    id: 2,
    playerNumber: 15,
    playerName: 'Jami Nosworthy',
    injuryDetail: 'Nosworthy',
    dateComeback: '13/04/2018',
  },
  {
    id: 3,
    playerNumber: 2,
    playerName: 'Hillard Gonning',
    injuryDetail: 'Gonning',
    dateComeback: '13/08/2018',
  },
  {
    id: 4,
    playerNumber: 9,
    playerName: 'Ursala Pedley',
    injuryDetail: 'Pedley',
    dateComeback: '07/06/2018',
  },
];

const manager = [
  {
    id: 1,
    name: 'Alena Danielski',
  },
  {
    id: 2,
    name: 'Bartel Nobes',
  },
  {
    id: 3,
    name: 'Cherianne Dibb',
  },
];

const goalKeeper = [
  {
    id: 1,
    playerNumber: 26,
    playerName: 'Falito Sowter',
    matchNumber: 1,
  },
  {
    id: 2,
    playerNumber: 29,
    playerName: 'Standford Pither',
    matchNumber: 3,
  },
  {
    id: 3,
    playerNumber: 6,
    playerName: 'Berri Owenson',
    matchNumber: 3,
  },
  {
    id: 4,
    playerNumber: 20,
    playerName: 'Theo Clutton',
    matchNumber: 0,
  },
];

const defender = [
  {
    id: 1,
    playerNumber: 8,
    playerName: 'Carrol Bakes',
    matchNumber: 22,
    goalNumber: 1,
  },
  {
    id: 2,
    playerNumber: 31,
    playerName: 'Marie Hanham',
    matchNumber: 17,
    goalNumber: 1,
  },
  {
    id: 3,
    playerNumber: 25,
    playerName: 'Tucker Idiens',
    matchNumber: 37,
    goalNumber: 3,
  },
  {
    id: 4,
    playerNumber: 18,
    playerName: 'Orren Chidzoy',
    matchNumber: 27,
    goalNumber: 1,
  },
  {
    id: 5,
    playerNumber: 38,
    playerName: 'Tate Bettenson',
    matchNumber: 15,
    goalNumber: 0,
  },
  {
    id: 6,
    playerNumber: 14,
    playerName: 'Issi Codd',
    matchNumber: 7,
    goalNumber: 3,
  },
  {
    id: 7,
    playerNumber: 36,
    playerName: 'Robby Overlow',
    matchNumber: 0,
    goalNumber: 3,
  },
  {
    id: 8,
    playerNumber: 23,
    playerName: 'Rorke Quibell',
    matchNumber: 2,
    goalNumber: 1,
  },
  {
    id: 9,
    playerNumber: 18,
    playerName: 'Franciska Possell',
    matchNumber: 24,
    goalNumber: 2,
  },
  {
    id: 10,
    playerNumber: 29,
    playerName: 'Patrizio Pickford',
    matchNumber: 8,
    goalNumber: 0,
  },
];

const midfieldder = [
  {
    id: 1,
    playerNumber: 26,
    playerName: 'Talia Burn',
    matchNumber: 22,
    goalNumber: 0,
  },
  {
    id: 2,
    playerNumber: 24,
    playerName: 'Jessi Aldington',
    matchNumber: 4,
    goalNumber: 3,
  },
  {
    id: 3,
    playerNumber: 3,
    playerName: 'Marcellus Claxson',
    matchNumber: 19,
    goalNumber: 3,
  },
  {
    id: 4,
    playerNumber: 43,
    playerName: 'Domenic Chantillon',
    matchNumber: 31,
    goalNumber: 0,
  },
  {
    id: 5,
    playerNumber: 33,
    playerName: "Rockwell O'Kieran",
    matchNumber: 22,
    goalNumber: 0,
  },
  {
    id: 6,
    playerNumber: 10,
    playerName: 'Sargent Althrope',
    matchNumber: 9,
    goalNumber: 2,
  },
  {
    id: 7,
    playerNumber: 55,
    playerName: 'Armand Kach',
    matchNumber: 8,
    goalNumber: 0,
  },
  {
    id: 8,
    playerNumber: 30,
    playerName: 'Jonis Shimony',
    matchNumber: 6,
    goalNumber: 3,
  },
  {
    id: 9,
    playerNumber: 4,
    playerName: 'Kailey Beyn',
    matchNumber: 33,
    goalNumber: 1,
  },
  {
    id: 10,
    playerNumber: 58,
    playerName: 'Sofia Maddrell',
    matchNumber: 28,
    goalNumber: 2,
  },
];

const forward = [
  {
    id: 1,
    playerNumber: 7,
    playerName: 'Karyl Berk',
    matchNumber: 5,
    goalNumber: 1,
  },
  {
    id: 2,
    playerNumber: 54,
    playerName: 'Renard Ledgister',
    matchNumber: 15,
    goalNumber: 0,
  },
  {
    id: 3,
    playerNumber: 6,
    playerName: 'La verne Watman',
    matchNumber: 5,
    goalNumber: 0,
  },
  {
    id: 4,
    playerNumber: 32,
    playerName: 'Daphna Rozsa',
    matchNumber: 1,
    goalNumber: 3,
  },
  {
    id: 5,
    playerNumber: 54,
    playerName: 'Rhianna Bellward',
    matchNumber: 11,
    goalNumber: 2,
  },
  {
    id: 6,
    playerNumber: 26,
    playerName: 'Lonnie Loder',
    matchNumber: 14,
    goalNumber: 2,
  },
];

export { forward, dataInjury, manager, goalKeeper, defender, midfieldder };
