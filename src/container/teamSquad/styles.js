import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  ViewMain: {
    backgroundColor: ColorTheme.white2,
    // backgroundColor: 'red',
    flex: 1,
  },
  viewPart: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: ColorTheme.white2,
  },

  titleText: {
    color: ColorTheme.gray,
    marginLeft: 10 * d.ratioW,
    marginBottom: 10 * d.ratioH,
    fontSize: FontSize.medium,
  },
});
