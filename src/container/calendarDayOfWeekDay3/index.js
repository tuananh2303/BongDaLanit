import React, { PureComponent } from 'react';
import { FlatList, ScrollView } from 'react-native';
// import firebase from 'react-native-firebase';
import { connect } from 'react-redux';
import Axios from 'axios';
import Card from '../../components/Card';
import { URL_LIVESCORE } from '../../constants/URL';
import HomeDataFake from './HomeDataFake';
import LanguageVI from '../../constants/Language';
import { Images } from '../../themes';
import Banner from '../../components/Banner';

// const database = firebase.database();
let myInterval = null;

class CalendarDayOfWeek extends PureComponent {
  state = {
    dataAll: [],
    data: [],
    dataSuccess: false,
    error: false,
    refreshing: false,
    continueReferesh: true,
  };

  componentDidMount() {
    this.getData();
    this.onRefresh();
    console.log(this.props.settingReducer.data.timeToRefresh * 1000);
  }

  getMatchesByLeagueID = (id, dataInput) => {
    const result = [];
    Object.values(dataInput.matches).map(item => {
      if (parseInt(item[3], 10) == parseInt(id, 10)) {
        // array [3] trong match la id cua league
        const itemToArr = {
          match_id: item[0], // array[0] la id cua tran dau
          match_date: item[6], // array[6] la thoi gian bat dau tran dau
          match_date_h1_start: item[7], // array[7] la thoi gian bat dau hiep 1
          match_date_h2_start: item[8], // array[8] la thoi gian bat dau hiep 2
          status: item[9], // status doi bong ow vi tri thu [9]
          team1_name: dataInput.teams[item[4]][1], // trong array[4] trong matches la id cua team , array[1] trong team la team_name
          team2_name: dataInput.teams[item[5]][1],
          score1: item[10], // ty so doi 1 la array[10]
          score2: item[11], // ty so doi 2 la array[11]
          team1_id: item[4],
          team2_id: item[5],
          team1_image: dataInput.teams[item[4]][2], // trong array[4] trong matches la id cua team , array[2] trong team la anh
          team2_image: dataInput.teams[item[5]][2],
        };
        result.push(itemToArr);
      }
    });
    return result;
  };

  getMatchesByStatus = dataInput => {
    const result = [];
    Object.values(dataInput.matches).map(item => {
      if (parseInt(item[9], 10) < 10 && parseInt(item[9], 10) > 0) {
        // array [9] trong match la status cua tran dau ( >0 && < 10) la cac tran dau dang dien ra
        const itemToArr = {
          match_id: item[0], // array[0] la id cua tran dau
          match_date: item[6], // array[6] la thoi gian bat dau tran dau
          match_date_h1_start: item[7], // array[7] la thoi gian bat dau hiep 1
          match_date_h2_start: item[8], // array[8] la thoi gian bat dau hiep 2
          status: item[9], // status doi bong ow vi tri thu [9]
          team1_name: dataInput.teams[item[4]][1], // trong array[4] trong matches la id cua team , array[1] trong team la team_name
          team2_name: dataInput.teams[item[5]][1],
          score1: item[10], // ty so doi 1 la array[10]
          score2: item[11], // ty so doi 2 la array[11]
          team1_id: item[4],
          team2_id: item[5],
          team1_image: dataInput.teams[item[4]][2], // trong array[4] trong matches la id cua team , array[2] trong team la anh
          team2_image: dataInput.teams[item[5]][2],
        };
        result.push(itemToArr);
      }
    });
    return result;
  };

  orderDataByLeague = response => {
    let leaguesResponse = [];
    if (this.props.liveScore) {
      leaguesResponse = [
        {
          id: null,
          team_name: LanguageVI.MatchLiveScore,
          image: Images.logoTeamDefault,
          data: this.getMatchesByStatus(response),
        },
      ];
    }
    if (!this.props.liveScore) {
      Object.values(response.leagues).map(item => {
        const itemToArr = {
          id: item[0],
          team_name: item[1],
          image: item[11],
          data: this.getMatchesByLeagueID(item[0], response),
        };
        leaguesResponse.push(itemToArr);
      });
    }
    return leaguesResponse;
  };

  getData = () => {
    console.log(this.props.dayInRange + 'getData');
    Axios.get(URL_LIVESCORE)
      .then(responseInit => {
        console.log(responseInit);
        const response = responseInit.data.data;

        this.setState({
          dataAll: response,
          data: this.orderDataByLeague(response),
          dataSuccess: true,
          lastUpdated: response.lastUpdated,
        });
      })
      .catch(error => {
        console.log(error);
      });
  };

  onRefresh = () => {
    myInterval = setInterval(() => {
      this.refreshData();
    }, this.props.settingReducer.data.timeToRefresh * 1000);
  };

  refreshData = () => {
    console.log('refresh calendar');

    Axios.get(URL_LIVESCORE + '?lastUpdated=' + this.state.lastUpdated)
      .then(responseInit => {
        const response = responseInit.data.data;
        const dataAll = this.state.dataAll;

        Object.values(response.matches).map(item => {
          dataAll.matches[item[0]] = item;
        });
        this.setState({
          lastUpdated: response.lastUpdated,
          data: this.orderDataByLeague(dataAll),
          dataAll,
        });
      })
      .catch(error => {
        console.log(error);
      });
  };

  componentWillUnmount() {
    console.log('unmount');

    clearInterval(myInterval);
  }

  render() {
    if (this.state.dataSuccess) {
      return (
        <ScrollView>
          <Banner />

          <FlatList
            refreshing={this.state.refreshing}
            onRefresh={() => {
              this.setState({
                refreshing: true,
              });
              setTimeout(
                () =>
                  this.setState({
                    refreshing: false,
                  }),
                500,
              );
            }}
            showsVerticalScrollIndicator={false}
            data={this.state.data}
            keyExtractor={(item, index) => index.toString()}
            initialNumToRender={3}
            renderItem={({ item, index }) => (
              <Card
                data={item}
                onPressTeam={() => {}}
                onPressMatch={data => {
                  // console.log(id);
                  this.props.navigation.navigate('Match', {
                    data,
                  });
                }}
              />
            )}
          />
        </ScrollView>
      );
    }
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={HomeDataFake}
        keyExtractor={(item, index) => index.toString()}
        initialNumToRender={1}
        renderItem={({ item, index }) => (
          <Card data={item} onPressTeam={() => {}} onPressMatch={() => {}} />
        )}
      />
    );
  }
}

const mapStateToProps = state => ({
  calendarReducer: state.calendarReducer,
  dataUserTeamLeagueLike: state.getUserTeamLeagueReducers,
  settingReducer: state.settingReducer,
});

const mapDispatchToProps = dispatch => ({
  // fetchDatagetUserTeamLeague: userID =>
  //   dispatch(fetchDatagetUserTeamLeague(userID)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CalendarDayOfWeek);
