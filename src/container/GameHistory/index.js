import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import Axios from 'axios';
import { connect } from 'react-redux';
import styles from './styles';
import GameHistoryRow from '../../components/GameHistoryRow';
import { URL_PREDICT_HISTORY } from '../../constants/URL';

class GameHistory extends PureComponent {
  state = {
    data: null,
    dataSuccess: false,
  };

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    console.log('gamehistory get data');

    Axios.get(URL_PREDICT_HISTORY + this.props.userReducer.dataFull.user.id)
      .then(response => {
        // console.log(response.data);
        this.setState({
          data: response.data.data,
          dataSuccess: true,
        });
      })
      .catch(error => console.log(error));
  };

  render() {
    if (this.state.dataSuccess) {
      return (
        <View style={styles.container}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={this.state.data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => {
              if (item != null) {
                if (item.status != 0) {
                  return (
                    <View style={styles.row}>
                      <GameHistoryRow data={item} />
                    </View>
                  );
                }
              }
            }}
          />
        </View>
      );
    }
    return null;
  }
}

const mapStateToProps = state => ({
  userReducer: state.userReducer,
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GameHistory);
