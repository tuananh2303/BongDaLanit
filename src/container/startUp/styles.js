import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  viewMain: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    flex: 1,
  },
  Image: {
    height: 500 * d.ratioH,
    width: d.windowSize.width,
  },
});
