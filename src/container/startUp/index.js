import React, { PureComponent } from 'react';
import { View, Image, AsyncStorage } from 'react-native';
import Axios from 'axios';
import { connect } from 'react-redux';
import Images from '../../themes/Images';
import {
  LOGIN_REF,
  SETTING_APP,
  SETTING_APP_DEFAULT,
} from '../../constants/AsyncStorage';
import { URL_LOGIN } from '../../constants/URL';
import styles from './styles';
import { addUserInfo, userInfo } from '../../redux/actions/userAction';
import { settingInit } from '../../redux/actions/settingAction';
import storeData from '../../utilities/AsynStorage';

class StartUp extends PureComponent {
  state = {};

  componentDidMount() {
    this.checkSetting();
    this.isFirstTime();
  }

  checkSetting = async () => {
    try {
      const valueString = await AsyncStorage.getItem(SETTING_APP);
      const value = JSON.parse(valueString);
      if (value == null) {
        this.props.settingInit(SETTING_APP_DEFAULT);
        storeData(SETTING_APP, SETTING_APP_DEFAULT);
      }
      if (value != null) {
        this.props.settingInit(value);
      }
    } catch (error) {
      console.log(error);
    }
  };

  isFirstTime = async () => {
    try {
      const valueString = await AsyncStorage.getItem(LOGIN_REF);
      const value = JSON.parse(valueString);

      if (value == null) {
        setTimeout(() => {
          this.props.navigation.navigate('Login');
        }, 1000);
      }

      if (value != null) {
        this.getDataUser(value);
      }
    } catch (error) {
      console.log(error);
    }
  };

  getDataUser = async data => {
    Axios.get(URL_LOGIN + data.fbid + '?token=' + data.token)
      .then(response => {
        console.log(response);
        if (response.data.status == 'success') {
          this.props.addUserInfo(data);
          this.props.userInfo(response.data);
          this.props.navigation.navigate('AppStack');
        }
        if (response.data.status == 'error') {
          this.props.navigation.navigate('Login');
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    return (
      <View style={styles.viewMain}>
        <Image
          resizeMode="contain"
          source={Images.startUpImage}
          style={styles.Image}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  settingReducer: state.settingReducer,
  userReducer: state.userReducer,
});

const mapDispatchToProps = dispatch => ({
  addUserInfo: data => dispatch(addUserInfo(data)),
  userInfo: data => dispatch(userInfo(data)),
  settingInit: data => dispatch(settingInit(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(StartUp);
