import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  ScrollView,
} from 'react-native';
import Axios from 'axios';
import { connect } from 'react-redux';

import styles from './styles';
import GameRow1 from '../../components/GameRow1';
import { URL_PREDICT_LIST } from '../../constants/URL';
import Banner from '../../components/Banner';

class GameSub extends PureComponent {
  state = {
    data: null,
    dataSuccess: false,
  };

  componentDidMount() {
    console.log(this.props.userReducer);

    this.getData();
  }

  getData = () => {
    console.log('gamesub get data');

    Axios.get(
      URL_PREDICT_LIST +
        this.props.userReducer.dataFull.user.id +
        '?token=' +
        this.props.userReducer.dataFull.user.token,
    )
      .then(response => {
        // console.log(response.data);
        this.setState({
          data: response.data.data.matches,
          dataSuccess: true,
        });
      })
      .catch(error => console.log(error));
  };

  render() {
    if (this.state.dataSuccess) {
      return (
        <ScrollView style={styles.container}>
          <Banner />
          <FlatList
            showsVerticalScrollIndicator={false}
            data={this.state.data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => {
              if (item != null) {
                return (
                  <View style={styles.row}>
                    <GameRow1
                      data={item}
                      userConfirmBet={() => this.getData()}
                    />
                  </View>
                );
              }
            }}
          />
        </ScrollView>
      );
    }
    return null;
  }
}

// export default GameSub;

const mapStateToProps = state => ({
  userReducer: state.userReducer,
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GameSub);
