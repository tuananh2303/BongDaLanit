import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';
// import { ColorTheme, FontSize } from '../../themes';

export default StyleSheet.create({
  viewMain: {
    flex: 1,
    backgroundColor: ColorTheme.gray300,
  },

  ViewContent: {
    flex: 1,
    backgroundColor: 'white',
  },
  Footer: {
    height: 100 * d.ratioH,
    backgroundColor: 'white',
    elevation: 3,
  },
  ViewNumTab: {
    height: 50 * d.ratioH,
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 5 * d.ratioH,
  },
  ViewNext: {
    // flexDirection: 'row',
    // justifyContent: 'space-between',
    alignItems: 'center',
  },
  NextBtn: {
    height: 30 * d.ratioH,
    // marginHorizontal: 25,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 2,
    width: d.windowSize.width / 5,
    backgroundColor: 'white',
    borderRadius: 10,
  },

  NextText: {
    color: ColorTheme.black,
    fontSize: FontSize.medium,
  },

  tab: {
    height: 25 * d.ratioH,
    width: 25 * d.ratioW,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },

  activeTab: {
    backgroundColor: ColorTheme.Teal900,
  },

  backTab: {
    backgroundColor: ColorTheme.Teal500,
  },

  TextActive: {
    fontSize: FontSize.medium,
    color: ColorTheme.white,
  },

  TextNotActive: {
    fontSize: FontSize.medium,
    color: ColorTheme.Teal900,
  },

  ListItemView: {
    flexDirection: 'row',
    margin: 10,
    marginLeft: 7 * d.ratioW,
  },
  ListItemText: {
    color: ColorTheme.black,
    fontSize: FontSize.medium,
    marginLeft: 10 * d.ratioW,
  },
  ListItemImage: {
    height: 40 * d.ratioH,
    width: 40 * d.ratioW,
  },
  ListItemLeft: {
    flex: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  ListItemRight: {
    flex: 1,
    alignItems: 'center',
  },
});
