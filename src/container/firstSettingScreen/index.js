import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Image, FlatList } from 'react-native';
// import firebase from 'react-native-firebase';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Images, ColorTheme } from '../../themes';
import LanguageVI from '../../constants/Language';
import Header from '../../components/Header';
import { URL_IMAGE } from '../../constants/URL';
import FastImage from '../../components/FastImage';

const TEAM = 'TEAM';
const LEAGUE = 'LEAGUE';

class FirstSetting extends PureComponent {
  state = {
    data: [],
    dataSuccess: false,
  };

  componentDidMount() {
    console.log('did mount screen', this.props.type);
    const result = [];
    Object.values(this.props.data).map(item => {
      let a = {
        id: parseInt(item[0]),
        name: item[1],
        image: item[2],
        favorite: true,
      };
      result.push(a);
    });

    this.setState({
      data: result,
      dataSuccess: true,
    });
  }

  renderItem = data => {
    // console.log(data);
    return (
      <View style={styles.ListItemView}>
        <View style={styles.ListItemLeft}>
          <FastImage
            linkImageDefault={Images.logoTeamDefault}
            resizeMode="contain"
            linkImage={
              'http://info.nowgoal.com/Image/team/images/' + data.image
            }
            style={styles.ListItemImage}
          />
          <Text style={styles.ListItemText}>{data.name}</Text>
        </View>
        <TouchableOpacity
          style={styles.ListItemRight}
          onPress={() => _onPress(data.id)}
        >
          <Icon name="md-checkbox" size={22} color={ColorTheme.Teal900} />
        </TouchableOpacity>
      </View>
    );
  };

  _onPress = () => {
    console.log('press');
  };

  renderItemLeague = data => {
    // console.log(data);
    return (
      <View style={styles.ListItemView}>
        <View style={styles.ListItemLeft}>
          <FastImage
            linkImage={URL_IMAGE + data.image}
            style={styles.ListItemImage}
            resizeMode="contain"
            linkImageDefault={Images.logoTeamDefault}
          />
          <Text style={styles.ListItemText}>{data.name}</Text>
        </View>
        <TouchableOpacity
          style={styles.ListItemRight}
          onPress={() => _onPress(data.id)}
        >
          <Icon name="md-checkbox" size={22} color={ColorTheme.Teal900} />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    console.log('render first');

    if (this.state.dataSuccess) {
      return (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={this.state.data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => {
            if (this.props.type == TEAM) {
              return this.renderItem(item);
            }
            return this.renderItemLeague(item);
          }}
        />
      );
    }
    return null;
  }
}

export default FirstSetting;
