import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    // height: 60 * d.ratioH,
    justifyContent: 'center',
    // elevation: 3,
    borderBottomColor: ColorTheme.gray500,
    borderBottomWidth: 0.25,
    backgroundColor: 'white',
    borderTopColor: ColorTheme.gray500,
    borderTopWidth: 0.25,
  },
  rowsub: {
    height: 50 * d.ratioH,
    justifyContent: 'center',
  },
});
