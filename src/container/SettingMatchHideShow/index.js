import React, { PureComponent } from 'react';
import styles from './styles';
import { View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import LanguageVI from '../../constants/Language';
import Header from '../../components/Header';
import Switch from '../../components/Switch';
import { ColorTheme } from '../../themes';
import IconBackCustom from '../../components/IconBackCustom';

const TOGGLESWITCH = 'toggleSwitch';
const TOGGLESWITCH1 = 'toggleSwitch1';
const TOGGLESWITCH2 = 'toggleSwitch2';
const TOGGLESWITCH3 = 'toggleSwitch3';
const TOGGLESWITCH4 = 'toggleSwitch4';
const TOGGLESWITCH5 = 'toggleSwitch5';

class SettingMatchHideShow extends PureComponent {
  state = {
    SwitchOnValueHolder: false,
    SwitchOnValueHolder1: false,
    SwitchOnValueHolder2: false,
    SwitchOnValueHolder3: false,
    SwitchOnValueHolder4: false,
    SwitchOnValueHolder5: false,
  };

  toggleSwitch = type => {
    if (type === TOGGLESWITCH) {
      return this.setState({
        SwitchOnValueHolder: !this.state.SwitchOnValueHolder,
      });
    }

    if (type === TOGGLESWITCH1) {
      return this.setState({
        SwitchOnValueHolder1: !this.state.SwitchOnValueHolder1,
      });
    }

    if (type === TOGGLESWITCH2) {
      return this.setState({
        SwitchOnValueHolder2: !this.state.SwitchOnValueHolder2,
      });
    }

    if (type === TOGGLESWITCH3) {
      return this.setState({
        SwitchOnValueHolder3: !this.state.SwitchOnValueHolder3,
      });
    }

    if (type === TOGGLESWITCH4) {
      return this.setState({
        SwitchOnValueHolder4: !this.state.SwitchOnValueHolder4,
      });
    }

    if (type === TOGGLESWITCH5) {
      return this.setState({
        SwitchOnValueHolder5: !this.state.SwitchOnValueHolder5,
      });
    }
  };

  rowToggleSwitch = (toggleSwitch, icon, title, stateValue) => {
    return (
      <TouchableOpacity
        style={styles.rowsub}
        onPress={() => this.toggleSwitch(toggleSwitch)}
      >
        <Switch
          icon={icon}
          title={title}
          value={stateValue}
          toggleSwitch1={() => this.toggleSwitch(toggleSwitch)}
        />
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Header
          title={LanguageVI.MatchEvent}
          rightHeader
          icon={
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <IconBackCustom />
              {/* <Icon name="md-arrow-back" size={22} color={ColorTheme.Teal900} /> */}
            </TouchableOpacity>
          }
          // elevation={true}
        />

        <View style={styles.row}>
          {this.rowToggleSwitch(
            'toggleSwitch',
            'ios-stats',
            LanguageVI.OffTheWoodwork,
            this.state.SwitchOnValueHolder,
          )}

          {this.rowToggleSwitch(
            'toggleSwitch1',
            'ios-man',
            LanguageVI.injuries,
            this.state.SwitchOnValueHolder1,
          )}

          {this.rowToggleSwitch(
            'toggleSwitch2',
            'ios-man',
            LanguageVI.Substitutions,
            this.state.SwitchOnValueHolder2,
          )}

          {this.rowToggleSwitch(
            'toggleSwitch3',
            'ios-man',
            LanguageVI.Penalties,
            this.state.SwitchOnValueHolder3,
          )}

          {this.rowToggleSwitch(
            'toggleSwitch4',
            'ios-man',
            LanguageVI.YellowCards,
            this.state.SwitchOnValueHolder4,
          )}

          {this.rowToggleSwitch(
            'toggleSwitch5',
            'ios-man',
            LanguageVI.RedCards,
            this.state.SwitchOnValueHolder5,
          )}
        </View>
      </View>
    );
  }
}

export default SettingMatchHideShow;
