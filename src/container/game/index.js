import React, { PureComponent } from 'react';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  AsyncStorage,
  Image,
} from 'react-native';
import { LoginButton, AccessToken, LoginManager } from 'react-native-fbsdk';
import Axios from 'axios';
import LottieView from 'lottie-react-native';
import GameSub from '../GameSub';
import GameHistory from '../GameHistory';
import GameRank from '../GameRank';
import GameRankTopEvent from '../GameRankTopEvent';
import GameRankTopPredict from '../GameRankTopPredict';
import RankingInfo from '../RankingInfo';

import styles from './styles';
import Header from '../../components/Header';
import LanguageVI from '../../constants/Language';
import { ColorTheme, Images } from '../../themes';
import { URL_SIGNIN, URL_GET_FB_INFO, URL_IMAGE } from '../../constants/URL';
import { LOGIN_REF } from '../../constants/AsyncStorage';
import { addUserInfo, userInfo } from '../../redux/actions/userAction';
import FastImage from '../../components/FastImage';
import Lottie from '../../themes/Lottie';

class Game extends PureComponent {
  state = {
    /* eslint-disable */
    index: 0,
    routes: [
      { key: 'gameplay', title: LanguageVI.GamePlay },
      { key: 'history', title: LanguageVI.History },
      { key: 'rankingTopEvent', title: LanguageVI.TopEvent },
      { key: 'rankingTopPredict', title: LanguageVI.TopPredict },
      { key: 'rankingThanhDu', title: LanguageVI.MasterPredict },
      { key: 'rankingInfo', title: LanguageVI.RankingInfo },
    ],
    /* eslint-enable */
  };

  facebookLogin = () => {
    LoginManager.logInWithReadPermissions(['public_profile']).then(
      result => {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          // console.log('Login success with permissions: ' + result.toString());
          AccessToken.getCurrentAccessToken().then(data => {
            console.log(data.accessToken.toString());
            this.getFacebookInfo(data.accessToken.toString());
          });
        }
      },
      error => {
        console.log('Login fail with error: ' + error);
      },
    );
  };

  getFacebookInfo = async accessToken => {
    await Axios.get(URL_GET_FB_INFO + accessToken)
      .then(response => {
        // handle success
        console.log(response);
        this.pushDataFacebookToServer(
          response.data.id,
          response.data.name,
          accessToken,
        );
      })
      .catch(error => {
        // handle error
        console.log(error);
      });
  };

  pushDataFacebookToServer = async (fbid, name, token) => {
    const dataFB = {
      fbid,
      name,
      token,
    };
    await Axios.post(URL_SIGNIN, dataFB)
      .then(response => {
        console.log(response);
        if (response.data.status == 'success') {
          AsyncStorage.setItem(LOGIN_REF, JSON.stringify(dataFB));
          console.log(dataFB);
          this.props.addUserInfo(dataFB);
          this.props.userInfo(response.data);
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  indicatorStyle = () => {
    if (this.props.settingReducer.data.theme == 1) {
      return styles.indicator;
    }
    return styles.indicator2;
  };

  _renderTabBarGetStyle = () => {
    if (this.props.settingReducer.data.theme == 2) {
      return styles.tabbar2;
    }
    if (this.props.settingReducer.data.theme == 3) {
      return styles.tabbar3;
    }
    if (this.props.settingReducer.data.theme == 4) {
      return styles.tabbar4;
    }
    if (this.props.settingReducer.data.theme == 5) {
      return [styles.tabbar4, { backgroundColor: ColorTheme.purple700 }];
    }
    return styles.tabbar;
  };

  renderLabelGetStyle = () => {
    if (this.props.settingReducer.data.theme == 1) {
      return styles.labelStyle;
    }
    return styles.labelStyle2;
  };

  renderLabel = ({ route }) => (
    <View style={styles.viewTab}>
      <Text style={this.renderLabelGetStyle()}>{route.title}</Text>
    </View>
  );

  _renderTabBar = props => {
    return (
      <TabBar
        {...props}
        scrollEnabled
        indicatorStyle={this.indicatorStyle()}
        style={this._renderTabBarGetStyle()}
        tabStyle={styles.tab}
        renderLabel={this.renderLabel}
      />
    );
  };

  gamePlay = () => <GameSub />;

  gameHistory = () => <GameHistory />;

  rankingTopEvent = () => (
    <GameRankTopEvent navigation={this.props.navigation} />
  );

  rankingTopPredict = () => (
    <GameRankTopPredict navigation={this.props.navigation} />
  );

  rankingThanhDu = () => <GameRank navigation={this.props.navigation} />;

  rankingInfo = () => <RankingInfo navigation={this.props.navigation} />;

  Header = () => {
    return (
      <Header
        title={
          this.props.userReducer.dataFull == null
            ? LanguageVI.Game
            : this.props.userReducer.dataFull.user.coin + ': points'
        }
        rightHeader={
          <TouchableOpacity
            onPress={() => {
              if (this.props.userReducer.data != null) {
                this.props.navigation.navigate('Profile', {
                  id: this.props.userReducer.dataFull.user.id,
                });
              }
            }}
          >
            <FastImage
              linkImageDefault={Images.defaultImage}
              linkImage={
                this.props.userReducer.dataFull == null
                  ? URL_IMAGE
                  : this.props.userReducer.dataFull.avatar
              }
              resizeMode="contain"
              style={{ height: 30, width: 30, borderRadius: 25 }}
            />
          </TouchableOpacity>
        }
      />
    );
  };

  render() {
    if (this.props.userReducer.dataFull != null) {
      if (this.props.userReducer.dataFull.class == 'banned') {
        return (
          <View>
            {this.Header()}
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
              <LottieView
                source={Lottie.error}
                autoPlay
                loop
                style={{ height: 250, width: 250 }}
              />
              <Text>{LanguageVI.DontHavePremission}</Text>
            </View>
          </View>
        );
      }
      return (
        <View style={styles.viewMain}>
          {this.Header()}
          <TabView
            navigationState={this.state}
            renderTabBar={this._renderTabBar}
            renderScene={SceneMap({
              gameplay: this.gamePlay,
              history: this.gameHistory,
              rankingTopEvent: this.rankingTopEvent,
              rankingTopPredict: this.rankingTopPredict,
              rankingThanhDu: this.rankingThanhDu,
              rankingInfo: this.rankingInfo,
            })}
            onIndexChange={index => this.setState({ index })}
            useNativeDriver
            initialLayout={{
              height: 0,
              width: Dimensions.get('window').width,
            }}
          />
        </View>
      );
    }
    return (
      <View style={styles.fbMain}>
        <TouchableOpacity
          style={styles.fb}
          onPress={() => this.facebookLogin()}
        >
          <Icon name="logo-facebook" size={30} color="white" />
          <Text style={styles.txt_fb}> Continue with Facebook</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

// export default Game;

const mapStateToProps = state => ({
  userReducer: state.userReducer,
  settingReducer: state.settingReducer,
});

const mapDispatchToProps = dispatch => ({
  addUserInfo: data => dispatch(addUserInfo(data)),
  userInfo: data => dispatch(userInfo(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Game);
