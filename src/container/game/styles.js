import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  viewMain: {
    flex: 1,
    backgroundColor: ColorTheme.gray300,
  },
  tabbar: {
    backgroundColor: 'white',
  },
  tabbar3: {
    backgroundColor: ColorTheme.blue800,
  },
  tab: {
    height: 30 * d.ratioH,
    width: d.windowSize.width / 3 - 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  indicator: {
    backgroundColor: ColorTheme.Teal900,
  },
  labelStyle: {
    fontSize: 12,
    fontWeight: 'normal',
    color: ColorTheme.Teal900,
    marginBottom: 5 * d.ratioH,
  },
  viewTab: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  fb: {
    height: 50 * d.ratioH,
    backgroundColor: ColorTheme.indigo700,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    margin: 40,
    marginBottom: 50 * d.ratioH,
    marginTop: 10 * d.ratioH,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: ColorTheme.gray300,
  },
  txt_fb: {
    fontSize: FontSize.medium,
    fontWeight: '500',
    color: ColorTheme.white,
  },
  fbMain: {
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'flex-end',
  },
  viewCoin: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },

  indicator2: {
    backgroundColor: ColorTheme.white,
  },
  tabbar2: {
    backgroundColor: ColorTheme.Teal900,
  },
  labelStyle2: {
    fontSize: 14,
    fontWeight: 'normal',
    color: ColorTheme.white,
  },
  tabbar4: {
    backgroundColor: ColorTheme.black,
  },
});
