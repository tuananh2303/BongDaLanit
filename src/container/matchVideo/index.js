import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Axios from 'axios';

import Article from '../../components/Article';
import styles from './styles';
import EmptyComponent from '../../components/EmptyComponent';

class MatchVideo extends PureComponent {
  state = {
    error: false,
    data: [],
    dataSuccess: false,
  };

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    Axios.get('https://muldata.tintuc.app/posts/category/0/1')
      .then(response => {
        // handle success
        this.setState({
          data: response.data.data,
          dataSuccess: true,
        });
      })
      .catch(error => {
        // handle error
        console.log(error);
        this.setState({
          error: true,
        });
      });
  };

  render() {
    if (this.state.dataSuccess) {
      return (
        <View style={styles.viewMain}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={this.state.data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => (
              <Article
                onPress={data =>
                  this.props.navigation.navigate('NewsContents', { data })
                }
                type={2}
                data={item}
              />
            )}
          />
        </View>
      );
    }
    if (this.state.error) {
      return (
        <View style={styles.viewMain}>
          <EmptyComponent />
        </View>
      );
    }
    return null;
  }
}

export default MatchVideo;
