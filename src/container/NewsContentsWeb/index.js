import React, { PureComponent } from 'react';
import { View, TouchableOpacity, WebView } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Header from '../../components/Header';
import LanguageVI from '../../constants/Language';
import { ColorTheme } from '../../themes';
import styles from './styles';
import IconBackCustom from '../../components/IconBackCustom';

class NewsContentsWeb extends PureComponent {
  state = {
    data_preRender: this.props.navigation.getParam('data'),
  };

  render() {
    return (
      <View style={styles.container}>
        <Header
          title={LanguageVI.headerNews}
          rightHeader
          icon={
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <IconBackCustom />
              {/* <Icon name="md-arrow-back" size={22} color={ColorTheme.Teal900} /> */}
            </TouchableOpacity>
          }
        />
        <WebView
          source={{
            uri: this.state.data_preRender.source_link,
          }}
        />
      </View>
    );
  }
}

export default NewsContentsWeb;
