import React, { PureComponent } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { CheckBox } from 'react-native-elements';

import Icon from 'react-native-vector-icons/Ionicons';
import LanguageVI from '../../constants/Language';
import Header from '../../components/Header';
import styles from './styles';
import { ColorTheme } from '../../themes';
import IconBackCustom from '../../components/IconBackCustom';

const CHECKED = 'checked';
const CHECKED1 = 'chec';

class SettingAppLanguage extends PureComponent {
  state = {
    checked: false,
    checked1: true,
  };

  // xet hien an nut checkbox...................................

  onPressSelectChecke = type => {
    if (type === CHECKED) {
      return this.setState({
        checked: !this.state.checked,
        checked2: false,
        checked1: false,
      });
    }

    if (type === CHECKED1) {
      return this.setState({
        checked1: !this.state.checked1,
        checked2: false,
        checked: false,
      });
    }
  };

  rowChecked = (text, checked, selectChecked) => {
    return (
      <CheckBox
        title={text}
        checkedIcon="dot-circle-o"
        uncheckedIcon="circle-o"
        checked={checked}
        containerStyle={styles.stylecontainer}
        onPress={() => this.onPressSelectChecke(selectChecked)}
        textStyle={styles.txt_checkBox}
      />
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Header
          title={LanguageVI.AppLanguage}
          rightHeader
          icon={
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <IconBackCustom />
              {/* <Icon name="md-arrow-back" size={22} color={ColorTheme.Teal900} /> */}
            </TouchableOpacity>
          }
          // elevation={true}
        />
        <View>
          {this.rowChecked(
            LanguageVI.AppLanguageDefault,
            this.state.checked,
            CHECKED,
          )}
        </View>

        <View>
          {this.rowChecked(
            LanguageVI.AppLanguageEnglish,
            this.state.checked1,
            CHECKED1,
          )}
        </View>
      </View>
    );
  }
}

export default SettingAppLanguage;
