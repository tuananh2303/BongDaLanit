import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  stylecontainer: {
    borderColor: ColorTheme.white,
    backgroundColor: ColorTheme.white,
  },
  txt_checkBox: {
    fontSize: FontSize.medium,
    fontWeight: '100',
    color: ColorTheme.black,
  },
});
