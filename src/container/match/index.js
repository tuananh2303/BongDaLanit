import React, { PureComponent } from 'react';
import {
  View,
  Text,
  Image,
  Platform,
  TouchableOpacity,
  FlatList,
  Dimensions,
  ScrollView,
} from 'react-native';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import Axios from 'axios';
import HeaderImageScrollView, {
  TriggeringView,
} from 'react-native-image-header-scroll-view';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';

import { Images, ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';
import styles from './styles';

import HeaderScore from '../../components/HeaderScore';
import HeaderTeam from '../../components/HeaderTeam';
import HeaderBubbleWinLose from '../../components/HeaderBubbleWinLose';
import LanguageVI from '../../constants/Language';

import MatchInfo from '../matchInfo';
import MatchTable from '../matchTable';
import MatchLineup from '../matchLineup';
import MatchTimer from '../matchTimer';
import MatchVideo from '../matchVideo';

import { URL_MATCH } from '../../constants/URL';
import { isIphoneX } from '../../utilities/device';

const VIDEO = 'video';
const INFO = 'info';
const TIMER = 'timer';
const TABLE = 'table';
const LINEUP = 'lineup';

class Match extends PureComponent {
  state = {
    tab: TIMER,
    dataNavigation: this.props.navigation.getParam('data', 1336),
    data: [],
    error: false,
    dataSuccess: false,
  };

  componentDidMount() {
    this.getData(this.state.dataNavigation);
  }

  getData = id => {
    Axios.get(URL_MATCH + id)
      .then(response => {
        console.log(response);

        this.setState({
          data: response.data.data,
          dataSuccess: true,
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({
          error,
          data: [],
        });
      });
  };

  componentHeaderInfo = data => {
    let team1 = '';
    let team2 = '';

    // 1 => Home or Away (0: Home, 1: Away),
    if (data[1] == 0) {
      team1 = data[3];
    }
    if (data[1] == 1) {
      team2 = data[3];
    }

    return (
      <View style={styles.componentHeaderInfo}>
        <View style={styles.componentHeaderInfoViewLeft}>
          <Text style={styles.componentHeaderInfoText}>{team1}</Text>
        </View>
        <View style={styles.componentHeaderInfoViewMiddle}>
          <Text style={styles.componentHeaderInfoText}>
            {data[0]}
            {"'"}
          </Text>
        </View>
        <View style={styles.componentHeaderInfoViewRight}>
          <Text style={styles.componentHeaderInfoText}>{team2}</Text>
        </View>
      </View>
      // null
    );
  };

  renderHeaderInfo = data => {
    const dataToHeader = [];
    if (data.length == 0) {
      console.log(data);
      return null;
    }

    data.map(item => {
      // const EVENT_GOAL = 1; // Sút vào
      // const EVENT_PENALTY_SCORED = 7; // Penalty đá vào
      // const EVENT_OWN_GOAL = 8; // Phản lưới nhà
      // const EVENT_ERROR_LEAD_TO_GOAL = 17; // Lỗi dẫn đến bàn thắng
      if (item[6] == 1 || item[6] == 7 || item[6] == 8 || item[6] == 17) {
        dataToHeader.push(item);
      }
    });

    // console.log('renderHeaderInfo', data);
    console.log(dataToHeader);

    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={dataToHeader}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => this.componentHeaderInfo(item)}
      />
    );
  };

  getRandomImage = random => {
    if (random == 1) {
      return Images.stadium;
    }
    if (random == 2) {
      return Images.stadium2;
    }
    if (random == 3) {
      return Images.stadium3;
    }
    if (random == 4) {
      return Images.stadium4;
    }
    if (random == 5) {
      return Images.stadium5;
    }
  };

  renderHeaderImage = () => {
    return (
      <Image
        source={this.getRandomImage(Math.floor(Math.random() * 5 + 1))}
        style={[
          styles.HeaderImage,
          {
            height: this.getHeight(),
          },
        ]}
        resizeMode="cover"
      />
    );
  };

  renderForeground = data => {
    if (data != null) {
      return (
        <View style={styles.ViewGroundMain}>
          <View style={styles.ViewGroundTop}>
            <HeaderTeam
              logoTeam={this.state.data.teams[2]}
              teamName={this.state.data.teams[1]}
            />
            <HeaderScore
              scoreLeft={data.overview[10]}
              scoreRight={data.overview[11]}
              date={data.overview[6]}
            />
            <HeaderTeam
              logoTeam={this.state.data.teams[5]}
              teamName={this.state.data.teams[4]}
            />
          </View>
          <View style={styles.ViewGroundBubble}>
            {this.renderHeaderInfo(data.events)}
          </View>
        </View>
      );
    }
    return null;
  };

  renderTouchableFixedForeground = () => {
    return (
      <View
        style={[styles.ViewFixGround, isIphoneX() ? { marginTop: 20 } : null]}
      >
        <View style={styles.ViewFixGroundLeft}>
          <TouchableOpacity
            style={styles.ButtonHeader}
            onPress={() => this.props.navigation.goBack()}
          >
            <Icon name="md-arrow-back" size={28} color={ColorTheme.white} />
          </TouchableOpacity>
        </View>
        {/* <TouchableOpacity style={styles.ButtonHeader}>
          <Icon name="md-star-outline" size={28} color={ColorTheme.white} />
        </TouchableOpacity> */}
      </View>
    );
  };

  renderContent = condition => {
    if (this.state.dataSuccess) {
      if (condition === TIMER) {
        return (
          <MatchTimer
            // dataNavigation={this.state.dataNavigation}
            data={this.state.data}
            onPressTeam={() => this.props.navigation.navigate('Team')}
            onPressMatch={data => {
              this.props.navigation.navigate('Match', { data });
              this.getData(data);
            }}
            onPressPlayer={() => this.props.navigation.navigate('Player')}
          />
          // null
        );
      }
      if (condition === LINEUP) {
        return (
          <MatchLineup
            data={this.state.data}
            // onPressTeam={() => this.props.navigation.navigate('Team')}
            // onPressMatch={() => this.props.navigation.navigate('Match')}
            onPressPlayer={() => {}}
          />
        );
      }
      if (condition === INFO) {
        return (
          <MatchInfo
            data={this.state.data}
            onPressTeam={() => this.props.navigation.navigate('Team')}
            onPressMatch={() => this.props.navigation.navigate('Match')}
            onPressPlayer={() => this.props.navigation.navigate('Player')}
          />
        );
      }
      if (condition === VIDEO) {
        return <MatchVideo navigation={this.props.navigation} />;
      }
      if (condition === TABLE) {
        return (
          <MatchTable
            data={this.state.data}
            onPressTeam={() => this.props.navigation.navigate('Team')}
            onPressMatch={() => this.props.navigation.navigate('Match')}
            onPressPlayer={() => this.props.navigation.navigate('Player')}
          />
        );
      }
    }

    return null;
  };

  getHeight = () => {
    // lấy tổng số lượng bàn thắng để render ra chiều dài của image
    let numItem = 0;

    if (this.state.dataSuccess) {
      const team1 =
        this.state.data.overview[10] == null
          ? 0
          : parseInt(this.state.data.overview[10], 10);

      const team2 =
        this.state.data.overview[11] == null
          ? 0
          : parseInt(this.state.data.overview[11], 10);
      // console.log(team1, team2);
      numItem = team1 + team2;
    }
    return (150 + 20 * numItem) * d.ratioH;
  };

  renderTabbar = (tabName, iconName) => {
    return (
      <TouchableOpacity
        style={this.state.tab == tabName ? styles.tabActive : styles.tab}
        onPress={() => this.setState({ tab: tabName })}
      >
        <Icon
          name={iconName}
          style={this.state.tab == tabName ? styles.iconActive : styles.icon}
          size={22}
        />
      </TouchableOpacity>
    );
  };

  getColor = () => {
    if (this.props.settingReducer.data.theme == 2) {
      return ColorTheme.Teal900;
    }
    if (this.props.settingReducer.data.theme == 3) {
      return ColorTheme.blue800;
    }
    if (this.props.settingReducer.data.theme == 4) {
      return ColorTheme.black;
    }
    if (this.props.settingReducer.data.theme == 5) {
      return ColorTheme.purple700;
    }

    return ColorTheme.Teal900;
  };

  render() {
    return (
      <View style={styles.viewMain}>
        <HeaderImageScrollView
          maxHeight={this.getHeight()}
          minHeight={Platform.OS === 'ios' ? 70 * d.ratioH : 50 * d.ratioH}
          minOverlayOpacity={0.4}
          maxOverlayOpacity={0.9}
          overlayColor={this.getColor()}
          renderHeader={() => this.renderHeaderImage()}
          fadeOutForeground={true}
          renderForeground={() =>
            this.renderForeground(
              this.state.dataSuccess ? this.state.data : null,
            )
          }
          renderTouchableFixedForeground={() =>
            this.renderTouchableFixedForeground()
          }
          foregroundParallaxRatio={1}
        >
          <TriggeringView
            style={styles.content}
            onHide={() => console.log('onHide')}
          >
            {this.renderContent(this.state.tab)}
          </TriggeringView>
        </HeaderImageScrollView>

        <View style={styles.tabbar}>
          {this.renderTabbar(INFO, 'ios-menu')}
          {this.renderTabbar(TABLE, 'ios-list')}
          {this.renderTabbar(TIMER, 'ios-timer')}
          {this.renderTabbar(VIDEO, 'md-paper')}
          {this.renderTabbar(LINEUP, 'md-keypad')}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  settingReducer: state.settingReducer,
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Match);
