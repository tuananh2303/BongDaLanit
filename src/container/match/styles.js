import { StyleSheet, Platform } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  viewMain: {
    flex: 1,
    backgroundColor: ColorTheme.gray300,
  },

  content: {
    minHeight: d.windowSize.height - 45 * d.ratioH,
  },

  HeaderImage: {
    width: d.windowSize.width,
  },

  icon: {
    color: ColorTheme.gray500,
  },

  iconActive: {
    color: ColorTheme.black,
  },

  tabbar: {
    backgroundColor: ColorTheme.white,
    flexDirection: 'row',
    elevation: 5,
  },
  tab: {
    height: 45 * d.ratioH,
    width: d.windowSize.width / 5,
    alignItems: 'center',
    justifyContent: 'center',
  },

  tabActive: {
    height: 45 * d.ratioH,
    width: d.windowSize.width / 5,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopColor: ColorTheme.black,
    borderTopWidth: 1.2,
  },

  indicator: {
    backgroundColor: ColorTheme.black,
  },
  labelStyle: {
    fontSize: FontSize.medium,
    fontWeight: 'normal',
    color: ColorTheme.black,
    marginBottom: 5 * d.ratioH,
  },

  // renderTouchableFixedForeground

  ViewFixGround: {
    height: 50 * d.ratioH,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  ViewFixGroundLeft: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  ButtonHeader: {
    height: 50 * d.ratioH,
    width: 60 * d.ratioW,
    alignItems: 'center',
    justifyContent: 'center',
  },

  TeamName: {
    color: ColorTheme.white,
    fontSize: FontSize.large,
    fontWeight: 'bold',
  },

  // renderForeground

  ViewGroundMain: {
    position: 'absolute',
    top: 60 * d.ratioH,
  },

  ViewGroundTop: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  ViewGroundBubble: {
    marginTop: 15 * d.ratioH,
    width: d.windowSize.width,
    flexDirection: 'row',
    justifyContent: 'center',
  },

  componentHeaderInfo: {
    flexDirection: 'row',
    height: 20 * d.ratioH,
    alignItems: 'center',
    justifyContent: 'center',
  },

  componentHeaderInfoText: {
    color: ColorTheme.white,
    fontSize: FontSize.small,
    fontWeight: 'bold',
  },

  componentHeaderInfoViewLeft: {
    flex: 6,
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },

  componentHeaderInfoViewRight: {
    flex: 6,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  componentHeaderInfoViewMiddle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
