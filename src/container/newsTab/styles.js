import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  viewMain: {
    flex: 1,
    backgroundColor: ColorTheme.white,
  },
});
