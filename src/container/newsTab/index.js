import React, { PureComponent } from 'react';
import { View, FlatList } from 'react-native';
import Axios from 'axios';
import { connect } from 'react-redux';
// import Icon from 'react-native-vector-icons/Ionicons';

import Article from '../../components/Article';
import styles from './styles';

class NewsTab extends PureComponent {
  state = {
    pageNum: 1,
    isFetching: false,
    error: false,
    data: [],
    dataSuccess: false,
    firstTimeLoading: true,
  };

  componentDidMount() {
    // console.log('get ' + this.props.category);
    // this.getData();
  }

  getData = () => {
    this.setState({ isFetching: true });
    Axios.get(this.props.url + this.props.category + '/' + this.state.pageNum)
      .then(response => {
        // handle success
        this.setState({
          data: response.data.data,
          dataSuccess: true,
          isFetching: false,
        });
      })
      .catch(error => {
        // handle error
        console.log(error);
        this.setState({
          error: true,
        });
      });
  };

  loadMoreData = pageNum => {
    Axios.get(this.props.url + this.props.category + '/' + pageNum)
      .then(response => {
        // handle success
        this.setState({
          data: this.state.data.concat(response.data.data),
          dataSuccess: true,
        });
      })
      .catch(error => {
        // handle error
        console.log(error);
        this.setState({
          error: true,
        });
      });
  };

  render() {
    // console.log(this.state.data);

    if (
      this.props.dataNewsConfigTab.index == this.props.routeKey &&
      this.state.firstTimeLoading
    ) {
      // console.log('get ' + this.props.routeKey);

      this.getData();
      this.setState({
        firstTimeLoading: false,
      });
    }
    if (this.state.error) {
      return null;
    }
    if (this.state.dataSuccess) {
      return (
        <View style={styles.viewMain}>
          <FlatList
            refreshing={this.state.isFetching}
            onRefresh={() => this.getData()}
            showsVerticalScrollIndicator={false}
            data={this.state.data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => (
              <Article
                type={index % 6 === 0 ? 1 : 2}
                data={item}
                onPress={data => {
                  if (item.post_type == 'link') {
                    return this.props.navigation.navigate('NewsContentsWeb', {
                      data,
                    });
                  }
                  return this.props.navigation.navigate('NewsContents', {
                    data: data,
                  });
                }}
              />
            )}
            onEndReached={() => {
              this.loadMoreData(this.state.pageNum + 1);
              this.setState({
                pageNum: this.state.pageNum + 1,
              });
            }}
            onEndReachedThreshold={5}
            initialNumToRender={3}
          />
        </View>
      );
    }
    if (!this.state.dataSuccess) {
      return (
        <View style={styles.viewMain}>
          <Article type={11} />
          <Article type={22} />
          <Article type={22} />
        </View>
      );
    }
    return null;
  }
}

// export default NewsTab;

const mapStateToProps = state => ({
  dataNewsConfigTab: state.getNewsConfigTabReducers,
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewsTab);
