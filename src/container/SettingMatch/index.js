import React, { PureComponent } from 'react';
import { View, TouchableOpacity, BackHandler } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';

import LanguageVI from '../../constants/Language';
import Header from '../../components/Header';
import { ColorTheme } from '../../themes';
import Switch from '../../components/Switch';
import styles from './styles';
import {
  settingTimeToRefresh,
  settingEvent,
  settingNoti,
} from '../../redux/actions/settingAction';
import storeData from '../../utilities/AsynStorage';
import { SETTING_APP } from '../../constants/AsyncStorage';
import IconBackCustom from '../../components/IconBackCustom';

class SettingMatch extends PureComponent {
  state = {
    eventVal: this.props.settingReducer.data.hideShowEvent,
    notiVal: this.props.settingReducer.data.notification,
  };

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack();
    this.props.settingEvent(this.state.eventVal);
    this.props.settingNoti(this.state.notiVal);

    storeData(SETTING_APP, this.props.settingReducer.data);
    return true;
  };

  render() {
    return (
      <View style={styles.container}>
        <Header
          title={LanguageVI.MatchSetting}
          rightHeader
          icon={
            <TouchableOpacity onPress={() => this.handleBackPress()}>
              <IconBackCustom />
              {/* <Icon name="md-arrow-back" size={22} color={ColorTheme.Teal900} /> */}
            </TouchableOpacity>
          }
          elevation={true}
        />

        <View style={styles.row}>
          <View style={styles.rowsub}>
            <Switch
              icon="ios-eye"
              title={LanguageVI.ShowHideMatchEvent}
              value={this.state.eventVal}
              toggleSwitch1={() =>
                this.setState({
                  eventVal: !this.state.eventVal,
                })
              }
            />
          </View>
        </View>
        <View style={styles.row}>
          <View style={styles.rowsub}>
            <Switch
              icon="ios-notifications"
              title={LanguageVI.PushNotifications}
              value={this.state.notiVal}
              toggleSwitch1={() =>
                this.setState({
                  notiVal: !this.state.notiVal,
                })
              }
            />
          </View>
        </View>
      </View>
    );
  }
}

// export default SettingMatch;

const mapStateToProps = state => ({
  settingReducer: state.settingReducer,
});

const mapDispatchToProps = dispatch => ({
  settingTimeToRefresh: data => dispatch(settingTimeToRefresh(data)),
  settingEvent: data => dispatch(settingEvent(data)),
  settingNoti: data => dispatch(settingNoti(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SettingMatch);
