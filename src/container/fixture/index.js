import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

class Home extends PureComponent {
  state = {};

  render() {
    return (
      <View>
        <Text> fixture </Text>
        <Icon name="ios-planet" />
      </View>
    );
  }
}

export default Home;
