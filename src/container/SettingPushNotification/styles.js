import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    // height: 60 * d.ratioH,
    justifyContent: 'center',
    // elevation: 2,
    borderBottomColor: ColorTheme.gray500,
    borderBottomWidth: 0.25,
  },
  rowsub: {
    height: 50 * d.ratioH,
    justifyContent: 'center',
  },
  rightHeader: {
    flexDirection: 'row',
  },
  iconRight: {
    flex: 1,
    alignItems: 'center',
  },
  viewModal: {
    marginTop: 20 * d.ratioH,
    marginEnd: 20 * d.ratioW,
    backgroundColor: 'white',
    marginLeft: 100,
    elevation: 3,
  },
  subTitle: {
    height: 40 * d.ratioH,
    justifyContent: 'center',
    paddingLeft: 20 * d.ratioW,
  },
  containerModal1: {
    flex: 1,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    justifyContent: 'center',
  },
  viewModal1: {
    flex: 1,
    backgroundColor: ColorTheme.grayModalTransparent,
  },
  viewSubModal1: {
    backgroundColor: ColorTheme.white,
    margin: 20,
    // height: 200,
  },
  viewScroll: {
    backgroundColor: 'white',
  },
  titleSubModal: {
    marginLeft: 20 * d.ratioW,
    height: 50 * d.ratioH,
    justifyContent: 'center',
  },
  txt_titleSubModal: {
    fontSize: FontSize.large,
    color: ColorTheme.black,
    fontWeight: '400',
  },
  subTime: {
    flexDirection: 'row',
  },
  subTimeRadio: {
    flex: 1,
  },
  subTimeTitle: {
    flex: 3,
  },
  txt_subTitle: {
    color: ColorTheme.black,
    fontWeight: '400',
  },
  stylecontainer: {
    borderColor: ColorTheme.white,
    backgroundColor: ColorTheme.white,
  },
  txt_checkBox: {
    fontSize: FontSize.medium,
    fontWeight: '100',
    color: ColorTheme.black,
  },
});
