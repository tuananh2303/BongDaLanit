import React, { PureComponent } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  TouchableOpacity,
  Modal,
  ScrollView,
} from 'react-native';
import { CheckBox } from 'react-native-elements';

import Icon from 'react-native-vector-icons/Ionicons';
import LanguageVI from '../../constants/Language';
import dataTopScore from './dataTopScore';
import HeaderPushNotification from '../../components/HeaderPushNotification';
import SettingPushNotifications from '../../components/SettingPushNotifications';
import { ColorTheme } from '../../themes';
import styles from './styles';
import IconBackCustom from '../../components/IconBackCustom';

const CHECKED = 'checked';
const CHECKED1 = 'checked1';
const CHECKED2 = 'checked2';
const CHECKED3 = 'checked3';
const CHECKED4 = 'checked4';
const DONE = 'done';
const DONE1 = 'done1';

class SettingPushNotification extends PureComponent {
  state = {
    modalVisible1: false,
    type: '',
    checked: true,
    checked1: false,
    checked2: false,
    checked3: false,
    checked4: false,
  };
  // xet show modal

  showModal1 = () => {
    this.setState({
      modalVisible1: true,
      type: DONE,
    });
  };

  showModal2 = () => {
    this.setState({
      modalVisible1: true,
      type: DONE1,
    });
  };

  hideModal1 = () => {
    this.setState({ modalVisible1: false });
  };

  // xet an modal khi click tuy chon thong bao phai xet se hide modal

  handleButton = screen => {
    this.setState({ modalVisible1: false });
    // this.props.navigation.navigate('SettingPushNotificationSetting');
    this.props.navigation.navigate(screen);
  };

  // xet an modal khi click tuy chon thong bao trai se hide modal

  handleChecked = type => {
    // this.setState({ modalVisible1: false });
    this.onPressSelectChecke(type);
  };

  // xet checkbox modal trai

  onPressSelectChecke = type => {
    if (type === CHECKED) {
      return this.setState({
        checked: !this.state.checked,
        checked2: false,
        checked1: false,
        checked3: false,
        checked4: false,
      });
    }

    if (type === CHECKED1) {
      return this.setState({
        checked1: !this.state.checked1,
        checked2: false,
        checked: false,
        checked3: false,
        checked4: false,
      });
    }

    if (type === CHECKED2) {
      return this.setState({
        checked2: !this.state.checked2,
        checked: false,
        checked1: false,
        checked3: false,
        checked4: false,
      });
    }

    if (type === CHECKED3) {
      return this.setState({
        checked3: !this.state.checked3,
        checked: false,
        checked1: false,
        checked2: false,
        checked4: false,
      });
    }

    if (type === CHECKED4) {
      return this.setState({
        checked4: !this.state.checked4,
        checked: false,
        checked1: false,
        checked2: false,
        checked3: false,
      });
    }
  };

  subTitle = title => {
    return (
      <View style={styles.subTitle}>
        <Text style={styles.txt_subTitle}>{title}</Text>
      </View>
    );
  };

  // 1 hang checkbox

  rowChecked = (text, checked, selectChecked) => {
    return (
      <CheckBox
        title={text}
        checkedIcon="dot-circle-o"
        uncheckedIcon="circle-o"
        checked={checked}
        containerStyle={styles.stylecontainer}
        onPress={() => this.handleChecked(selectChecked)}
        textStyle={styles.txt_checkBox}
      />
    );
  };

  // xet hien modal trai hay phai

  rowModal = type => {
    if (type === DONE) {
      return (
        <View style={styles.viewSubModal1}>
          <View style={styles.titleSubModal}>
            <Text style={styles.txt_titleSubModal}>
              {LanguageVI.PushNotifications}
            </Text>
          </View>

          <View>
            {this.rowChecked(LanguageVI.On, this.state.checked, 'checked')}
          </View>

          <View>
            {this.rowChecked(LanguageVI.Off, this.state.checked1, 'checked1')}
          </View>

          <View>
            {this.rowChecked(
              LanguageVI.MuteFor2Hours,
              this.state.checked2,
              'checked2',
            )}
          </View>

          <View>
            {this.rowChecked(
              LanguageVI.MuteFor4Hours,
              this.state.checked3,
              'checked3',
            )}
          </View>

          <View>
            {this.rowChecked(
              LanguageVI.MuteUntill08,
              this.state.checked4,
              'checked4',
            )}
          </View>
        </View>
      );
    }

    if (type === DONE1) {
      return (
        <View style={styles.viewModal}>
          <View>
            <TouchableOpacity
              onPress={() =>
                this.handleButton('SettingPushNotificationSetting')
              }
            >
              {this.subTitle(LanguageVI.NotificationSettings)}
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.handleButton('SettingPushNotificationEditDeffault')
              }
            >
              {this.subTitle(LanguageVI.EditDefaultNotifications)}
            </TouchableOpacity>

            <TouchableOpacity>
              {this.subTitle(LanguageVI.RemoveAllNotifications)}
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.handleButton('SettingPushNotificationTroubleshoot')
              }
            >
              {this.subTitle(LanguageVI.TroubleshootNotifications)}
            </TouchableOpacity>
          </View>
        </View>
      );
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <HeaderPushNotification
          title={LanguageVI.PushNotifications}
          rightHeader={
            <View style={styles.rightHeader}>
              <TouchableOpacity
                onPress={() => {
                  this.showModal1();
                }}
                style={styles.iconRight}
              >
                <Icon
                  name="ios-notifications"
                  size={22}
                  color={ColorTheme.Teal900}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  this.showModal2();
                }}
                style={styles.iconRight}
              >
                <Icon name="md-more" size={22} color={ColorTheme.Teal900} />
              </TouchableOpacity>
            </View>
          }
          icon={
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <IconBackCustom />
              {/* <Icon name="md-arrow-back" size={22} color={ColorTheme.Teal900} /> */}
            </TouchableOpacity>
          }
          elevation={true}
        />
        <View style={styles.containerModal1}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.modalVisible1}
            animationType={'fade'}
            // onShow={this.hideModal}
            onRequestClose={this.hideModal1}
          >
            <TouchableHighlight
              style={styles.viewModal1}
              onPress={() => {
                this.hideModal1();
              }}
            >
              {this.rowModal(this.state.type)}
            </TouchableHighlight>
          </Modal>
        </View>

        <ScrollView style={styles.viewScroll}>
          <SettingPushNotifications
            data={dataTopScore}
            onPressTeamNotification={() =>
              this.props.navigation.navigate('SettingPushNotificationTeam')
            }
          />
        </ScrollView>
      </View>
    );
  }
}

export default SettingPushNotification;
