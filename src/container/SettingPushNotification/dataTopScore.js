const dataTopScore = [
  {
    id: 1,
    title: 'Averil Skaid',
    sub_title: 'Averil',
    goals: 8,
  },
  {
    id: 2,
    title: 'Town Bontine',
    sub_title: 'Town',
    goals: 99,
  },
  {
    id: 3,
    title: 'Emlyn Arran',
    sub_title: 'Emlyn',
    goals: 56,
  },
  {
    id: 4,
    title: 'Sanford Clevely',
    sub_title: 'Sanford',
    goals: 62,
  },
  {
    id: 5,
    title: 'Ulick Havoc',
    sub_title: 'Ulick',
    goals: 8,
  },
  {
    id: 6,
    title: 'Vlad Faux',
    sub_title: 'Vlad',
    goals: 63,
  },
  {
    id: 7,
    title: 'Raoul Wormald',
    sub_title: 'Raoul',
    goals: 60,
  },
  {
    id: 8,
    title: 'Chuck Reading',
    sub_title: 'Chuck',
    goals: 90,
  },
  {
    id: 9,
    title: 'Wyn Dietsche',
    sub_title: 'Wyn',
    goals: 34,
  },
 
];
export default dataTopScore;
