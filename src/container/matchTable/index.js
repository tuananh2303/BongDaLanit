import React, { PureComponent } from 'react';
import { View } from 'react-native';
import TeamTableScroce from '../../components/TeamTableScroce';
import styles from './styles';
import EmptyComponent from '../../components/EmptyComponent';

class MatchTable extends PureComponent {
  state = {
    data: [],
    dataSuccess: false,
  };

  // standing => [
  // 0 => team_id,
  // 1 => Ten đoi
  // 2 => rank,
  // 3 => matches, (So trận)
  // 4 => wins, (So tran thang)
  // 5 => draws, (So tran hoa)
  // 6 => losses, (So tran thua)
  // 7 => So ban thang
  // 8 => So ban thua
  // 9 =>
  // 10 => points, (Điem)

  componentDidMount() {
    // console.log(this.props.data.standing);
    const dataArr = [];
    this.props.data.standing.map(item => {
      const itemPush = {
        id: item[0],
        rank: item[2],
        team_name: item[1],
        status: 2,
        so_tran: item[3],
        hieu_so: parseInt(item[7], 10) - parseInt(item[8], 10),
        diem: item[10],
      };
      dataArr.push(itemPush);
    });
    this.setState({
      data: dataArr,
      dataSuccess: true,
    });
  }

  render() {
    if (this.state.dataSuccess) {
      // console.log(this.state.data);

      if (this.state.data.length == 0) {
        return (
          <View style={styles.ViewMain}>
            <EmptyComponent />
          </View>
        );
      }
      return (
        <View style={styles.ViewMain}>
          <TeamTableScroce
            data={{
              leagh: '',
              data: this.state.data,
            }}
            teamMain1={this.props.data.teams[0]}
            teamMain2={this.props.data.teams[3]}
            onPressTeam={() => this.props.onPressTeam()}
          />
        </View>
      );
    }
    return null;
  }
}

export default MatchTable;
