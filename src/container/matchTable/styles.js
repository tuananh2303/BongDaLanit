import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  ViewMain: {
    backgroundColor: ColorTheme.white2,
    // backgroundColor: 'red',
    flex: 1,
  },
});
