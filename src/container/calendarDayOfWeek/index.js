import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
// import firebase from 'react-native-firebase';
import { connect } from 'react-redux';
import Axios from 'axios';
import Icon from 'react-native-vector-icons/Ionicons';
import Card from '../../components/Card';
import { URL_LIVESCORE, URL_CALENDAR } from '../../constants/URL';
import HomeDataFake from './HomeDataFake';

// const database = firebase.database();

class CalendarDayOfWeek extends PureComponent {
  state = {
    data: [],
    dataSuccess: false,
    error: false,
    dataTeam: [],
    firstTimeLoading: true,
  };

  getMatchesByLeagueID = (id, dataInput) => {
    const result = [];
    Object.values(dataInput.matches).map(item => {
      if (parseInt(item[3], 10) == parseInt(id, 10)) {
        // array [3] trong match la id cua league

        const itemToArr = {
          match_id: item[0], // array[0] la id cua tran dau
          match_date: item[6], // array[6] la thoi gian bat dau tran dau
          status: item[9], // status doi bong ow vi tri thu [9]
          team1_name: dataInput.teams[item[4]][1], // trong array[4] trong matches la id cua team , array[1] trong team la team_name
          team2_name: dataInput.teams[item[5]][1],
          score1: item[10], // ty so doi 1 la array[10]
          score2: item[11], // ty so doi 2 la array[11]
          team1_id: item[4],
          team2_id: item[5],
          team1_image: dataInput.teams[item[4]][2], // trong array[4] trong matches la id cua team , array[2] trong team la anh
          team2_image: dataInput.teams[item[5]][2],
        };
        result.push(itemToArr);
      }
    });

    return result;
  };

  componentDidMount() {}

  getData = () => {
    console.log(this.props.dayInRange + 'getData');

    Axios.get(URL_CALENDAR + this.props.dayInRange)
      .then(response => {
        // handle success
        console.log(response);
        // if (this.props.dayInRange == 3) {
        //   database.ref('/livescore/calendar').set(response.data);
        // }
        const leaguesResponse = [];
        Object.values(response.data.data.leagues).map(item => {
          // // console.log(item);
          const itemToArr = {
            id: item[0],
            team_name: item[1],
            image: item[11],
            data: this.getMatchesByLeagueID(item[0], response.data.data),
          };
          leaguesResponse.push(itemToArr);
        });
        // console.log(leaguesResponse);

        this.setState({
          dataTeam: response.data.data.teams,
          data: leaguesResponse,
          dataSuccess: true,
        });
      })
      .catch(error => {
        console.log(error);

        this.setState({
          error: error,
          data: [],
        });
      });
  };

  getDataFromFirebase = () => {};

  render() {
    if (
      this.props.calendarReducer.day == this.props.dayInRange &&
      this.state.firstTimeLoading
    ) {
      this.getData();
      this.setState({
        firstTimeLoading: false,
      });
    }
    if (this.state.dataSuccess) {
      return (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={this.state.data}
          keyExtractor={(item, index) => index.toString()}
          initialNumToRender={1}
          renderItem={({ item, index }) => (
            <Card
              data={item}
              onPressTeam={() => {}}
              onPressMatch={data => {
                // console.log(id);
                this.props.navigation.navigate('Match', {
                  data,
                });
              }}
            />
          )}
        />
      );
    }
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={HomeDataFake}
        keyExtractor={(item, index) => index.toString()}
        initialNumToRender={1}
        renderItem={({ item, index }) => (
          <Card data={item} onPressTeam={() => {}} onPressMatch={() => {}} />
        )}
      />
    );
  }
}

const mapStateToProps = state => ({
  calendarReducer: state.calendarReducer,

  dataUserTeamLeagueLike: state.getUserTeamLeagueReducers,
});

const mapDispatchToProps = dispatch => ({
  // fetchDatagetUserTeamLeague: userID =>
  //   dispatch(fetchDatagetUserTeamLeague(userID)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CalendarDayOfWeek);
