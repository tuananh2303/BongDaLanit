import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  AsyncStorage,
} from 'react-native';
// import {
//   GoogleSignin,
//   GoogleSigninButton,
//   statusCodes,
// } from 'react-native-google-signin';
import Axios from 'axios';
import { connect } from 'react-redux';
import { LoginButton, AccessToken, LoginManager } from 'react-native-fbsdk';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Images } from '../../themes';
import LanguageVI from '../../constants/Language';
import { LOGIN_REF } from '../../constants/AsyncStorage';
import { URL_GET_FB_INFO, URL_SIGNIN } from '../../constants/URL';
import { addUserInfo, userInfo } from '../../redux/actions/userAction';
// GoogleSignin.configure();

// const database = firebase.database();

class Login extends PureComponent {
  state = {};

  componentDidMount() {}

  facebookLogin = () => {
    // LoginManager.logOut();
    LoginManager.logInWithReadPermissions(['public_profile']).then(
      result => {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          // console.log('Login success with permissions: ' + result.toString());
          AccessToken.getCurrentAccessToken().then(data => {
            console.log(data.accessToken.toString());
            this.getFacebookInfo(data.accessToken.toString());
          });
        }
      },
      error => {
        console.log('Login fail with error: ' + error);
      },
    );
  };

  getFacebookInfo = async accessToken => {
    await Axios.get(URL_GET_FB_INFO + accessToken)
      .then(response => {
        // handle success
        console.log(response);
        this.pushDataFacebookToServer(
          response.data.id,
          response.data.name,
          accessToken,
        );
      })
      .catch(error => {
        // handle error
        console.log(error);
      });
  };

  pushDataFacebookToServer = async (fbid, name, token) => {
    const dataFB = {
      fbid,
      name,
      token,
    };
    await Axios.post(URL_SIGNIN, dataFB)
      .then(response => {
        console.log(response);
        if (response.data.status == 'success') {
          AsyncStorage.setItem(LOGIN_REF, JSON.stringify(dataFB));
          console.log(dataFB);
          this.props.addUserInfo(dataFB);
          this.props.userInfo(response.data);

          this.props.navigation.navigate('AppStack');
        }
      })
      .catch(error => {
        // console.log(error);
      });
  };

  skipLogin = () => {
    this.props.navigation.navigate('AppStack');
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.top}>
          <Image style={styles.logo} source={Images.startUpImage} />
        </View>
        <View style={styles.bottom}>
          <View style={styles.google}>
            {/* <View>
              <Image style={styles.logoGoogle} source={Images.iconGoogle} />
            </View>
            <View>
              <Text style={styles.txt_google}>Continue with Google</Text>
            </View> */}
          </View>

          <TouchableOpacity
            style={styles.fb}
            onPress={() => this.facebookLogin()}
          >
            <Icon name="logo-facebook" size={30} color="white" />
            <Text style={styles.txt_fb}> Continue with Facebook</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.skipBtn}
            onPress={() => this.skipLogin()}
          >
            <Text style={styles.skipText}>{LanguageVI.skip}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  userReducer: state.userReducer,
});

const mapDispatchToProps = dispatch => ({
  addUserInfo: data => dispatch(addUserInfo(data)),
  userInfo: data => dispatch(userInfo(data)),
});
// export default Login;

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);

// pushDataDeviceToFireBase = async uniqueId => {
//   await database.ref('user/device' + uniqueId + '/user').set({
//     id: uniqueId,
//     name: uniqueId,
//     avatar: null,
//   });

//   this.loginRefFireBase('device' + uniqueId);
// };

// loginRefFireBase = async loginRefFireBase => {
//   try {
//     await AsyncStorage.setItem(LOGIN_REF_FIREBASE, loginRefFireBase);
//     await database
//       .ref('user/' + loginRefFireBase + '/favorite')
//       .on('value', snapShot => {
//         console.log(snapShot.val());

//         if (snapShot.val() == null) {
//           this.props.navigation.navigate('FirstSetting');
//         } else {
//           this.props.navigation.navigate('AppStack');
//         }
//       });
//   } catch (error) {
//     console.log(error);
//   }
// };

// pushDataFacebookToFireBase = async data => {
//   await database.ref('user/facebook' + data.id + '/user').set({
//     id: data.id,
//     name: data.name,
//     avatar:
//       'http://graph.facebook.com/' +
//       data.id +
//       '/picture?width=500&height=500',
//   });

//   this.loginRefFireBase('facebook' + data.id);
// };

// pushDataGoogleToFireBase = async userInfo => {
//   await database
//     .ref('user/google' + userInfo.user.id + '/google')
//     .set(userInfo);

//   await database.ref('user/google' + userInfo.user.id + '/user').set({
//     id: userInfo.user.id,
//     name: userInfo.user.name,
//     avatar: userInfo.user.photo,
//   });
//   this.loginRefFireBase('google' + userInfo.user.id);
// };

// googleLogin = async () => {
//   try {
//     await GoogleSignin.hasPlayServices();
//     const userInfo = await GoogleSignin.signIn();
//     this.pushDataGoogleToFireBase(userInfo);
//   } catch (error) {
//     if (error.code === statusCodes.SIGN_IN_CANCELLED) {
//       // sign in was cancelled
//       console.log('cancelled');
//     }
//     if (error.code === statusCodes.IN_PROGRESS) {
//       // operation in progress already
//       console.log('in progress');
//     }
//     if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
//       // PLAY_SERVICES_NOT_AVAILABLE
//       console.log('play services not available or outdated');
//     } else {
//       console.log('Something went wrong', error.toString());
//     }
//   }
// };

// isSignedIn = async () => {
//   const isSignedIn = await GoogleSignin.isSignedIn();
//   this.setState({ isLoginScreenPresented: !isSignedIn });
// };
