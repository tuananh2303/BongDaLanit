import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  top: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: d.windowSize.width,
    height: 200 * d.ratioH,
  },
  bottom: {
    flex: 1,
    // justifyContent: 'flex-end',
  },
  google: {
    height: 50 * d.ratioH,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    margin: 40,
    marginTop: 100 * d.ratioH,
    marginBottom: 10 * d.ratioH,
    borderRadius: 10,
    // borderWidth: 1,
    // borderColor: ColorTheme.gray300,
  },
  logoGoogle: {
    width: 30 * d.ratioW,
    height: 30 * d.ratioH,
  },
  txt_google: {
    fontSize: FontSize.medium,
    fontWeight: '500',
    color: ColorTheme.black,
  },
  fb: {
    height: 50 * d.ratioH,
    backgroundColor: ColorTheme.indigo700,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    margin: 40,
    marginBottom: 10 * d.ratioH,
    marginTop: 10 * d.ratioH,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: ColorTheme.gray300,
  },
  txt_fb: {
    fontSize: FontSize.medium,
    fontWeight: '500',
    color: ColorTheme.white,
  },

  skipBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10 * d.ratioH,
  },

  skipText: {
    color: ColorTheme.black,
    fontSize: FontSize.medium,
    textDecorationLine: 'underline',
  },
});
