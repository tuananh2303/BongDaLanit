import React, { PureComponent } from 'react';
import {
  Text,
  View,
  Slider,
  TouchableOpacity,
  BackHandler,
} from 'react-native';
import { connect } from 'react-redux';

import Icon from 'react-native-vector-icons/Ionicons';
import LanguageVI from '../../constants/Language';
import Header from '../../components/Header';
import Switch from '../../components/Switch';
import { ColorTheme } from '../../themes';
import { settingTimeToRefresh } from '../../redux/actions/settingAction';

import storeData from '../../utilities/AsynStorage';
import styles from './styles';
import { SETTING_APP } from '../../constants/AsyncStorage';
import IconBackCustom from '../../components/IconBackCustom';

class SettingData extends PureComponent {
  state = {
    value: this.props.settingReducer.data.timeToRefresh,
    SwitchOnValueHolder: false,
  };

  componentDidMount() {
    console.log(this.props.settingReducer);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack();
    this.props.settingTimeToRefresh(this.state.value);
    storeData(SETTING_APP, this.props.settingReducer.data);
    return true;
  };

  toggleSwitch = () => {
    this.setState({
      SwitchOnValueHolder: !this.state.SwitchOnValueHolder,
    });
  };

  rowToggleSwitch = (icon, title, stateValue) => {
    return (
      <TouchableOpacity
        style={styles.rowsub}
        onPress={() => this.toggleSwitch()}
      >
        {/* {rowIcon(icon, title, stateValue)} */}
        <Switch
          icon={icon}
          title={title}
          value={stateValue}
          toggleSwitch1={() => this.toggleSwitch()}
        />
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Header
          title={LanguageVI.Data}
          rightHeader
          icon={
            <TouchableOpacity onPress={() => this.handleBackPress()}>
              <IconBackCustom />
              {/* <Icon name="md-arrow-back" size={22} color={ColorTheme.Teal900} /> */}
            </TouchableOpacity>
          }
          // elevation={true}
        />

        <View style={styles.rowTitle}>
          <View style={styles.titleShort}>
            <Text style={styles.txt_rowTitle}>{LanguageVI.SetTheTime}</Text>
          </View>

          <View style={styles.titleLong}>
            <View>
              <Text style={styles.txt_rowTitle}>
                {LanguageVI.DependingOnBattery}
              </Text>
            </View>
            <View>
              <Text style={styles.txt_rowTitle}>
                {LanguageVI.DependingOnBattery1}
              </Text>
            </View>
          </View>

          <View style={styles.viewSlider}>
            <View style={styles.titleSlider}>
              <View style={styles.titleSliderLeft}>
                <Text>{LanguageVI.Every}</Text>
              </View>
              <View style={styles.titleValue}>
                <Text>{this.state.value}</Text>
              </View>
              <View style={styles.titleSliderRight}>
                <Text>{LanguageVI.Seconds}</Text>
              </View>
            </View>

            <View style={styles.widthSlider}>
              <Slider
                step={10}
                minimumValue={10}
                maximumValue={120}
                value={this.state.value}
                onValueChange={val => {
                  this.setState({ value: val });
                }}
              />
            </View>
          </View>
          <View style={styles.status}>
            <View style={styles.statusLeft}>
              <Text style={styles.txt_status}>{LanguageVI.Fast}</Text>
              <Text>{LanguageVI.AddictFriendly}</Text>
            </View>
            <View style={styles.statusRight}>
              <Text style={styles.txt_status}>{LanguageVI.Slow}</Text>
              <Text>{LanguageVI.BatteryFriendly}</Text>
            </View>
          </View>
        </View>

        {/* <View style={styles.rowswitch}>
          {this.rowToggleSwitch(
            'md-image',
            LanguageVI.ShowPhotos,
            this.state.SwitchOnValueHolder,
          )}
        </View> */}
      </View>
    );
  }
}

// export default SettingData;

const mapStateToProps = state => ({
  settingReducer: state.settingReducer,
});

const mapDispatchToProps = dispatch => ({
  // addUserInfo: data => dispatch(addUserInfo(data)),
  // userInfo: data => dispatch(userInfo(data)),
  // settingInit: data => dispatch(settingInit(data)),
  settingTimeToRefresh: data => dispatch(settingTimeToRefresh(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SettingData);
