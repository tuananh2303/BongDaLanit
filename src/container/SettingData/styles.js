import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  rowswitch: {
    height: 50 * d.ratioH,
    justifyContent: 'center',
    // elevation: 2,
    borderBottomColor: ColorTheme.gray500,
    borderBottomWidth: 0.25,
    backgroundColor: 'white',
  },
  titleShort: {
    height: 50 * d.ratioH,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleLong: {
    height: 60 * d.ratioH,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowTitle: {
    marginBottom: 10 * d.ratioH,
    backgroundColor: 'white',
    borderBottomColor: ColorTheme.gray500,
    borderBottomWidth: 0.25,
    elevation: 2,
  },
  txt_rowTitle: {
    color: ColorTheme.black,
  },
  viewSlider: {
    height: 50 * d.ratioH,
    justifyContent: 'center',
    alignItems: 'center',
  },
  widthSlider: {
    width: 350 * d.ratioW,
    marginTop: 10 * d.ratioH,
  },
  titleSlider: {
    flexDirection: 'row',
  },
  titleSliderLeft: {
    // flex: 4,
    // alignItems: 'flex-end',
    marginRight: 5 * d.ratioW,
  },
  titleSliderRight: {
    // flex: 4,
    marginLeft: 5 * d.ratioW,
  },
  titleValue: {
    // flex: 1,
    alignItems: 'center',
  },
  status: {
    height: 60 * d.ratioH,
    flexDirection: 'row',
    paddingTop: 10 * d.ratioH,
  },
  statusLeft: {
    flex: 1,
    paddingLeft: 15 * d.ratioW,
  },
  statusRight: {
    flex: 1,
    alignItems: 'flex-end',
    paddingRight: 15 * d.ratioW,
  },
  txt_status: {
    fontSize: FontSize.medium,
    color: ColorTheme.black,
    fontWeight: '400',
  },
});
