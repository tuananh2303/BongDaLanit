import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import TopScoreGoals from '../../components/TopScoreGoals';
import TopScoreMins from '../../components/TopScoreMins';
import dataTopScore from './dataTopScore';
import dataTopScoremins from './dataTopScoremins';
import Header from '../../components/Header';
import LanguageVI from '../../constants/Language';
import styles from './styles';


class Home extends PureComponent {
  
  state = {
    index: 0,
    routes: [
      { key: 'goals', title: 'Goals' },
      { key: 'mins', title: 'Mins/Goal' },
    ],
  };

  _renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      tabStyle={styles.tab}
      labelStyle={styles.label}
    />
  );

  renTopScoreGoals = () => {
    return <TopScoreGoals data={dataTopScore} />;
  };

  renTopScoreMins = () => {
    return <TopScoreMins data={dataTopScoremins} />;
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header title={LanguageVI.top20} rightHeader />
        <TabView
          style={[styles.container, this.props.style]}
          navigationState={this.state}
          renderScene={SceneMap({
            goals: this.renTopScoreGoals,
            mins: this.renTopScoreMins,
          })}
          renderTabBar={this._renderTabBar}
          onIndexChange={index => {
            this.setState({ index });
          }}
          initialLayout={{
            height: 0,
            width: Dimensions.get('window').width,
          }}
        />
      </View>
    );
  }
}

export default Home;
