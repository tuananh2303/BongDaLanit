const dataTopScoremins = [
  {
    id: 1,
    title: 'Averil Skaid',
    sub_title: 'Averil',
    mins: 8,
  },
  {
    id: 2,
    title: 'Town Bontine',
    sub_title: 'Town',
    mins: 99,
  },
  {
    id: 3,
    title: 'Emlyn Arran',
    sub_title: 'Emlyn',
    mins: 56,
  },
  {
    id: 4,
    title: 'Sanford Clevely',
    sub_title: 'Sanford',
    mins: 62,
  },
  {
    id: 5,
    title: 'Ulick Havoc',
    sub_title: 'Ulick',
    mins: 8,
  },
  {
    id: 6,
    title: 'Vlad Faux',
    sub_title: 'Vlad',
    mins: 63,
  },
  {
    id: 7,
    title: 'Raoul Wormald',
    sub_title: 'Raoul',
    mins: 60,
  },
  {
    id: 8,
    title: 'Chuck Reading',
    sub_title: 'Chuck',
    mins: 90,
  },
  {
    id: 9,
    title: 'Wyn Dietsche',
    sub_title: 'Wyn',
    mins: 34,
  },
  {
    id: 10,
    title: 'Bernie Hanselmann',
    sub_title: 'Bernie',
    mins: 72,
  },
];
export default dataTopScoremins;
