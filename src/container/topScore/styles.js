import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  tabbar: {
    backgroundColor: ColorTheme.green,
  },
  tab: {
    width: d.windowSize.width / 2,
  },
  indicator: {
    backgroundColor: ColorTheme.white,
  },
  label: {
    fontSize: 12,
    color: ColorTheme.white,
  },
});
