import { StyleSheet, Platform } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  viewMain: {
    flex: 1,
    backgroundColor: ColorTheme.gray300,
  },

  content: {
    minHeight: d.windowSize.height - 45 * d.ratioH,
  },

  HeaderImage: {
    height: 180 * d.ratioH,
    width: d.windowSize.width,
  },

  tabbar: {
    backgroundColor: ColorTheme.white,
    flexDirection: 'row',
    elevation: 5,
  },
  tab: {
    height: 45 * d.ratioH,
    width: d.windowSize.width / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },

  tabActive: {
    height: 45 * d.ratioH,
    width: d.windowSize.width / 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopColor: ColorTheme.black,
    borderTopWidth: 1.2,
  },

  indicator: {
    backgroundColor: ColorTheme.black,
  },
  labelStyle: {
    fontSize: FontSize.medium,
    fontWeight: 'normal',
    color: ColorTheme.black,
    marginBottom: 5,
  },

  // renderTouchableFixedForeground

  ViewFixGround: {
    height: 60 * d.ratioH,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  ViewFixGroundLeft: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  ButtonHeader: {
    height: 60 * d.ratioH,
    width: 60 * d.ratioW,
    alignItems: 'center',
    justifyContent: 'center',
  },

  TeamName: {
    color: ColorTheme.white,
    fontSize: FontSize.large,
    fontWeight: 'bold',
  },

  // renderForeground

  ViewGroundMain: {
    position: 'absolute',
    top: 60 * d.ratioH,
  },

  ViewGroundTop: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  ViewGroundBubble: {
    marginTop: 5 * d.ratioH,
    width: d.windowSize.width,
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
