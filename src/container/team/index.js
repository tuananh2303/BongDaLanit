import React, { PureComponent } from 'react';
import {
  View,
  Text,
  Image,
  Platform,
  TouchableOpacity,
  FlatList,
  Dimensions,
  ScrollView,
} from 'react-native';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';

import HeaderImageScrollView from 'react-native-image-header-scroll-view';
import Icon from 'react-native-vector-icons/Ionicons';
import { Images, ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';
import styles from './styles';

import HeaderScore from '../../components/HeaderScore';
import HeaderTeam from '../../components/HeaderTeam';
import HeaderBubbleWinLose from '../../components/HeaderBubbleWinLose';
import LanguageVI from '../../constants/Language';
import TeamClub from '../teamClub';
import TeamSquad from '../teamSquad';

const WinLose = ['win', 'lose', 'draw', 'win', ''];

export default class Team extends PureComponent {
  state = {
    tab: 'club',
  };

  renderHeaderImage = () => {
    return (
      <Image
        source={Images.teamImage}
        style={styles.HeaderImage}
        resizeMode="cover"
      />
    );
  };

  renderForeground = () => {
    return (
      <View style={styles.ViewGroundMain}>
        <View style={styles.ViewGroundTop}>
          <HeaderTeam logoTeam={Images.real} teamName="Real madrid" />
          <HeaderScore scoreLeft={2} scoreRight={3} date="6 th 10" />
          <HeaderTeam logoTeam={Images.valencia} teamName="Valencia" />
        </View>
        <View style={styles.ViewGroundBubble}>
          {WinLose.map((item, index) => (
            <HeaderBubbleWinLose type={item} key={index} />
          ))}
        </View>
      </View>
    );
  };

  renderTouchableFixedForeground = () => {
    return (
      <View style={styles.ViewFixGround}>
        <View style={styles.ViewFixGroundLeft}>
          <TouchableOpacity
            style={styles.ButtonHeader}
            onPress={() => this.props.navigation.goBack()}
          >
            <Icon name="md-arrow-back" size={28} color={ColorTheme.white} />
          </TouchableOpacity>
          <Text style={styles.TeamName}>Valencial</Text>
        </View>
        <TouchableOpacity style={styles.ButtonHeader}>
          <Icon name="md-star-outline" size={28} color={ColorTheme.white} />
        </TouchableOpacity>
      </View>
    );
  };

  navigate = destinated => {
    this.props.navigation.navigate(destinated);
  };

  renderContent = condition => {
    if (condition === 'club') {
      return (
        <TeamClub
          onPressTeam={() => this.navigate('Team')}
          onPressMatch={() => this.navigate('Match')}
          onPressPlayer={() => this.navigate('Player')}
          onPressLeague={() => this.navigate('League')}
          onPressShowAllFixture={() => this.navigate('Fixture')}
          onPressShowAllNews={() => this.navigate('News')}
          onPressShowAllTransfer={() => this.navigate('TransferRumours')}
        />
      );
    }
    return (
      <TeamSquad
        onPressTeam={() => this.navigate('Team')}
        onPressMatch={() => this.navigate('Match')}
        onPressPlayer={() => this.navigate('Player')}
        onPressLeague={() => this.navigate('League')}
      />
    );
  };

  getStyleTab = condition => {
    if (condition === 'club') {
      return styles.tab;
    }
    return styles.tabActive;
  };

  render() {
    return (
      <View style={styles.viewMain}>
        <HeaderImageScrollView
          maxHeight={180 * d.ratioH}
          minHeight={Platform.OS === 'ios' ? 80 * d.ratioH : 60 * d.ratioH}
          minOverlayOpacity={0.3}
          maxOverlayOpacity={0.6}
          overlayColor={ColorTheme.Teal900}
          renderHeader={() => this.renderHeaderImage()}
          fadeOutForeground={true}
          renderForeground={() => this.renderForeground()}
          renderTouchableFixedForeground={() =>
            this.renderTouchableFixedForeground()
          }
          foregroundParallaxRatio={1}
        >
          <View style={styles.content}>
            {this.renderContent(this.state.tab)}
          </View>
        </HeaderImageScrollView>

        <View style={styles.tabbar}>
          <TouchableOpacity
            style={this.state.tab == 'club' ? styles.tabActive : styles.tab}
            onPress={() => this.setState({ tab: 'club' })}
          >
            <Text style={styles.labelStyle}>Câu lạc bộ</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={this.state.tab == 'squad' ? styles.tabActive : styles.tab}
            onPress={() => this.setState({ tab: 'squad' })}
          >
            <Text style={styles.labelStyle}>Đội hình</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
