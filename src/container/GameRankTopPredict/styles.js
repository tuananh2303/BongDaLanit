import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  ViewMain: {
    flex: 1,
    backgroundColor: 'white',
  },
  rowRank: {
    height: 70 * d.ratioH,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  rank: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  info: {
    flex: 5,
    flexDirection: 'column',
    paddingLeft: 20 * d.ratioW,
  },
  imgAvatar: {
    height: 50 * d.ratioH,
    width: 50 * d.ratioW,
    borderRadius: 25,
  },
  name: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  class: {
    flex: 1,
    justifyContent: 'center',
  },
  pointGain: {
    flex: 1,
    justifyContent: 'center',
  },
  imgClass: {
    height: 20 * d.ratioH,
    width: 40 * d.ratioW,
    borderRadius: 5,
  },
  nameText: {
    flex: 3,
    justifyContent: 'center',
  },
  nameIconClass: {
    flex: 1,
    justifyContent: 'center',
  },
  txtFullName: {
    fontSize: FontSize.medium + 2,
    color: ColorTheme.black,
  },
  txtClass: {
    fontSize: FontSize.medium,
    color: ColorTheme.black,
  },
  txtPointGain: {
    fontSize: FontSize.medium,
    color: ColorTheme.black,
  },
  buttonGroup: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50 * d.ratioH,
    paddingLeft: 50 * d.ratioW,
    paddingRight: 50 * d.ratioW,
    backgroundColor: 'white',
  },
  buttonGroupSub: {
    height: 30 * d.ratioH,
    flexDirection: 'row',
    borderWidth: 2,
    // borderColor: ColorTheme.Teal500,
    borderRadius: 5,
  },
  date: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: 2,
  },
  dateColor: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: ColorTheme.Teal500,
    borderRadius: 2,
  },

  txtDate: {
    fontSize: FontSize.medium,
    // color: ColorTheme.Teal500,
  },
  txtDateColor: {
    fontSize: FontSize.medium,
    color: ColorTheme.white,
  },
  txtMonth: {
    fontSize: FontSize.medium,
    color: ColorTheme.Teal500,
  },
  txtMonthColor: {
    fontSize: FontSize.medium,
    color: ColorTheme.white,
  },
});
