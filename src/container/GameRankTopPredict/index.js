import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import Axios from 'axios';
import { connect } from 'react-redux';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Images, ColorTheme } from '../../themes';
import LanguageVI from '../../constants/Language';
import { URL_RANK_TOP_PREDICT } from '../../constants/URL';
import GameRankRow from '../../components/GameRankRow';

const DAY_TYPE = 0;
const MONTH_TYPE = 1;

class GameRankTopPredict extends PureComponent {
  state = {
    isFetching: false,
    data: [],
    dataSuccess: false,
    type: MONTH_TYPE,
  };

  componentDidMount() {
    this.getDataGameRank(this.state.type);
  }

  getDataGameRank = id => {
    this.setState({ isFetching: true });
    Axios.get(URL_RANK_TOP_PREDICT + id)
      .then(response => {
        // handle success
        console.log(response.data);
        if (response.data.status == 'success') {
          this.setState({
            data: response.data,
            dataSuccess: true,
            isFetching: false,
          });
        }
      })
      .catch(error => {
        // handle error
        console.log(error);
      });
  };

  setDataMonth = type => {
    this.setState({
      type,
    });

    this.getDataGameRank(type);
  };

  renderList = () => {
    if (this.state.dataSuccess) {
      return (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={this.state.data.data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => {
            if (item != null) {
              return (
                <GameRankRow
                  data={item}
                  onPress={() =>
                    this.props.navigation.navigate('Profile', { id: item.id })
                  }
                />
              );
            }
          }}
        />
      );
    }
    if (this.state.isFetching) {
      return (
        <View>
          <ActivityIndicator size="large" color={ColorTheme.Teal900} />
        </View>
      );
    }
    return null;
  };

  dateColor = () => {
    if (this.props.settingReducer.data.theme == 2) {
      return [styles.dateColor, { backgroundColor: ColorTheme.Teal900 }];
    }
    if (this.props.settingReducer.data.theme == 3) {
      return [styles.dateColor, { backgroundColor: ColorTheme.blue700 }];
    }
    if (this.props.settingReducer.data.theme == 4) {
      return [styles.dateColor, { backgroundColor: ColorTheme.black }];
    }
    if (this.props.settingReducer.data.theme == 5) {
      return [styles.dateColor, { backgroundColor: ColorTheme.purple700 }];
    }
    return [styles.dateColor, { backgroundColor: ColorTheme.Teal900 }];
  };

  txtDate = () => {
    if (this.props.settingReducer.data.theme == 2) {
      return [styles.txtDate, { color: ColorTheme.Teal900 }];
    }
    if (this.props.settingReducer.data.theme == 3) {
      return [styles.txtDate, { color: ColorTheme.blue700 }];
    }
    if (this.props.settingReducer.data.theme == 4) {
      return [styles.txtDate, { color: ColorTheme.black }];
    }
    if (this.props.settingReducer.data.theme == 5) {
      return [styles.txtDate, { color: ColorTheme.purple700 }];
    }
    return [styles.txtDate, { color: ColorTheme.Teal900 }];
  };

  buttonGroupSub = () => {
    if (this.props.settingReducer.data.theme == 2) {
      return [styles.buttonGroupSub, { borderColor: ColorTheme.Teal900 }];
    }
    if (this.props.settingReducer.data.theme == 3) {
      return [styles.buttonGroupSub, { borderColor: ColorTheme.blue700 }];
    }
    if (this.props.settingReducer.data.theme == 4) {
      return [styles.buttonGroupSub, { borderColor: ColorTheme.black }];
    }
    if (this.props.settingReducer.data.theme == 5) {
      return [styles.buttonGroupSub, { borderColor: ColorTheme.purple700 }];
    }
    return [styles.buttonGroupSub, { borderColor: ColorTheme.Teal900 }];
  };

  render() {
    return (
      <View style={styles.ViewMain}>
        <View style={styles.buttonGroup}>
          <View style={this.buttonGroupSub()}>
            <TouchableOpacity
              style={
                this.state.type == DAY_TYPE ? this.dateColor() : styles.date
              }
              onPress={() => this.setDataMonth(DAY_TYPE)}
            >
              <Text
                style={
                  this.state.type == DAY_TYPE
                    ? styles.txtDateColor
                    : this.txtDate()
                }
              >
                Ngày
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                this.state.type == DAY_TYPE ? styles.date : this.dateColor()
              }
              onPress={() => this.setDataMonth(MONTH_TYPE)}
            >
              <Text
                style={
                  this.state.type == MONTH_TYPE
                    ? styles.txtDateColor
                    : this.txtDate()
                }
              >
                Tháng
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        {this.renderList()}
      </View>
    );
  }
}

// export default GameRankTopPredict;

const mapStateToProps = state => ({
  userReducer: state.userReducer,
  settingReducer: state.settingReducer,
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GameRankTopPredict);
