import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import Axios from 'axios';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Images, ColorTheme } from '../../themes';
import LanguageVI from '../../constants/Language';
import { URL_RANK_TOP_EVENT } from '../../constants/URL';
import GameRankRow from '../../components/GameRankRow';
// import dataGameRankFake from './dataGameRankFake';

class GameRankTopEvent extends PureComponent {
  state = {
    isFetching: false,
    error: false,
    data: [],
    dataSuccess: false,
  };

  componentDidMount() {
    this.getDataGameRank();
  }

  getDataGameRank = () => {
    this.setState({ isFetching: true });
    Axios.get(
      URL_RANK_TOP_EVENT +
        this.props.userReducer.dataFull.user.id +
        '?token=' +
        this.props.userReducer.dataFull.user.token,
    )
      .then(response => {
        this.setState({
          data: response.data,
          dataSuccess: true,
          isFetching: false,
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({
          error: true,
        });
      });
  };

  txtEventTitle = () => {
    if (this.props.settingReducer.data.theme == 2) {
      return [styles.txtEventTitle, { color: ColorTheme.Teal900 }];
    }
    if (this.props.settingReducer.data.theme == 3) {
      return [styles.txtEventTitle, { color: ColorTheme.blue700 }];
    }
    if (this.props.settingReducer.data.theme == 4) {
      return [styles.txtEventTitle, { color: ColorTheme.black }];
    }
    if (this.props.settingReducer.data.theme == 5) {
      return [styles.txtEventTitle, { color: ColorTheme.purple700 }];
    }
    return [styles.txtEventTitle, { color: ColorTheme.Teal900 }];
  };

  renderEvent = data => {
    return (
      <View style={styles.containerEvent}>
        <View style={styles.eventTitle}>
          <Text style={this.txtEventTitle()}>{data.event_title}</Text>
        </View>
        <View style={styles.eventPrize}>
          <Text style={this.txtEventTitle()}>
            {LanguageVI.Prize}
            {data.prize}
          </Text>
        </View>
        <View style={styles.userPoint}>
          <Text style={styles.txtEvent}>
            {LanguageVI.YourPoint}
            {data.user_point}
          </Text>
        </View>
        <View style={styles.eventDate}>
          <View style={styles.dateStart}>
            <View style={styles.dateStartTop}>
              <Text style={styles.txtEvent}>{LanguageVI.StartDate}</Text>
            </View>
            <View style={styles.dateStartBottom}>
              <Text style={styles.txtEvent}>{data.start_date}</Text>
            </View>
          </View>
          <View style={styles.dateEnd}>
            <View style={styles.dateEndTop}>
              <Text style={styles.txtEvent}>{LanguageVI.EndDate}</Text>
            </View>
            <View style={styles.dateEndBottom}>
              <Text style={styles.txtEvent}>{data.end_date}</Text>
            </View>
          </View>
        </View>
      </View>
    );
  };

  render() {
    if (this.state.isFetching) {
      return (
        <View>
          <ActivityIndicator size="large" color={ColorTheme.Teal900} />
        </View>
      );
    }
    if (this.state.dataSuccess === true) {
      return (
        <View style={styles.ViewMain}>
          {this.renderEvent(this.state.data)}
          <FlatList
            showsVerticalScrollIndicator={false}
            data={this.state.data.data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => {
              if (item != null) {
                return (
                  <GameRankRow
                    data={item}
                    onPress={() =>
                      this.props.navigation.navigate('Profile', { id: item.id })
                    }
                  />
                );
              }
            }}
          />
        </View>
      );
    }
    return null;
  }
}

const mapStateToProps = state => ({
  userReducer: state.userReducer,
  settingReducer: state.settingReducer,
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GameRankTopEvent);
