import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  ViewMain: {
    flex: 1,
    backgroundColor: 'white',
  },
  containerEvent: {
    backgroundColor: 'white',
  },
  eventTitle: {
    height: 50 * d.ratioH,
    justifyContent: 'center',
    paddingLeft: 10 * d.ratioW,
  },
  eventPrize: {
    height: 50 * d.ratioH,
    justifyContent: 'center',
    paddingLeft: 10 * d.ratioW,
  },
  eventDate: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderWidth: 0.5,
    borderColor: ColorTheme.gray400,
    borderBottomColor: ColorTheme.gray400,
    borderBottomWidth: 0.5,
  },
  txtEvent: {
    color: ColorTheme.black,
    fontSize: FontSize.medium,
  },
  txtEventTitle: {
    // color: ColorTheme.Teal500,
    fontSize: FontSize.medium + 3,
  },

  dateStart: {
    flex: 1,
    height: 50 * d.ratioH,
    flexDirection: 'column',
    borderRightWidth: 0.5,
    borderRightColor: ColorTheme.gray400,
  },
  dateEnd: {
    flex: 1,
    height: 50 * d.ratioH,
    flexDirection: 'column',
  },
  dateStartTop: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: ColorTheme.gray400,
    borderBottomWidth: 0.5,
  },
  dateStartBottom: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dateEndTop: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: ColorTheme.gray400,
    borderBottomWidth: 0.5,
  },
  dateEndBottom: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  userPoint: {
    height: 50 * d.ratioH,
    justifyContent: 'center',
    alignItems: 'center',
  },

  // style gameRow
  rowRank: {
    height: 70 * d.ratioH,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  rank: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  info: {
    flex: 5,
    flexDirection: 'column',
    paddingLeft: 20 * d.ratioW,
  },
  imgAvatar: {
    height: 50 * d.ratioH,
    width: 50 * d.ratioW,
    borderRadius: 25,
  },
  name: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  class: {
    flex: 1,
    justifyContent: 'center',
  },
  pointGain: {
    flex: 1,
    justifyContent: 'center',
  },
  imgClass: {
    height: 60 * d.ratioH,
    width: 30 * d.ratioW,
  },
  nameText: {
    flex: 3,
    justifyContent: 'center',
  },
  nameIconClass: {
    flex: 1,
    justifyContent: 'center',
    // backgroundColor: 'red',
  },
  txtFullName: {
    fontSize: FontSize.medium + 2,
    color: ColorTheme.black,
  },
  txtClass: {
    fontSize: FontSize.medium,
    color: ColorTheme.black,
  },
  txtPointGain: {
    fontSize: FontSize.medium,
    color: ColorTheme.black,
  },
});
