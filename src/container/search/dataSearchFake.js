const dataSearchFake = [
  {
    id: 1,
    title: 'Top result',
    status: 'row',
    data: {
      id: 1,
      images: 'http://dummyimage.com/222x237.bmp/5fa2dd/ffffff',
      name: 'Alfy Lempenny',
    },
  },
  {
    id: 2,
    title: 'Team',
    status: 'row1',
    data: [
      {
        id: 1,
        images: 'http://dummyimage.com/222x237.bmp/5fa2dd/ffffff',
        name: 'Alfy Lempenny',
      },
      {
        id: 2,
        images: 'http://dummyimage.com/219x136.png/5fa2dd/ffffff',
        name: 'Lindy Bourdon',
      },
      {
        id: 3,
        images: 'http://dummyimage.com/226x173.png/ff4444/ffffff',
        name: 'Brice Borles',
      },
      {
        id: 4,
        images: 'http://dummyimage.com/223x237.bmp/cc0000/ffffff',
        name: 'Erich Challenor',
      },
      {
        id: 5,
        images: 'http://dummyimage.com/110x148.png/5fa2dd/ffffff',
        name: "Steven O'Heyne",
      },
      {
        id: 6,
        images: 'http://dummyimage.com/180x142.bmp/cc0000/ffffff',
        name: 'Dunn Glowacz',
      },
      {
        id: 7,
        images: 'http://dummyimage.com/165x122.bmp/ff4444/ffffff',
        name: 'Mychal Ackhurst',
      },
      {
        id: 8,
        images: 'http://dummyimage.com/195x129.jpg/cc0000/ffffff',
        name: 'Meridith Dulake',
      },
      {
        id: 9,
        images: 'http://dummyimage.com/189x206.jpg/dddddd/000000',
        name: 'Uta Mayhead',
      },
      {
        id: 10,
        images: 'http://dummyimage.com/116x132.jpg/5fa2dd/ffffff',
        name: 'Codi Daulby',
      },
    ],
  },
  {
    id: 3,
    title: 'Competitions',
    status: 'sub_row',
    data: [
      {
        id: 1,
        images: 'http://dummyimage.com/129x219.bmp/ff4444/ffffff',
        name: 'Emmye Sweedland',
        sub_name: 'Avrit Fishley',
      },
      {
        id: 2,
        images: 'http://dummyimage.com/235x143.png/dddddd/000000',
        name: 'Freddi Keward',
        sub_name: 'Peg Essery',
      },
      {
        id: 3,
        images: 'http://dummyimage.com/174x216.jpg/dddddd/000000',
        name: 'Rogers Copperwaite',
        sub_name: 'Hatty Harbor',
      },
      {
        id: 4,
        images: 'http://dummyimage.com/129x172.jpg/ff4444/ffffff',
        name: 'Daniel Gerrets',
        sub_name: 'Rafi De Marchi',
      },
      {
        id: 5,
        images: 'http://dummyimage.com/167x238.bmp/dddddd/000000',
        name: 'Arne Creevy',
        sub_name: 'Zorina Trineman',
      },
      {
        id: 6,
        images: 'http://dummyimage.com/169x126.bmp/ff4444/ffffff',
        name: 'Teodoor Huzzay',
        sub_name: 'Lindi Veness',
      },
      {
        id: 7,
        images: 'http://dummyimage.com/172x176.jpg/cc0000/ffffff',
        name: 'Jorge Burtwell',
        sub_name: 'Lara Rayer',
      },
      {
        id: 8,
        images: 'http://dummyimage.com/139x247.bmp/ff4444/ffffff',
        name: 'Kimble Giovannilli',
        sub_name: 'Wallache Bruckshaw',
      },
      {
        id: 9,
        images: 'http://dummyimage.com/242x174.jpg/dddddd/000000',
        name: 'Walsh Feather',
        sub_name: 'Conn Palfery',
      },
      {
        id: 10,
        images: 'http://dummyimage.com/219x190.png/ff4444/ffffff',
        name: 'Mariquilla Okie',
        sub_name: 'Crissy McChesney',
      },
    ],
  },
];
export default dataSearchFake;
