import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  viewMain: {
    height: 60 * d.ratioH,
    backgroundColor: 'white',
    flex: 1,
    // elevation: 3,
  },
  input: {
    backgroundColor: 'white',
    margin: 10,
    height: 40 * d.ratioH,
    flexDirection: 'row',
    borderRadius: 20,
    elevation: 3,
  },
  inputLeft: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputMid: {
    flex: 6,
    justifyContent: 'center',
  },
  textInput: {
    height: 40 * d.ratioH,
  },
  inputRight: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
