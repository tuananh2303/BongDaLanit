import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import styles from './styles';
import Icon from 'react-native-vector-icons/Ionicons';
import { ColorTheme } from '../../themes';

class Search extends PureComponent {
  state = {
    value: '',
  };

  submitEdit = () => {
    return this.props.navigation.navigate('searchContent', {
      value: this.state.value,
    });
  };

  render() {
    return (
      <View style={styles.viewMain}>
        <View style={styles.input}>
          <TouchableOpacity
            style={styles.inputLeft}
            onPress={() => this.props.navigation.goBack()}
          >
            <Icon name="ios-arrow-back" size={23} color={ColorTheme.Teal900} />
          </TouchableOpacity>
          <View style={styles.inputMid}>
            <TextInput
              style={styles.textInput}
              onChangeText={value => this.setState({ value })}
              value={this.state.value}
              placeholder="Nhập nội dung tìm kiếm"
              onSubmitEditing={this.submitEdit}
            />
          </View>
          <TouchableOpacity
            style={styles.inputRight}
            onPress={() => this.setState({ value: '' })}
          >
            <Icon name="ios-close" size={25} color={ColorTheme.Teal900} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default Search;
