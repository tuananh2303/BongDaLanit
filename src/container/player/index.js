import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Image } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import LanguageVI from '../../constants/Language';
import Header from '../../components/Header';
import { row, rowsub, title, rowsubImg } from '../../components/Row';
import { ColorTheme, Images } from '../../themes';

class Home extends PureComponent {
  state = {};

  render() {
    return (
      <ScrollView style={styles.viewMain}>
        <Header
          title={LanguageVI.Player}
          rightHeader={
            <Icon name="md-star-outline" size={23} style={{ color: 'white' }} />
          }
        />

        <View style={styles.avataView}>
          <Image source={Images.hedera} style={styles.avata} />
        </View>
        <View style={styles.nameView}>
          <Text style={styles.nameText}>Cristiano Ronaldo</Text>
        </View>
        <View>
          {rowsub('md-person', 'Gary James Cahill', LanguageVI.FullName)}
          {rowsubImg(Images.england, 'England', LanguageVI.Nationality)}
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Team')}
          >
            {rowsubImg(Images.chelsea, 'Chelsea', LanguageVI.Team)}
          </TouchableOpacity>
          {rowsub('ios-shirt', '24', LanguageVI.ShirtNumber)}
          {rowsub('md-locate', LanguageVI.Defenders, LanguageVI.Position)}
          {rowsub('md-heart-empty', '23-03-1997', LanguageVI.BirthDate)}
          {rowsub('ios-walk', 'Chân phải', LanguageVI.PreferredFoot)}
          {rowsub('md-body', '85kg | 191 cm', LanguageVI.WeightAndHeight)}
        </View>
      </ScrollView>
    );
  }
}

export default Home;
