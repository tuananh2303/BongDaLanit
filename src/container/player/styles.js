import { StyleSheet } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  viewMain: {
    flex: 1,
    backgroundColor: 'white',
  },
  Add: {
    paddingTop: 20 * d.ratioH,
    paddingBottom: 10 * d.ratioH,
  },
  container: {
    borderBottomColor: ColorTheme.white2,
    borderBottomWidth: 1 * d.ratioH,
  },
  avata: {
    height: 100 * d.ratioH,
    width: 100 * d.ratioH,
    borderRadius: 50 * d.ratioH,
  },
  avataView: {
    alignItems: 'center',
    marginTop: 20 * d.ratioH,
    marginBottom: 20 * d.ratioH,
  },
  nameView: {
    alignItems: 'center',
    borderBottomColor: ColorTheme.white2,
    borderBottomWidth: 1 * d.ratioH,
    paddingBottom: 10 * d.ratioH,
  },
  nameText: {
    color: ColorTheme.black,
    fontSize: FontSize.large,
    fontWeight: '300',
  },
});
