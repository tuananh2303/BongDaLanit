import { StyleSheet } from 'react-native';

import { ColorTheme } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  viewMain: {
    flex: 1,
    backgroundColor: ColorTheme.gray300,
  },
  tabbar: {
    // backgroundColor: ColorTheme.Teal900,
    backgroundColor: 'white',
  },
  tabbar2: {
    // backgroundColor: ColorTheme.Teal900,
    backgroundColor: ColorTheme.Teal900,
  },
  tab: {
    height: 45 * d.ratioH,
    width: d.windowSize.width / 7,
    alignItems: 'center',
    justifyContent: 'center',
  },
  indicator: {
    backgroundColor: ColorTheme.Teal900,
  },

  indicator2: {
    backgroundColor: ColorTheme.white,
  },

  labelStyle: {
    fontSize: 12,
    fontWeight: 'normal',
    color: ColorTheme.Teal900,
    marginBottom: 5 * d.ratioH,
  },
  labelStyle2: {
    fontSize: 12,
    fontWeight: 'normal',
    color: ColorTheme.white,
    marginBottom: 5 * d.ratioH,
  },
  viewTab: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabbar3: {
    backgroundColor: ColorTheme.blue800,
  },
  tabbar4: {
    backgroundColor: ColorTheme.black,
  },
});
