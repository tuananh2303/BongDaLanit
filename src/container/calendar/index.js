import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import changeCalenDar from '../../redux/actions/calendarAction';
import styles from './styles';
import Header from '../../components/Header';
import LanguageVI from '../../constants/Language';
import { ColorTheme } from '../../themes';
import {
  getTimeFromNow,
  dayOfWeek,
  dayOfMonth,
} from '../../utilities/MomentTime';
import CalendarDayOfWeek from '../calendarDayOfWeek';
import CalendarDayOfWeekDay3 from '../calendarDayOfWeekDay3';

class Calendar extends PureComponent {
  /* eslint-disable */
  state = {
    index: 2,
    routes: [
      { key: 'day1', dayNum: dayOfMonth(-2), dayWeek: dayOfWeek(-2) },
      { key: 'day2', dayNum: dayOfMonth(-1), dayWeek: dayOfWeek(-1) },
      { key: 'day3', dayNum: dayOfMonth(0), dayWeek: dayOfWeek(0) },
      { key: 'day4', dayNum: dayOfMonth(1), dayWeek: dayOfWeek(1) },
      { key: 'day5', dayNum: dayOfMonth(2), dayWeek: dayOfWeek(2) },
      { key: 'day6', dayNum: dayOfMonth(3), dayWeek: dayOfWeek(3) },
      { key: 'day7', dayNum: dayOfMonth(4), dayWeek: dayOfWeek(4) },
      // { key: 'daymonth', icons: 'ios-calendar' },
    ],
    liveScore: false,
  };
  /* eslint-enable */

  componentDidMount() {
    // console.log(this.props.calendarReducer);
  }

  renderLabel = ({ route }) => (
    <View style={styles.viewTab}>
      <Text style={this.renderLabelGetStyle()}>{route.dayNum}</Text>
      <Text style={this.renderLabelGetStyle()}>{route.dayWeek}</Text>
    </View>
  );

  _renderIconGetstyle = () => {
    if (this.props.settingReducer.data.theme == 1) {
      return ColorTheme.Teal900;
    }
    return ColorTheme.white;
  };

  _renderIcon = ({ route }) => (
    <Icon name={route.icons} size={24} color={this._renderIconGetstyle()} />
  );

  indicatorStyle = () => {
    if (this.props.settingReducer.data.theme == 1) {
      return styles.indicator;
    }
    return styles.indicator2;
  };

  _renderTabBarGetStyle = () => {
    if (this.props.settingReducer.data.theme == 2) {
      return styles.tabbar2;
    }
    if (this.props.settingReducer.data.theme == 3) {
      return styles.tabbar3;
    }
    if (this.props.settingReducer.data.theme == 4) {
      return styles.tabbar4;
    }
    if (this.props.settingReducer.data.theme == 5) {
      return [styles.tabbar4, { backgroundColor: ColorTheme.purple700 }];
    }
    return styles.tabbar;
  };

  renderLabelGetStyle = () => {
    if (this.props.settingReducer.data.theme == 1) {
      return styles.labelStyle;
    }
    return styles.labelStyle2;
  };

  _renderTabBar = props => {
    return (
      <TabBar
        {...props}
        scrollEnabled
        indicatorStyle={this.indicatorStyle()}
        style={this._renderTabBarGetStyle()}
        tabStyle={styles.tab}
        renderLabel={this.renderLabel}
        renderIcon={this._renderIcon}
      />
    );
  };

  day1 = () => (
    <CalendarDayOfWeek navigation={this.props.navigation} dayInRange={1} />
  );

  day2 = () => (
    <CalendarDayOfWeek navigation={this.props.navigation} dayInRange={2} />
  );

  day3 = data => (
    <CalendarDayOfWeekDay3
      navigation={this.props.navigation}
      dayInRange={3}
      liveScore={data}
    />
    // <CalendarDayOfWeek navigation={this.props.navigation} dayInRange={3} />
  );

  day4 = () => (
    <CalendarDayOfWeek navigation={this.props.navigation} dayInRange={4} />
  );

  day5 = () => (
    <CalendarDayOfWeek navigation={this.props.navigation} dayInRange={5} />
  );

  day6 = () => (
    <CalendarDayOfWeek navigation={this.props.navigation} dayInRange={6} />
  );

  day7 = () => (
    <CalendarDayOfWeek navigation={this.props.navigation} dayInRange={7} />
  );

  daymonth = () => (
    <CalendarDayOfWeek navigation={this.props.navigation} dayInRange={3} />
  );

  pressLiveScore = () => {
    this.setState({
      liveScore: !this.state.liveScore,
      index: 2,
    });
    // console.log(this.state);

    this.props.changeCalenDar(3);
  };

  colorLiveScoreIcon = () => {
    if (this.state.liveScore) {
      if (this.props.settingReducer.data.theme == 1) {
        return ColorTheme.LightBlue500;
      }
      return ColorTheme.white;
    }
    if (this.props.settingReducer.data.theme == 1) {
      return ColorTheme.Teal900;
    }
    return ColorTheme.white;
  };

  render() {
    return (
      <View style={styles.viewMain}>
        <Header
          title={LanguageVI.Calendar}
          rightHeader={
            <Icon
              onPress={() => this.pressLiveScore()}
              name={this.state.liveScore ? 'ios-time' : 'ios-timer'}
              size={27}
              color={this.colorLiveScoreIcon()}
            />
          }
        />
        <TabView
          navigationState={this.state}
          renderTabBar={this._renderTabBar}
          renderScene={SceneMap({
            day1: this.day1,
            day2: this.day2,
            day3: () => this.day3(this.state.liveScore),
            day4: this.day4,
            day5: this.day5,
            day6: this.day6,
            day7: this.day7,
            // daymonth: this.daymonth,
          })}
          onIndexChange={index => {
            if (index != 2) {
              this.setState({ liveScore: false });
            }
            this.setState({ index });
            this.props.changeCalenDar(index + 1);
          }}
          useNativeDriver
          initialLayout={{
            height: 0,
            width: Dimensions.get('window').width,
          }}
        />
      </View>
    );
  }
}

// export default Calendar;

const mapStateToProps = state => ({
  calendarReducer: state.calendarReducer,
  settingReducer: state.settingReducer,
});

const mapDispatchToProps = dispatch => ({
  changeCalenDar: day => dispatch(changeCalenDar(day)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Calendar);
