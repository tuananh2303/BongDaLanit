import { StyleSheet, Platform } from 'react-native';

import { ColorTheme, FontSize } from '../../themes';
import * as d from '../../utilities/Tranform';

export default StyleSheet.create({
  viewMain: {
    flex: 1,
    backgroundColor: ColorTheme.gray300,
  },

  content: {
    minHeight: d.windowSize.height - 45 * d.ratioH,
  },

  renderBaseLink: {
    borderRadius: 10,
    borderWidth: 1,
    borderColor: ColorTheme.white2,
  },

  HeaderImage: {
    width: d.windowSize.width,
    height: 200 * d.ratioH,
  },

  icon: {
    color: ColorTheme.gray500,
  },

  iconActive: {
    color: ColorTheme.black,
  },

  tabbar: {
    backgroundColor: ColorTheme.white,
    flexDirection: 'row',
    elevation: 5,
  },
  tab: {
    height: 45 * d.ratioH,
    width: d.windowSize.width / 5,
    alignItems: 'center',
    justifyContent: 'center',
  },

  tabActive: {
    height: 45 * d.ratioH,
    width: d.windowSize.width / 5,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopColor: ColorTheme.black,
    borderTopWidth: 1.2,
  },

  indicator: {
    backgroundColor: ColorTheme.black,
  },
  labelStyle: {
    fontSize: FontSize.medium,
    fontWeight: 'normal',
    color: ColorTheme.black,
    marginBottom: 5 * d.ratioH,
  },

  excerpt: {
    paddingHorizontal: 12 * d.ratioH,
    paddingTop: 10 * d.ratioH,
    fontSize: FontSize.medium,
    fontWeight: 'normal',
    color: ColorTheme.black,
    marginBottom: 5 * d.ratioH,
  },
  // renderTouchableFixedForeground

  ViewFixGround: {
    height: 50 * d.ratioH,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  ViewFixGroundLeft: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  ButtonHeader: {
    height: 50 * d.ratioH,
    width: 60 * d.ratioW,
    alignItems: 'center',
    justifyContent: 'center',
  },

  TeamName: {
    color: ColorTheme.white,
    fontSize: FontSize.large + 3,
    // fontWeight: 'bold',
    textShadowColor: ColorTheme.black,
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
  },

  // renderForeground

  ViewGroundMain: {
    position: 'absolute',
    bottom: 10 * d.ratioH,
    left: 10 * d.ratioW,
    right: 10 * d.ratioW,
  },

  ViewGroundTop: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  ViewGroundBubble: {
    marginTop: 15 * d.ratioH,
    width: d.windowSize.width,
    flexDirection: 'row',
    justifyContent: 'center',
  },

  componentHeaderInfo: {
    flexDirection: 'row',
    height: 20 * d.ratioH,
    alignItems: 'center',
    justifyContent: 'center',
  },

  componentHeaderInfoText: {
    color: ColorTheme.white,
    fontSize: FontSize.small,
    fontWeight: 'bold',
  },

  componentHeaderInfoViewLeft: {
    flex: 6,
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },

  componentHeaderInfoViewRight: {
    flex: 6,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  componentHeaderInfoViewMiddle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  content_img: {
    // paddingHorizontal: 12 * d.ratioH,
    paddingVertical: 12,
  },
  img_content: {
    height: 200 * d.ratioH,
    width: 340 * d.ratioW,
  },
  txt_content: {
    paddingHorizontal: 12 * d.ratioH,
    paddingTop: 10 * d.ratioH,
    fontSize: FontSize.medium,
    fontWeight: 'normal',
    color: ColorTheme.black,
    marginBottom: 5 * d.ratioH,
  },
  posts_related: {
    paddingTop: 10 * d.ratioH,
  },
  containerModal1: {
    flex: 1,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    justifyContent: 'center',
  },
  viewModal1: {
    flex: 1,
    backgroundColor: ColorTheme.grayModalTransparent,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  slider: {
    flex: 1,
    backgroundColor: 'black',
  },
  iconSlider: {
    height: 50 * d.ratioH,
    justifyContent: 'center',
    paddingLeft: 20 * d.ratioW,
  },
  gallery: {
    flex: 1,
    backgroundColor: 'black',
  },
  customImage: {
    height: 250 * d.ratioH,
    width: 360 * d.ratioW,
  },
  buttons: {
    height: 50 * d.ratioH,
    flexDirection: 'row',
    backgroundColor: 'rgb(33,33,33)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonSub: {
    width: 10 * d.ratioW,
    height: 10 * d.ratioH,
    borderRadius: 5,
    margin: 2,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  customSlide: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgb(33,33,33)',
  },
  buttonSelected: {
    backgroundColor: 'rgb(3,169,244)',
    width: 10 * d.ratioW,
    height: 10 * d.ratioH,
    borderRadius: 5,
  },
  content1: {
    height: 50 * d.ratioH,
    justifyContent: 'center',
    backgroundColor: 'rgb(33,33,33)',
  },
  close: {
    height: 70 * d.ratioH,
    backgroundColor: 'rgb(33,33,33)',
    justifyContent: 'center',
    paddingLeft: 20 * d.ratioW,
  },
});
