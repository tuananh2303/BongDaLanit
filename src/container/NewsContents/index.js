import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  Platform,
  Modal,
} from 'react-native';
import Axios from 'axios';
import HeaderImageScrollView, {
  TriggeringView,
} from 'react-native-image-header-scroll-view';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';

import { ShareApi, ShareDialog } from 'react-native-fbsdk';

import * as d from '../../utilities/Tranform';
import styles from './styles';
import Article from '../../components/Article';
import { ColorTheme, Images } from '../../themes';
import ImageResize from '../../components/ImageResize';
import { isIphoneX } from '../../utilities/device';
// const FBSDK = require('react-native-fbsdk');

// const { ShareApi, ShareDialog } = FBSDK;

class NewsContents extends PureComponent {
  state = {
    data_preRender: this.props.navigation.getParam('data'),
    isFetching: false,
    error: false,
    data1: [],
    dataSuccess: false,
  };

  componentDidMount() {
    this.getData(this.state.data_preRender.id);
  }

  getData = id => {
    this.setState({ isFetching: true });
    Axios.get('https://muldata.tintuc.app/posts/' + id)
      .then(response => {
        // handle success
        this.setState({
          data1: response.data.data,
          dataSuccess: true,
          isFetching: false,
        });
      })
      .catch(error => {
        // handle error
        // console.log(error);
        this.setState({
          error: true,
        });
      });
  };

  getIdData = (value, id) => {
    this.setState({
      data_preRender: value,
      data1: [],
    });
    return this.getData(id);
  };

  renderHeaderImage = () => {
    return (
      <Image
        source={{ uri: this.state.data_preRender.image_full_size }}
        style={styles.HeaderImage}
        resizeMode="cover"
      />
    );
  };

  renderForeground = () => {
    return (
      <View style={styles.ViewGroundMain}>
        <Text style={styles.TeamName}>{this.state.data_preRender.title}</Text>
      </View>
    );
  };

  shareFB = shareContent => {
    ShareDialog.canShow(shareContent)
      .then(canShow => {
        if (canShow) {
          return ShareDialog.show(shareContent);
        }
      })
      .then(
        result => {
          if (result.isCancelled) {
            console.log('Share cancelled');
          } else {
            console.log('Share success with postId: ', result);
          }
        },
        error => {
          console.log('Share fail with error: ', error);
        },
      );
  };

  renderTouchableFixedForeground = () => {
    return (
      <View
        style={[styles.ViewFixGround, isIphoneX() ? { marginTop: 20 } : null]}
      >
        <View style={styles.ViewFixGroundLeft}>
          <TouchableOpacity
            style={styles.ButtonHeader}
            onPress={() => this.props.navigation.goBack()}
          >
            <Icon name="md-arrow-back" size={28} color={ColorTheme.white} />
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={styles.ButtonHeader}
          onPress={() => {
            const mtLiveLinks = {
              contentType: 'link',
              contentUrl: this.state.data_preRender.share_link,
              contentDescription: this.state.data_preRender.title,
            };
            this.shareFB(mtLiveLinks);
          }}
        >
          <Icon name="md-share" size={22} color={ColorTheme.white} />
        </TouchableOpacity>
      </View>
    );
  };

  renderBaseLink = () => (
    <View>
      <Text style={styles.excerpt}>
        {this.state.data_preRender.base_link}
        {' - '}
        {this.state.data_preRender.time_ago}
      </Text>
      <Text style={styles.excerpt}>{this.state.data_preRender.excerpt}</Text>
    </View>
  );

  filterImage = dataInput => {
    const listImage = [];
    dataInput.map(item => {
      if (item.type === 'image') {
        listImage.push(item);
      }
    });
    return listImage;
  };

  rederContent = data => {
    return data.map((item, key) => {
      if (item.type == 'image') {
        return (
          <TouchableOpacity
            activeOpacity={1}
            style={styles.content_img}
            onPress={() =>
              this.props.navigation.navigate('newsImage', {
                data: this.filterImage(data),
                initialImage: item,
              })
            }
            key={key.toString()}
          >
            <ImageResize
              linkImageDefault={Images.defaultImage}
              resizeMode="contain"
              linkImage={item.data.src}
            />
          </TouchableOpacity>
        );
      }
      return (
        <View style={styles.text} key={key.toString()}>
          <Text style={styles.txt_content}>{item.data}</Text>
        </View>
      );
    });
  };

  getColor = () => {
    if (this.props.settingReducer.data.theme == 2) {
      return ColorTheme.Teal900;
    }
    if (this.props.settingReducer.data.theme == 3) {
      return ColorTheme.blue800;
    }
    if (this.props.settingReducer.data.theme == 4) {
      return ColorTheme.black;
    }
    if (this.props.settingReducer.data.theme == 5) {
      return ColorTheme.purple700;
    }

    return ColorTheme.Teal900;
  };

  render() {
    if (this.state.isFetching) {
      return (
        <HeaderImageScrollView
          maxHeight={200 * d.ratioH}
          minHeight={Platform.OS === 'ios' ? 70 * d.ratioH : 50 * d.ratioH}
          minOverlayOpacity={0.1}
          maxOverlayOpacity={0.9}
          overlayColor={this.getColor()}
          renderHeader={() => this.renderHeaderImage()}
          fadeOutForeground={true}
          renderForeground={() =>
            this.renderForeground(
              this.state.dataSuccess ? this.state.data : null,
            )
          }
          renderTouchableFixedForeground={() =>
            this.renderTouchableFixedForeground()
          }
          foregroundParallaxRatio={1}
        >
          <TriggeringView
            // style={styles.content}
            onHide={() => console.log('onHide')}
          >
            {this.renderBaseLink()}
          </TriggeringView>
        </HeaderImageScrollView>
      );
    }

    if (this.state.dataSuccess) {
      return (
        <HeaderImageScrollView
          maxHeight={200 * d.ratioH}
          minHeight={Platform.OS === 'ios' ? 70 * d.ratioH : 50 * d.ratioH}
          minOverlayOpacity={0.1}
          maxOverlayOpacity={0.9}
          overlayColor={this.getColor()}
          renderHeader={() => this.renderHeaderImage()}
          fadeOutForeground={true}
          renderForeground={() =>
            this.renderForeground(
              this.state.dataSuccess ? this.state.data : null,
            )
          }
          renderTouchableFixedForeground={() =>
            this.renderTouchableFixedForeground()
          }
          foregroundParallaxRatio={1}
        >
          <TriggeringView
            style={styles.content}
            onHide={() => console.log('onHide')}
          >
            {this.renderBaseLink()}
            <View style={styles.content}>
              {this.rederContent(this.state.data1.content)}
            </View>
            <View style={styles.posts_related}>
              {this.state.data1.posts_related.map((data, key) => {
                return (
                  <Article
                    type={2}
                    data={data}
                    key={key.toString()}
                    onPress={() => {
                      if (data.post_type == 'link') {
                        return this.props.navigation.navigate(
                          'NewsContentsWeb',
                          {
                            data,
                          },
                        );
                      }
                      return this.getIdData(data, data.id);
                    }}
                  />
                );
              })}
            </View>
          </TriggeringView>
        </HeaderImageScrollView>
      );
    }
    return null;
  }
}

const mapStateToProps = state => ({
  settingReducer: state.settingReducer,
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewsContents);
