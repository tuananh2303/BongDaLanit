import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import dataTransfer from './dataTransfer';
import data from './dataFake';
import TeamTableScroce from '../../components/TeamTableScroce';

import LanguageVI from '../../constants/Language';
import Header from '../../components/Header';
import Table from '../../components/Table';
import InfoKickRow from '../../components/InfoKickRow';
import FixtureRow from '../../components/FixturesRow';
import dataFixtureFake from './dataFixtureFake';

class Home extends PureComponent {
  state = {};

  renderTopScore = data => {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => (
          <InfoKickRow
            onPressPlayer={() => this.props.onPressPlayer()}
            title={item.title}
            sub_title={<Text>{item.sub_title}</Text>}
            rightContent={<Text>{item.id + ' ' + LanguageVI.goals}</Text>}
          />
        )}
      />
    );
  };

  renderFixture = data => {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => (
          <FixtureRow
            onPressMatch={() => this.props.onPressMatch()}
            data={item}
          />
        )}
      />
    );
  };

  render() {
    return (
      <ScrollView>
        <Header
          title="Premier League"
          rightHeader={
            <Icon name="md-star-outline" size={23} style={{ color: 'white' }} />
          }
        />

        <Table
          title={LanguageVI.Fixtures}
          showAllText={LanguageVI.SHOWALL}
          content={this.renderFixture(dataFixtureFake)}
        />

        <Table
          title={LanguageVI.TopScore}
          // subTitle={LanguageVI.SortByPopularity}
          showAllText={LanguageVI.SHOWALL}
          content={this.renderTopScore(dataTransfer)}
          onPressShowAll={() => this.props.navigation.navigate('TopScore')}
        />

        <TeamTableScroce
          data={data}
          onPressTeam={() => this.props.onPressTeam()}
        />
      </ScrollView>
    );
  }
}

export default Home;
