const dataFixture = [
  {
    id: 1,
    date: '03/11/2017',
    league: 'Tyrone Niblo',
    data: {
      end: true,
      team1: 'Rafa Yedy',
      team2: 'Kalina May',
      score1: 4,
      score2: 2,
    },
  },
  {
    id: 2,
    date: '17/02/2018',
    league: 'Sebastiano Dellenty',
    data: {
      end: false,
      team1: 'Rafa Yedy',
      team2: 'Kalina May',
      score1: 4,
      score2: 2,
    },
  },
  {
    id: 3,
    date: '26/10/2017',
    league: 'Brinn Rex',
    data: {
      end: true,
      team1: 'Rafa Yedy',
      team2: 'Kalina May',
      score1: 4,
      score2: 2,
    },
  },
];

export default dataFixture;
